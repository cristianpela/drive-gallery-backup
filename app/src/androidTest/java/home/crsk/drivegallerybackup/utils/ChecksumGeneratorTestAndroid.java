package home.crsk.drivegallerybackup.utils;

import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Test;

import java.io.File;
import java.util.List;

import home.crsk.drivegallerybackup.ApplicationTest;
import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.sqlite.GalleryDAO;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by criskey on 21/1/2017.
 */
@SmallTest
public class ChecksumGeneratorTestAndroid extends ApplicationTest {

    @Test
    public void test_generation() {
        long start = Utils.now();
        GalleryDAO galleryDAO = new GalleryDAO(getContext());
        List<FileInfo> photos = galleryDAO.read(null, null);
        ChecksumGenerator checksumGenerator = new ChecksumGenerator();
        String[] checkSums1 =  new String[photos.size()];
        for (int i = 0, s = photos.size(); i < s; i++) {
            checkSums1[i]= checksumGenerator.generate(new File(photos.get(i).localPath));
        }
        String[] checkSums2 =  new String[photos.size()];
        for (int i = 0, s = photos.size(); i < s; i++) {
            checkSums2[i]= checksumGenerator.generate(new File(photos.get(i).localPath));
        }
        assertArrayEquals(checkSums1, checkSums2);
        float seconds = (Utils.now() - start) * 1f / 1000;
        System.out.println("=====Checksum process duration: " + seconds + " =======");
    }

}