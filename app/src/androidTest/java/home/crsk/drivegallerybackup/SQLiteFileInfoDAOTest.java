package home.crsk.drivegallerybackup;

import android.test.suitebuilder.annotation.SmallTest;
import android.util.Pair;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import home.crsk.drivegallerybackup.sqlite.FileInfoDAO;
import home.crsk.drivegallerybackup.sqlite.Page;
import home.crsk.drivegallerybackup.sqlite.Pageable;
import home.crsk.drivegallerybackup.sqlite.Transaction;
import home.crsk.drivegallerybackup.utils.FluentMap;
import home.crsk.drivegallerybackup.model.FileInfo;

import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.DRIVE_ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.LOCAL_ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.NAME;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.PENDING_UP;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable._ID;

/**
 * Created by criskey on 2/1/2017.
 */

@SmallTest
public class SQLiteFileInfoDAOTest extends ApplicationTest {

    @Test
    public void test_inserting_file_info() {
        final FileInfo fileInfo = generateRandomFileInfo();
        FileInfo result = mConnection.runTransaction(new Transaction.Query<FileInfo>() {
            @Override
            public FileInfo execute(FileInfoDAO dao) {
                dao.create(fileInfo);
                return dao.read(DRIVE_ID + " = ?", new String[]{fileInfo.driveId})
                        .iterator().next();
            }
        });
        FileInfo updated = fileInfo.mutate(new FluentMap().put(_ID, result.id).get());
        assertEquals(updated, result);
    }

    @Test
    public void test_count() {
        final int size = 10;
        int count = mConnection.runTransaction(new Transaction.Query<Integer>() {
            @Override
            public Integer execute(FileInfoDAO dao) {
                for (int i = 0; i < size; i++) {
                    dao.create(generateRandomFileInfo());
                }
                return dao.count();
            }
        });
        assertEquals("Count must be " + size, size, count);
    }

    @Test
    public void test_paging() {

        assertEquals("Must be no next page when are no records", false, mDao.read(new Page(100)).first.hasMore());

        //inserting 11 records
        final List<FileInfo> toRecord = new ArrayList<>();
        final int recordSize = 11;
        for (int i = 0; i < recordSize; i++) {
            toRecord.add(generateRandomFileInfo());
        }
        Collections.reverse(toRecord);// reverse by CREATED_TIME descending in order to match the query


        final Pair<Pageable, List<FileInfo>> result = mConnection.runTransaction(new Transaction.Query<Pair<Pageable, List<FileInfo>>>() {
            @Override
            public Pair<Pageable, List<FileInfo>> execute(FileInfoDAO dao) {
                for (FileInfo f : toRecord)
                    dao.create(f);
                return dao.read(new Page(100));
            }
        });

        final int count = mDao.count();
        assertEquals("Count must be " + recordSize, recordSize, count);

        Pageable page = result.first;
        assertEquals("Must be no next page when there are less records than " + page.getSize(), false,
                page.hasMore());
        assertEquals("Retrieved page must have records " + recordSize, recordSize, result.second.size());

        final int pageSize_ = 5;
        Pair<Pageable, List<FileInfo>> result_ = mDao.read(new Page(pageSize_));//page one [1...5]
        page = result_.first;
        assertEquals("Retrieved page must have 5 records ", pageSize_, result_.second.size());
        String expectedPageToken = Long.toString(toRecord.get(pageSize_ - 1).createdTime);
        assertEquals("Next page token must be " + expectedPageToken, expectedPageToken, page.getNextToken());

        result_ = mDao.read(page);//page two (5...10]
        page = result_.first;
        assertEquals("Retrieved page must have 5 records ", pageSize_, result_.second.size());
        expectedPageToken = Long.toString(toRecord.get((pageSize_ + pageSize_ - 1)).createdTime);
        assertEquals("Next page token must be: " + expectedPageToken, expectedPageToken, page.getNextToken());

        result_ = mDao.read(page);//third page [10]
        page = result_.first;
        assertEquals("Retrieved page must have 1 records ", 1, result_.second.size());
        assertEquals("Should be no more pages: ", false, page.hasMore());

        page.setSize(100);
        result_ = mDao.read(page);
        page = result_.first;
        assertEquals("Retrieved page must have 1 records ", 1, result_.second.size());
        assertEquals("Should be no more pages: ", false, page.hasMore());

    }

    @Test
    public void test_update() {

        FileInfo original = generateRandomFileInfo();
        long createdId = mDao.create(original);

        FileInfo toUpdate = original.mutate(new FluentMap()
                .put(_ID, createdId)
                .put(NAME, "toUpdate name")
                .get());
        mDao.update(toUpdate);
        FileInfo result = mDao.read(DRIVE_ID + " = ?", new String[]{toUpdate.driveId})
                .iterator()
                .next();
        assertEquals(toUpdate, result);

        toUpdate = toUpdate.mutate(new FluentMap()
                .put(_ID, 0L)
                .put(NAME, "toUpdate name2")
                .put(LOCAL_ID, 12345L)
                .get());
        mDao.update(toUpdate);
        result = mDao.read(_ID + " = ?", new String[]{"" + createdId})
                .iterator()
                .next();
        assertEquals("toUpdate name2", result.name);
        assertEquals(12345L, result.localId);
    }

    @Test
    public void test_delete() {

        final List<FileInfo> records = new ArrayList<>();

        mConnection.runTransaction(new Transaction.Query<Void>() {
            @Override
            public Void execute(FileInfoDAO dao) {
                for (int i = 0; i < 10; i++) {
                    final FileInfo fileInfo = generateRandomFileInfo();
                    long id = dao.create(fileInfo);
                    records.add(fileInfo.mutate(new FluentMap()
                            .put(_ID, id).get()));

                }
                return null;
            }
        });

        assertTrue(mDao.delete(records.get(5)) == 1);
        assertTrue(mDao.delete(records.get(0)) == 1);
        assertTrue(mDao.delete(records.get(2)) == 1);
        assertTrue(mDao.count() == 7);

    }

    @Test
    public void test_read_pending() {
        final int countPending = mConnection.runTransaction(new Transaction.Query<Integer>() {
            @Override
            public Integer execute(FileInfoDAO dao) {
                for (int i = 0; i < 10; i++) {
                    dao.create(generateRandomFileInfo(i % 2 == 0, false, false));
                }
                return dao.read(PENDING_UP + "=1").size();
            }
        });
        assertEquals("Must be 5 pendingUp files", 5, countPending);
    }


    @Test
    public void test_exists() {
        FileInfo fileInfo = generateRandomFileInfo();
        assertFalse(mDao.exists(fileInfo));
        long id = mDao.create(fileInfo);
        fileInfo = fileInfo.mutate(new FluentMap()
                .put(_ID, id)
                .get());
        assertTrue(mDao.exists(fileInfo));
    }


    @Override
    protected void tearDown() throws Exception {
        mConnection.close();
        super.tearDown();
    }

}
