package home.crsk.drivegallerybackup;

import android.app.Application;
import android.os.SystemClock;
import android.test.ApplicationTestCase;

import java.util.Random;
import java.util.UUID;

import home.crsk.drivegallerybackup.sqlite.FileInfoDAO;
import home.crsk.drivegallerybackup.sqlite.FileInfoDBConnection;
import home.crsk.drivegallerybackup.sqlite.SQLiteFileInfoConnection;
import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.model.FileInfoBuilder;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }


    private static final String TEST_DB = "test.db";

    FileInfoDAO mDao;

    FileInfoDBConnection mConnection;

    Random mRandom = new Random();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mConnection = new SQLiteFileInfoConnection(getContext(), TEST_DB, 1);
        mConnection.open();
        mDao = mConnection.getDAO();
        mDao.deleteAll();
    }

    @Override
    protected void tearDown() throws Exception {
        mConnection.close();
        super.tearDown();
    }

    protected FileInfo generateRandomFileInfo() {
        return generateRandomFileInfo(false, false, false);
    }

    protected FileInfo generateRandomFileInfo(boolean pendingUp, boolean pendingUpdate, boolean pendingDelete) {
        return new FileInfoBuilder()
                .setId(-1)
                .setDriveId(UUID.randomUUID().toString())
                .setLocalId(mRandom.nextInt(10000) + 1)
                .setName(UUID.randomUUID().toString())
                .setCreatedTime(SystemClock.currentThreadTimeMillis())
                .setMimeType(UUID.randomUUID().toString())
                .setLocalPath(UUID.randomUUID().toString())
                .setThumbPath(UUID.randomUUID().toString())
                .setSynced(true)
                .setPendingDel(pendingDelete)
                .setPendingUp(pendingUp)
                .setPendingUpdate(pendingUpdate)
                .createFileInfo();
    }
}