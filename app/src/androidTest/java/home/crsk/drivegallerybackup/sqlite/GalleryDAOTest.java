package home.crsk.drivegallerybackup.sqlite;

import android.util.Pair;

import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import home.crsk.drivegallerybackup.ApplicationTest;
import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * Created by criskey on 11/1/2017.
 */
public class GalleryDAOTest extends ApplicationTest{

   @Rule FileInfoDAO mDao = new GalleryDAO(getContext());

    @Test
    public void test_fetching_pages(){
        Page page = new Page(10);
        Pair<Pageable, List<FileInfo>> result = mDao.read(page);
        assertEquals("Result must be 10", 10, result.second.size());
        System.out.println(result.second);
    }
}