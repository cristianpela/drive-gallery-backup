package home.crsk.drivegallerybackup;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;

import home.crsk.drivegallerybackup.databinding.ActivityMainBinding;
import home.crsk.drivegallerybackup.services.GalleryBackupIntentService;
import home.crsk.drivegallerybackup.utils.Utils;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;


public class MainActivity extends BaseActivity {

    public static final String ACTION_AUTH_ERROR = NS + "action.AUTH_ERROR";
    public static final String ACTION_AUTH_SUCCESS = NS + "action.AUTH_SUCCESS";
    public static final String ACTION_AUTH_RESET = NS + "action.AUTH_RESET";


    private static final int REQUEST_AUTHORIZATION = 1000;
    private static final int RC_GET_ACCOUNTS = 2000;
    private static final int REQUEST_GOOGLE_PLAY_SERVICES = 3000;

    private ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.setState(mState);
        mBinding.buttonAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseAccount();
            }
        });
        mBinding.buttonAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSubmision();
            }
        });
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void handleSubmision() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else {
            boolean lastStep = mState.isAuthenticated() && !mState.isConfigured();
            if (lastStep) {
                if (!TextUtils.isEmpty(mState.getFolderName())) {
                    mState.setProcessing(true);
                    startService(new Intent(getApplicationContext(), GalleryBackupIntentService.class)
                            .setAction(GalleryBackupIntentService.ACTION_SETUP)
                            .putExtra(GalleryBackupIntentService.EXTRA_SETUP_DIR, mState.getFolderName()));
                    finish();
                } else {
                    Toast.makeText(this, "Please enter a folder name!", Toast.LENGTH_SHORT).show();
                }
            } else if (!mState.isAuthenticated()) {
                if (!TextUtils.isEmpty(mState.getAccountName())) {
                    mState.setProcessing(true);
                    startService(new Intent(getApplicationContext(), GalleryBackupIntentService.class)
                            .setAction(GalleryBackupIntentService.ACTION_AUTH)
                            .putExtra(GalleryBackupIntentService.EXTRA_AUTH_ACCOUNT, mState.getAccountName()));
                } else {
                    Toast.makeText(this, "Account name is empty!", Toast.LENGTH_SHORT).show();
                }
            } else {
                //resetting auth data
                new AlertDialog.Builder(this)
                        .setTitle("Warning")
                        .setMessage("After reset, the application will create a new folder on Google Drive even if you keep the last name. " +
                                "The files from the last one are still on Google Drive. You will have to manually delete that folder, if you want.")
                        .setCancelable(true)
                        .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mState.setProcessing(true);
                                startService(new Intent(getApplicationContext(), GalleryBackupIntentService.class)
                                        .setAction(GalleryBackupIntentService.ACTION_RESET));
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
            }
        }

    }

    @AfterPermissionGranted(RC_GET_ACCOUNTS)
    private void chooseAccount() {
        final String[] perms = {Manifest.permission.GET_ACCOUNTS};
        if (EasyPermissions.hasPermissions(this, perms)) {
            final String[] accountNames = Utils.getAccountsName(AccountManager.get(this), "com.google");
            new AlertDialog.Builder(this)
                    .setTitle("Choose an account")
                    .setItems(accountNames, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which >= 0) mState.setAccountName(accountNames[which]);
                        }
                    })
                    .create()
                    .show();
        } else {
            EasyPermissions.requestPermissions(this, "This app needs to access the gmail accounts of this phone. Allow it?",
                    RC_GET_ACCOUNTS, perms);
        }
    }

    private void handleBroadcastIntent(Intent intent) {
        String action = intent.getAction();
        if (action.equals(ACTION_AUTH_ERROR)) {
            UserRecoverableAuthIOException authRecover = (UserRecoverableAuthIOException)
                    intent.getSerializableExtra(EXTRA_ERROR);
            startActivityForResult(authRecover.getIntent(), REQUEST_AUTHORIZATION);
        } else if (action.equals(ACTION_AUTH_SUCCESS)) {
            mState.setAuthenticated(true);
        } else if (action.equals(ACTION_AUTH_RESET)) {
            //this should be reset by service
            mState = mIStateLoader.load();
            mBinding.setState(mState);
        } else if (action.equals(ACTION_ERROR)) {
            Toast.makeText(this, intent.getStringExtra(EXTRA_ERROR), Toast.LENGTH_LONG).show();
        }
        mState.setProcessing(false);
    }


    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_GOOGLE_PLAY_SERVICES:
                    if (resultCode != RESULT_OK) {
                        Toast.makeText(this,
                                "This app requires Google Play Services. Please install " +
                                        "Google Play Services on your device and relaunch this app.", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case REQUEST_AUTHORIZATION: {
                    mState.setAuthenticated(true);
                    break;
                }
            }
        }
    }


    @Override
    protected void onReceive(Context context, Intent intent) {
        handleBroadcastIntent(intent);
    }

    @Override
    protected IntentFilter registerFilterIntent() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_AUTH_ERROR);
        filter.addAction(ACTION_AUTH_SUCCESS);
        filter.addAction(ACTION_AUTH_RESET);
        filter.addAction(ACTION_ERROR);
        return filter;
    }

    /**
     * Check that Google Play services APK is installed and up to date.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Attempt to resolve a missing, out-of-date, invalid or disabled Google
     * Play Services installation via a user dialog, if possible.
     */
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }


    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     *                             Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                MainActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }


    public void actionViewGallery(View view) {
        startActivity(new Intent(this, GalleryBackupActivity.class));
    }
}
