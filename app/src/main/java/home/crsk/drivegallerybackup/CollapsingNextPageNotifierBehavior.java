package home.crsk.drivegallerybackup;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.View;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by criskey on 22/12/2016.
 */

public class CollapsingNextPageNotifierBehavior extends AppBarLayout.ScrollingViewBehavior {

    private static final String TAG = CollapsingNextPageNotifierBehavior.class.getSimpleName();

    private static final int MAX_SKIPPED_SCROLL_FRAMES = 10;

    private WeakReference<CollapsingNextPageNotifier> mCollapsingNextPageNotifierWeakReference;

    private CollapsingNextPageNotifier mCollapsingNextPageNotifier;

    private AppBarLayout mAppBarLayout;

    private FloatingActionButton mFab;

    private boolean mScrollDown;

    private boolean mVisible;


    private int mSkippedScrollFrames;

    public CollapsingNextPageNotifierBehavior() {
    }

    public CollapsingNextPageNotifierBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return super.layoutDependsOn(parent, child, dependency) ||
                dependency instanceof CollapsingNextPageNotifier ||
                dependency instanceof FloatingActionButton;
    }

    @Override
    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, View child, View directTargetChild, View target, int nestedScrollAxes) {
        return true;
    }

    @Override
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, View child, View target, int dx, int dy, int[] consumed) {
        mScrollDown = dy > 0;
    }

    @Override
    public void onNestedScroll(CoordinatorLayout coordinatorLayout, View child, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        boolean downScrollReached = mScrollDown && dyConsumed == 0;
        if (downScrollReached && mSkippedScrollFrames++ > MAX_SKIPPED_SCROLL_FRAMES && !mVisible) {
            CollapsingNextPageNotifier notif = getCollapsingNextPageNotifier(coordinatorLayout, child);
            notif.show();
            mVisible = true;
        }

        getFab(coordinatorLayout, child).setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, View child, View target) {
        if (mVisible) {
            CollapsingNextPageNotifier notif = getCollapsingNextPageNotifier(coordinatorLayout, child);
            notif.hide();
            mVisible = false;
        }
        mSkippedScrollFrames = 0;
        getFab(coordinatorLayout, child).setVisibility(View.VISIBLE);
    }

    private CollapsingNextPageNotifier getCollapsingNextPageNotifier(CoordinatorLayout coordinatorLayout, View child) {
        if (mCollapsingNextPageNotifier != null) {
            return mCollapsingNextPageNotifier;
        }

        List<View> dependencies = coordinatorLayout.getDependencies(child);
        for (int i = 0, s = dependencies.size(); i < s; i++) {
            View v = dependencies.get(i);
            if (v instanceof CollapsingNextPageNotifier) {
                mCollapsingNextPageNotifier = (CollapsingNextPageNotifier) v;
                break;
            }
        }
        return mCollapsingNextPageNotifier;
    }

    private AppBarLayout getAppBarLayout(CoordinatorLayout coordinatorLayout, View child) {
        if (mAppBarLayout != null)
            return mAppBarLayout;

        List<View> dependencies = coordinatorLayout.getDependencies(child);
        for (int i = 0, s = dependencies.size(); i < s; i++) {
            View v = dependencies.get(i);
            if (v instanceof AppBarLayout) {
                mAppBarLayout = (AppBarLayout) v;
                break;
            }
        }
        return mAppBarLayout;

    }

    private FloatingActionButton getFab(CoordinatorLayout coordinatorLayout, View child) {
        if (mFab != null)
            return mFab;

        List<View> dependencies = coordinatorLayout.getDependencies(child);
        for (int i = 0, s = dependencies.size(); i < s; i++) {
            View v = dependencies.get(i);
            if (v instanceof FloatingActionButton) {
                mFab = (FloatingActionButton) v;
                break;
            }
        }
        return mFab;
    }
}
