package home.crsk.drivegallerybackup;

import android.app.Application;
import android.content.Context;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;

import home.crsk.drivegallerybackup.services.CloudService;
import home.crsk.drivegallerybackup.services.StateLoader;
import home.crsk.drivegallerybackup.services.SyncService;
import home.crsk.drivegallerybackup.sqlite.SQLiteFileInfoConnection;

/**
 * Created by criskey on 6/1/2017.
 */

public class GalleryBackApplication extends Application {


    @Override
    public void onCreate() {
        Context context = getApplicationContext();
        CloudService.init(context);
        SQLiteFileInfoConnection.init(context);
        State state = StateLoader.getInstance(context).load();
        if (state.isConfigured()) {
            SyncService.schedule(context);
        }
    }

    @Override
    public void onLowMemory() {
        SQLiteFileInfoConnection.getInstance().close();
        super.onLowMemory();
    }

}
