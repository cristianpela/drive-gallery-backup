package home.crsk.drivegallerybackup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.services.drive.Drive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import home.crsk.drivegallerybackup.async.AsyncImageDownloader;
import home.crsk.drivegallerybackup.databinding.ActivityGalleryBackupBinding;
import home.crsk.drivegallerybackup.dialogs.AboutDialog;
import home.crsk.drivegallerybackup.dialogs.PhotoDialog;
import home.crsk.drivegallerybackup.recycler.ListingType;
import home.crsk.drivegallerybackup.recycler.ObservableFileInfoEntries;
import home.crsk.drivegallerybackup.recycler.OnRecyclerItemPressedListener;
import home.crsk.drivegallerybackup.services.CloudService;
import home.crsk.drivegallerybackup.services.GalleryBackupIntentService;
import home.crsk.drivegallerybackup.sqlite.GalleryDAO;
import home.crsk.drivegallerybackup.sqlite.Page;
import home.crsk.drivegallerybackup.sqlite.Pageable;
import home.crsk.drivegallerybackup.utils.Utils;
import home.crsk.drivegallerybackup.model.FileInfo;

import static home.crsk.drivegallerybackup.recycler.ListingType.LIST;
import static home.crsk.drivegallerybackup.recycler.ListingType.GRID;
import static home.crsk.drivegallerybackup.utils.UIUtils.showMultiLineMessage;
import static home.crsk.drivegallerybackup.utils.UIUtils.showSimpleMessage;

public class GalleryBackupActivity extends BaseActivity implements OnRecyclerItemPressedListener,
        CollapsingNextPageNotifier.OnCountDownFinished,
        OnPhotoSynced,
        PhotoDialog.OnTrackCurrentPhotoFromDialog,
        ActionMode.Callback,
        CheckBoxActionProvider.OnCheckBoxMenuActionListener {

    //@formatter:off
    public static final String ACTION_FILES_RESPONSE = NS + "action.FILES_RESPONSE";
    public static final String EXTRA_FILES = NS + "extra.FILES";
    public static final String EXTRA_FILE_DELETE = NS + "extra.FILE_DELETE";
    public static final String EXTRA_NEXT_PAGE = NS + "extra.NEXT_PAGE";

    public static final String ACTION_NEW_FILE_UP_RESPONSE = NS + "action.NEW_FILE_UP_RESPONSE";
    public static final String EXTRA_NEW_FILE_UP = NS + "extra.NEW_FILE_UP";

    public static final String ACTION_FILE_DELETE_RESPONSE = NS + "action.FILE_DELETE_RESPONSE";
    public static final String EXTRA_FILE_DELETE_ERROR = NS + "extra.FILE_DELETE_ERROR";

    public static final String ACTION_NEW_MULTIPLE_FILE_UP_RESPONSE = NS + "action.NEW_MULTIPLE_FILE_UP_RESPONSE";
    public static final String EXTRA_NEW_MULTIPLE_FILE_UP = NS + "extra.NEW_MULTIPLE_FILE_UP";

    public static final String ACTION_FILE_UPDATE_RESPONSE = NS + "action.FILE_UPDATE_RESPONSE";

    public static final String ACTION_SYNC_RESPONSE = NS + "action.SYNC_RESPONSE";

    private static final String EXTRA_THUMB_SIZE =  NS + "extra.THUMB_SIZE";
    //@formatter:on

    private static final String TAG = GalleryBackupActivity.class.getSimpleName();

    private static final int DEFAULT_ITEMS_PER_PAGE = 12;

    private ActivityGalleryBackupBinding mBinding;

    private ObservableFileInfoEntries mFileInfoEntries;

    private ThumbnailDownloader mThumbnailDownloader;

    private String mAppName;

    private FileInfo mCurrentFileInfo;

    private CheckBoxActionProvider mCheckBoxMenuItemActionProvider;

    private Pageable mPage;

    private Pair<Integer, Integer> mThumbSize;

    @ListingType
    private int mListingType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFileInfoEntries = new ObservableFileInfoEntries();
        mListingType = getResources().getInteger(R.integer.gallery_listing_type);
        mAppName = getResources().getString(R.string.app_name);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_gallery_backup);
        mBinding.setList(mFileInfoEntries);
        mBinding.setItemPressedListener(this);
        mBinding.setListingType(LIST);
        mBinding.collapsingNextPageNotifier.setOnCountDownFinished(this);
        setSupportActionBar(mBinding.toolbar);
        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionRefresh(DEFAULT_ITEMS_PER_PAGE);
            }
        });
        if (savedInstanceState != null) {
            Log.d(TAG, "Recreating data adapter from saved instance state");
            mFileInfoEntries.addAllFileInfo(savedInstanceState.<FileInfo>getParcelableArrayList(EXTRA_FILES));
            mPage = savedInstanceState.getParcelable(EXTRA_NEXT_PAGE);
            int[] thArr = savedInstanceState.getIntArray(EXTRA_THUMB_SIZE);
            mThumbSize = new Pair(thArr[0], thArr[1]);
        } else {
            mPage = newPage(DEFAULT_ITEMS_PER_PAGE);
            calculateThumbSize();
            fetchGallery();
        }

    }

    @NonNull
    private Pageable newPage(int size) {
        return new Page(size);
    }

    private void calculateThumbSize() {
        Pair<Integer, Integer> screenSize = Utils.getScreenSize(this);
        final int halfScreenHeight = (int) (screenSize.second * 0.5);
        final int defaultHeight = 220; // default drive api thumb size
        int desiredHeight = (defaultHeight >= halfScreenHeight) ? defaultHeight : halfScreenHeight;
        mThumbSize = new Pair(screenSize.first, desiredHeight);
    }

    private void fetchThumbnail(FileInfo fileInfo) {
        mCurrentFileInfo = fileInfo;
        if (fileInfo != null) {
            actionCancel(null); // cancel a previous thumbnail fetch task, if any
            mThumbnailDownloader = (ThumbnailDownloader) new ThumbnailDownloader(fileInfo).executeTask();
        }
    }

    private void setToolbarTitle(String title) {
        mBinding.toolbar.setTitle(title);
        mBinding.collapsingToolbar.setTitle(title);
    }

    public void fetchGallery() {
        PhotoDialog photoDialog = (PhotoDialog) getSupportFragmentManager()
                .findFragmentByTag(PhotoDialog.FRAGMENT_PHOTO_DIALOG_TAG);
        boolean endOfGallery = true;
        if (mPage.hasMore()) {
            startService(new Intent(getApplicationContext(), GalleryBackupIntentService.class)
                    .setAction(GalleryBackupIntentService.ACTION_FILES_REQUEST)
                    .putExtra(GalleryBackupIntentService.EXTRA_NEXT_PAGE, mPage));
            mBinding.collapsingNextPageNotifier.setDownloading(true);
            endOfGallery = false;
        }
        if (photoDialog != null) {
            photoDialog.setEndOfGallery(endOfGallery);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.delete:
                startSupportActionMode(this);
                mBinding.setCheckVisible(true);
                return true;
            case R.id.info:
                new AboutDialog().show(getSupportFragmentManager(), AboutDialog.TAG);
                return true;
            case R.id.sync:
                startService(new Intent(getApplicationContext(), GalleryBackupIntentService.class)
                        .setAction(GalleryBackupIntentService.ACTION_SYNC)
                        .putExtra(GalleryBackupIntentService.EXTRA_SYNC_FORCED, true));
                return true;
            case R.id.mock_sync:
                startService(new Intent(getApplicationContext(), GalleryBackupIntentService.class)
                        .setAction(GalleryBackupIntentService.ACTION_SYNC));
                return true;
            case R.id.account:
                finish();
                return true;
            case R.id.list_view:
                changeListPresentation(LIST);
                return true;
            case R.id.grid_view:
                changeListPresentation(GRID);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void changeListPresentation(@ListingType int listingType) {
        mListingType = listingType;
        mBinding.setListingType(listingType);
        if (listingType == LIST) {
            fetchThumbnail(mCurrentFileInfo);
        } else {
            mBinding.imageThumb.setImageDrawable(null);
        }
    }

    private void displayGallery(Collection<FileInfo> fetchedGallery) {
        mFileInfoEntries.addAllFileInfo(fetchedGallery);
        mBinding.collapsingNextPageNotifier.setDownloading(false);
        PhotoDialog photoDialog = (PhotoDialog) getSupportFragmentManager()
                .findFragmentByTag(PhotoDialog.FRAGMENT_PHOTO_DIALOG_TAG);
        if (photoDialog != null) {
            photoDialog.setFileInfoList(mFileInfoEntries.getData());
        }
    }

    private void actionRefresh(int itemsPerPage) {
        mPage = newPage(itemsPerPage);
        mBinding.collapsingNextPageNotifier.setHasMoreDownloads(true);
        mFileInfoEntries.clear();
        fetchGallery();
    }

    public void actionCancel(View view) {
        if (mThumbnailDownloader != null &&
                mThumbnailDownloader.getStatus() != AsyncTask.Status.FINISHED) {
            mThumbnailDownloader.cancel();
        }
    }

    public void deletePhoto(final FileInfo fileInfo) {
        Snackbar.make(mBinding.coordinatorLayout, "Delete " + fileInfo.name + " ?", Snackbar.LENGTH_LONG)
                .setAction("Delete", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mFileInfoEntries.remove(fileInfo);
                        startService(new Intent(v.getContext().getApplicationContext(),
                                GalleryBackupIntentService.class)
                                .setAction(GalleryBackupIntentService.ACTION_FILE_DELETE_REQUEST)
                                .putExtra(GalleryBackupIntentService.EXTRA_FILE, fileInfo));
                    }
                })
                .show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_FILES, mFileInfoEntries.getData());
        outState.putParcelable(EXTRA_NEXT_PAGE, mPage);
        outState.putIntArray(EXTRA_THUMB_SIZE, new int[]{mThumbSize.first, mThumbSize.second});
    }

    @Override
    protected void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(ACTION_FILES_RESPONSE)) {
            ArrayList<FileInfo> fetchedGallery = intent.getParcelableArrayListExtra(EXTRA_FILES);
            mPage = intent.getParcelableExtra(EXTRA_NEXT_PAGE);
            if (!mPage.hasMore()) {
                mBinding.collapsingNextPageNotifier.setHasMoreDownloads(false);
            } else {
                mPage.setSize(DEFAULT_ITEMS_PER_PAGE);
            }
            Log.i(TAG, mPage.toString());
            displayGallery(fetchedGallery);
        } else if (action.equals(ACTION_NEW_FILE_UP_RESPONSE)) {
            actionRefresh(mFileInfoEntries.size() + 1);
            showSimpleMessage(mBinding.coordinatorLayout, "File Uploaded");
        } else if (action.equals(ACTION_NEW_MULTIPLE_FILE_UP_RESPONSE)) {
            int no = intent.getIntExtra(EXTRA_NEW_MULTIPLE_FILE_UP, 0);
            actionRefresh(mFileInfoEntries.size() + no);
            showSimpleMessage(mBinding.coordinatorLayout, "Batch File Uploaded");
        } else if (action.equals(ACTION_FILE_DELETE_RESPONSE)) {
            //TODO Deal with delete response?
        } else if (action.equals(ACTION_FILE_UPDATE_RESPONSE)) {
            actionRefresh(mFileInfoEntries.size());
        } else if (action.equals(ACTION_ERROR)) {
            showSimpleMessage(mBinding.coordinatorLayout, intent.getStringExtra(EXTRA_ERROR));
        } else if (action.equals(ACTION_SYNC_RESPONSE)) {
            actionRefresh(mFileInfoEntries.size());
        }
    }

    @Override
    protected IntentFilter registerFilterIntent() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_FILES_RESPONSE);
        intentFilter.addAction(ACTION_NEW_FILE_UP_RESPONSE);
        intentFilter.addAction(ACTION_NEW_MULTIPLE_FILE_UP_RESPONSE);
        intentFilter.addAction(ACTION_FILE_DELETE_RESPONSE);
        intentFilter.addAction(ACTION_FILE_UPDATE_RESPONSE);
        intentFilter.addAction(ACTION_ERROR);
        intentFilter.addAction(ACTION_SYNC_RESPONSE);
        return intentFilter;
    }


    @Override
    protected void onResume() {
        //    SQLiteFileInfoConnection.getInstance().open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        actionCancel(null);
        //  SQLiteFileInfoConnection.getInstance().close();
        super.onPause();
    }

    @Override
    public void onItemPressed(FileInfo fileInfo, boolean isLongPressed) {
        if (isLongPressed) {
            deletePhoto(fileInfo);
        } else {
            if (mListingType == LIST)
                fetchThumbnail(fileInfo);
            else
                viewPhoto(mFileInfoEntries.index(fileInfo));
        }
    }

    @Override
    public void onCountDownFinished() {
        fetchGallery();
    }

    public void actionViewPhoto(View view) {
        viewPhoto(mFileInfoEntries.index(mCurrentFileInfo));
    }

    private void viewPhoto(int index) {
        if (index != -1) {
            PhotoDialog.showDialog(getSupportFragmentManager(), mFileInfoEntries.getData(), index);
        } else {
            showSimpleMessage(mBinding.coordinatorLayout, "Photo was previously removed");
        }
    }

    @Override
    public void onPhotoSynced(int index, FileInfo fileInfo) {
        mFileInfoEntries.setFileInfo(index, fileInfo);
    }

    @Override
    public void onTrackCurrentPhotoFromDialog(int index, FileInfo fileInfo) {
        mCurrentFileInfo = fileInfo;
        mBinding.listGalleryBackup.getLayoutManager().scrollToPosition(index);
        if (mListingType == LIST)
            fetchThumbnail(fileInfo);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.toolbar_contextual, menu);
        mCheckBoxMenuItemActionProvider = (CheckBoxActionProvider) MenuItemCompat.getActionProvider(menu.findItem(R.id.delete_contextual));
        mCheckBoxMenuItemActionProvider.setOnCheckBoxMenuActionListener(this);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mBinding.setCheckVisible(false);
        mCheckBoxMenuItemActionProvider.removeOnCheckBosMenuActionListener(this);
        final ArrayList<FileInfo> checkedList = mFileInfoEntries.getCheckedData();
        if (!checkedList.isEmpty())
            new AlertDialog.Builder(this)
                    .setTitle("Warning")
                    .setMessage("Are you sure you want to delete the " + checkedList.size() + " selected file(s) from cloud?")
                    .setCancelable(true)
                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mFileInfoEntries.remove(checkedList);
                            startService(new Intent(getApplicationContext(), GalleryBackupIntentService.class)
                                    .setAction(GalleryBackupIntentService.ACTION_MULTIPLE_FILE_DELETE_REQUEST)
                                    .putParcelableArrayListExtra(GalleryBackupIntentService.EXTRA_MULTIPLE_FILES, checkedList));
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create()
                    .show();
    }

    @Override
    public void onCheckBoxMenuAction(boolean isChecked) {
        mFileInfoEntries.checkAll(isChecked);
    }

    private class ThumbnailDownloader extends AsyncImageDownloader {

        private String mDriveId;

        public ThumbnailDownloader(FileInfo fileInfo) {
            super(getApplicationContext(), fileInfo, false);
            this.mDriveId = mFileInfo.driveId;
        }

        @Override
        protected ThrowableResult<Bitmap> doInBackground(FileInfo... params) {
            Bitmap thumb;
            try {
                GalleryDAO galleryDAO = new GalleryDAO(mContext);
                String path = null;
                if (galleryDAO.exists(mFileInfo)) {
                    path = mFileInfo.localPath;
                } else if (mCacheService.exists(mFileInfo)) {
                    path = mCacheService.getPath(mFileInfo);
                }
                if (path == null)
                    throw new IOException(mFileInfo.name + " not found locally");
                thumb = getSampleSizedBitmap(path, mThumbSize.first, mThumbSize.second);
                thumb = ThumbnailUtils.extractThumbnail(thumb, mThumbSize.first, mThumbSize.second,
                        ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                return result(thumb);
            } catch (IOException e) {
                return super.doInBackground(params);
            }
        }

        @Override
        public HttpResponse createHttpResponse() throws IOException {
            CloudService cloudService = CloudService.getInstance();
            Drive driveService = cloudService.getGoogleDriveAPI();
            FileInfo fileInfo = cloudService.fetchOne(mDriveId);
            GenericUrl url = new GenericUrl(fileInfo.thumbPath);
            return driveService.getRequestFactory()
                    .buildGetRequest(url).execute();
        }

        @Override
        protected void onPreExecute() {
            setToolbarTitle("Downloading...");
            mBinding.progDownload.setProgress(0);
            setVisibility(View.VISIBLE);
        }

        @Override
        protected void onCancelled(ThrowableResult<Bitmap> result) {
            if (result == null || result.error == null)
                return;
            Log.e(TAG, "Task canceled, reason: " + result.error.getMessage());
            setToolbarTitle(mAppName);
            setVisibility(View.GONE);
            showMultiLineMessage(mBinding.coordinatorLayout, result.error.getMessage());
        }


        @Override
        protected void onProgressUpdate(Long... values) {
            float current = values[0];
            float total = values[1];
            float percentage = current / total * 100;
            mBinding.progDownload.setProgress((int) percentage);
        }

        @Override
        protected void onPostExecute(ThrowableResult<Bitmap> result) {
            if (result.result != null) {
                setToolbarTitle(mCurrentFileInfo.name);
                mBinding.imageThumb.setImageBitmap(result.result);
                setVisibility(View.GONE);
            } else if (result.error != null) {
                showMultiLineMessage(mBinding.coordinatorLayout, result.error.getMessage());
            }
        }

        private void setVisibility(int visibility) {
            mBinding.progDownload.setVisibility(visibility);
            mBinding.buttonCancel.setVisibility(visibility);
        }

    }

}
