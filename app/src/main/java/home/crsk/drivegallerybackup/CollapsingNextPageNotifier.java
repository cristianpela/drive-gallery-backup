package home.crsk.drivegallerybackup;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import java.util.concurrent.TimeUnit;

public class CollapsingNextPageNotifier extends FrameLayout {

    private CountDownTimer mCountDown;

    private OnCountDownFinished mOnCountDownFinished;

    private boolean isDownloading;

    private boolean hasMoreDownloads = true;

    private boolean mCanceled;

    private boolean mCountDownStarted;

    private long MAX_COUNTDOWN_MILLIS = TimeUnit.SECONDS.toMillis(1);

    private int MAX_PROGRESS = 50;

    private Animation mScaleAnimation;

    public CollapsingNextPageNotifier(Context context) {
        super(context);
    }

    public CollapsingNextPageNotifier(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CollapsingNextPageNotifier(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnCountDownFinished(OnCountDownFinished onCountDownFinished) {
        mOnCountDownFinished = onCountDownFinished;
    }

    public void show() {
        if (!hasMoreDownloads)
            return;

        mScaleAnimation = new ScaleAnimation(1.0f, 1.0f, 0.0f, 1.0f,  Animation.RELATIVE_TO_SELF, 1f,
                Animation.RELATIVE_TO_SELF, 0f);
        mScaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setVisibility(VISIBLE);
                if (!isDownloading) {
                    if (!mCountDownStarted) {
                        // getCountDownProgressBar().setProgress(MAX_PROGRESS);
                        mCountDownStarted = true;
                        mCanceled = false;
                        mCountDown = new CountDownTimer(MAX_COUNTDOWN_MILLIS, TimeUnit.SECONDS.toMillis(1)) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                //  getCountDownProgressBar().setProgress((int) millisUntilFinished / 100);
                            }

                            @Override
                            public void onFinish() {
                                getCountDownProgressBar().setProgress(0);
                                mCountDownStarted = false;
                                if (mOnCountDownFinished != null && !mCanceled) {
                                    mOnCountDownFinished.onCountDownFinished();
                                    mCanceled = false;
                                }

                            }
                        }.start();
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        this.startAnimation(mScaleAnimation);

      //  mOnCountDownFinished.onCountDownFinished();

    }

    public void hide() {
        setVisibility(GONE);
        if (!isDownloading) {
            resetCountDown();
        }
        if(mScaleAnimation!=null)
        mScaleAnimation.cancel();
    }

    public void setDownloading(boolean downloading) {
        isDownloading = downloading;
        getPageProgressBar().setVisibility((downloading) ? VISIBLE : GONE);
      //  getCountDownProgressBar().setVisibility((downloading) ? GONE : VISIBLE);
        if (!downloading)
            hide();
        else
            invalidate();
    }

    public void setHasMoreDownloads(boolean hasMore) {
        hasMoreDownloads = hasMore;
    }

    private void resetCountDown() {
        if (mCountDown != null) {
            mCanceled = true;
            mCountDownStarted = false;
            mCountDown.cancel();
            getCountDownProgressBar().setProgress(MAX_PROGRESS);
        }
    }

    private ProgressBar getCountDownProgressBar() {
        return (ProgressBar) ((FrameLayout) getChildAt(0)).getChildAt(0);
    }

    private ProgressBar getPageProgressBar() {
        return (ProgressBar) ((FrameLayout) getChildAt(0)).getChildAt(1);
    }

    interface OnCountDownFinished {
        void onCountDownFinished();
    }

}
