package home.crsk.drivegallerybackup;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.CompoundButton;

/**
 * Created by criskey on 30/12/2016.
 */

public class CheckBoxActionProvider extends ActionProvider {


    private OnCheckBoxMenuActionListener onCheckBoxMenuActionListener;

    public interface OnCheckBoxMenuActionListener{
        void onCheckBoxMenuAction(boolean isChecked);
    }
    /**
     * Creates a new instance.
     *
     * @param context Context for accessing resources.
     */
    public CheckBoxActionProvider(Context context) {
        super(context);
    }

    @Override
    public View onCreateActionView() {
        AppCompatCheckBox checkbox =  new AppCompatCheckBox(getContext());
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(onCheckBoxMenuActionListener!=null)
                    onCheckBoxMenuActionListener.onCheckBoxMenuAction(isChecked);
            }
        });
        return checkbox;
    }

    public void setOnCheckBoxMenuActionListener(OnCheckBoxMenuActionListener listener){
        onCheckBoxMenuActionListener = listener;
    }

    public void removeOnCheckBosMenuActionListener(OnCheckBoxMenuActionListener listener){
        onCheckBoxMenuActionListener = null;
    }




}
