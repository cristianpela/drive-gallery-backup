package home.crsk.drivegallerybackup;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import home.crsk.drivegallerybackup.services.GalleryBackupIntentService;

public class ActionSendForwardActivity extends AppCompatActivity {

    private static final String TAG = ActionSendForwardActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String action = intent.getAction();
        Intent intentService = new Intent(getApplicationContext(), GalleryBackupIntentService.class);
        if (action.equals(Intent.ACTION_SEND)) {
            Log.d(TAG, "Single Share: " + intent.getParcelableExtra(Intent.EXTRA_STREAM).toString());
            intentService.setAction(GalleryBackupIntentService.ACTION_UPLOAD)
                    .setData((Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM));
        } else if (action.equals(Intent.ACTION_SEND_MULTIPLE)) {
            Log.d(TAG, "Single Multiple: " + intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM).toString());
            intentService.setAction(GalleryBackupIntentService.ACTION_MULTIPLE_UPLOAD)
                    .putParcelableArrayListExtra(GalleryBackupIntentService.EXTRA_MULTIPLE_FILES,
                            intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM));
        }
        startService(intentService);
        finish();
    }
}
