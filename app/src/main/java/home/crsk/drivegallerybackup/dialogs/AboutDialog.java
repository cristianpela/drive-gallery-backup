package home.crsk.drivegallerybackup.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.About;

import java.io.IOException;

import home.crsk.drivegallerybackup.R;
import home.crsk.drivegallerybackup.State;
import home.crsk.drivegallerybackup.databinding.AboutDialogBinding;
import home.crsk.drivegallerybackup.services.CloudService;
import home.crsk.drivegallerybackup.services.StateLoader;

/**
 * Created by criskey on 30/12/2016.
 */

public class AboutDialog extends DialogFragment {

    public static final String TAG = AboutDialog.class.getSimpleName();
    private static final float ONE_GB = 1024 * 1024 * 1024f;
    private AboutDialogBinding mBinding;
    private AsyncTask<Void, Void, AboutWrapper> mAboutTask;

    public AboutDialog() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        mBinding = DataBindingUtil.inflate(layoutInflater, R.layout.about_dialog, null, false);
        startTask();
        return new AlertDialog.Builder(getContext())
                .setTitle("About")
                .setView(mBinding.getRoot())
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
    }


    @Override
    public void onDetach() {
        if (mAboutTask != null
                && mAboutTask.getStatus() != AsyncTask.Status.FINISHED) {
            mAboutTask.cancel(true);
        }
        super.onDetach();
    }

    private void startTask(){
        mAboutTask = new AsyncTask<Void, Void, AboutWrapper>() {
            @Override
            protected AboutWrapper doInBackground(Void... params) {
                Context context = getContext().getApplicationContext();
                Drive driveService = CloudService.getInstance().getGoogleDriveAPI();
                try {
                    About about = driveService.about().get().setFields("storageQuota, user").execute();
                    State state = StateLoader.getInstance(context).load();
                    return new AboutWrapper(state.getFolderName(), state.getFolderId(), about);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(AboutWrapper aboutWrapper) {
                if (aboutWrapper != null) {
                    About about = aboutWrapper.about;
                    mBinding.txtAboutAccount.setText(about.getUser().getDisplayName());
                    mBinding.txtAboutFolder.setText(aboutWrapper.folderName);
                    About.StorageQuota quota = about.getStorageQuota();
                    float usage = quota.getUsage() / ONE_GB;
                    float limit = quota.getLimit() / ONE_GB;
                    mBinding.txtAboutSpace.setText(String.format("%.3f/%.3f GB", usage, limit));
                }
            }
        }.execute();
    }

    private static class AboutWrapper {

        final String folderName;
        final String folderId;
        final About about;

        public AboutWrapper(String folderName, String folderId, About about) {
            this.folderName = folderName;
            this.folderId = folderId;
            this.about = about;
        }
    }
}
