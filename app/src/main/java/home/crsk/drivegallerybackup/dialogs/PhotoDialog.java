package home.crsk.drivegallerybackup.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.api.client.http.HttpResponse;
import com.google.api.services.drive.Drive;

import java.io.IOException;
import java.util.ArrayList;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.GalleryBackupActivity;
import home.crsk.drivegallerybackup.OnPhotoSynced;
import home.crsk.drivegallerybackup.OnSwipeTouchListener;
import home.crsk.drivegallerybackup.R;
import home.crsk.drivegallerybackup.async.AsyncImageDownloader;
import home.crsk.drivegallerybackup.async.SynchronizingPhotoDownloader;
import home.crsk.drivegallerybackup.databinding.DialogPhotoBinding;
import home.crsk.drivegallerybackup.services.CloudService;
import home.crsk.drivegallerybackup.sqlite.GalleryDAO;
import home.crsk.drivegallerybackup.utils.UIUtils;
import home.crsk.drivegallerybackup.utils.Utils;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by criskey on 26/12/2016.
 */

public class PhotoDialog extends DialogFragment {

    public static final String FRAGMENT_PHOTO_DIALOG_TAG = "PHOTO_DIALOG";

    private static final String EXTRA_FILE_INFO_LIST = "EXTRA_FILE_INFO_LIST";

    private static final String EXTRA_CURRENT_INDEX = "EXTRA_CURRENT_INDEX";

    private static final String TAG = PhotoDialog.class.getSimpleName();

    private DialogPhotoBinding mBinding;

    private PhotoDownloader mPhotoDownloader;

    private LocalSynchronized mLocalSynchronizedTask;

    private Bitmap mCurrentBitmap;

    private FileInfo mFileInfo;

    private ArrayList<FileInfo> mFileInfoList;

    private int mCurrentIndex;

    private OnPhotoSynced mOnPhotoSynced;

    private OnTrackCurrentPhotoFromDialog mOnTrackCurrentPhotoFromDialog;

    private String[] mLoadingSources;

    private boolean mWaitForNextPage;

    private boolean mEndOfGallery;

    public PhotoDialog() {
    }

    public static void showDialog(FragmentManager fragmentManager, ArrayList<FileInfo> fileInfoList, int index) {
        PhotoDialog photoDialog = new PhotoDialog();
        Bundle args = new Bundle();
        args.putParcelableArrayList(EXTRA_FILE_INFO_LIST, fileInfoList);
        args.putInt(EXTRA_CURRENT_INDEX, index);
        photoDialog.setArguments(args);
        photoDialog.show(fragmentManager, FRAGMENT_PHOTO_DIALOG_TAG);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_photo, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mLoadingSources = getResources().getStringArray(R.array.loading_sources);
        mFileInfoList = getArguments().getParcelableArrayList(EXTRA_FILE_INFO_LIST);
        mCurrentIndex = getArguments().getInt(EXTRA_CURRENT_INDEX);
        init();
        mBinding.imgPhoto.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
            @Override
            public void onSwipeLeft() {
                if (mCurrentIndex < mFileInfoList.size() - 1) {
                    mCurrentIndex++;
                    init();
                } else {
                    showEndOfListMessage();
                }
            }

            @Override
            public void onSwipeRight() {
                if (mCurrentIndex > 0) {
                    mCurrentIndex--;
                    init();
                }
            }
        });
    }

    private void showEndOfListMessage() {
        if (mEndOfGallery) {
            Snackbar.make(mBinding.txtLoadingSource, "End of gallery", Snackbar.LENGTH_SHORT).show();
        } else if (mWaitForNextPage) {
            Snackbar.make(mBinding.txtLoadingSource, "Wait for next gallery page to read...", Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(mBinding.txtLoadingSource, "Load next gallery page?", Snackbar.LENGTH_LONG)
                    .setAction("YES", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            GalleryBackupActivity activity = (GalleryBackupActivity) getActivity();
                            activity.fetchGallery();
                        }
                    })
                    .show();
        }
    }

    private void init() {
        cancelTasks();
        mFileInfo = mFileInfoList.get(mCurrentIndex);
        mBinding.txtLoadingSource.setText("");
        mBinding.txtPositionTrack.setText((mCurrentIndex + 1) + "/" + mFileInfoList.size());
        mBinding.btnSync.setVisibility((mFileInfo.synced) ? GONE : VISIBLE);
        mBinding.btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLocalSynchronizedTask = new LocalSynchronized(getContext(), mFileInfo, mCurrentBitmap);
                mLocalSynchronizedTask.executeTask();
            }
        });
        mPhotoDownloader = (PhotoDownloader) new PhotoDownloader(mFileInfo).executeTask();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPhotoSynced)
            mOnPhotoSynced = (OnPhotoSynced) context;
        if (context instanceof OnTrackCurrentPhotoFromDialog)
            mOnTrackCurrentPhotoFromDialog = (OnTrackCurrentPhotoFromDialog) context;
    }

    @Override
    public void onDetach() {
        cancelTasks();

        //unload any bitmap
        if (mCurrentBitmap != null) {
            mBinding.imgPhoto.setImageBitmap(null);
            mBinding.imgPhoto.setImageDrawable(null);
            mCurrentBitmap.recycle();
            mCurrentBitmap = null;
        }

        //recycle interfaces
        mOnPhotoSynced = null;
        if (mOnTrackCurrentPhotoFromDialog != null)
            mOnTrackCurrentPhotoFromDialog.onTrackCurrentPhotoFromDialog(mCurrentIndex, mFileInfo);
        mOnTrackCurrentPhotoFromDialog = null;

        super.onDetach();
    }

    private void cancelTasks() {
        //cancel any task
        if (mPhotoDownloader != null &&
                mPhotoDownloader.getStatus() != AsyncTask.Status.FINISHED)
            mPhotoDownloader.cancel(true);

        if (mLocalSynchronizedTask != null &&
                mLocalSynchronizedTask.getStatus() != AsyncTask.Status.FINISHED)
            mLocalSynchronizedTask.cancel(true);
    }

    public void setEndOfGallery(boolean endOfGallery) {
        this.mWaitForNextPage = !endOfGallery;
        this.mEndOfGallery = endOfGallery;
    }

    public void setFileInfoList(ArrayList<FileInfo> fileInfoList) {
        this.mFileInfoList = fileInfoList;
        this.mWaitForNextPage = false;
        mBinding.txtPositionTrack.setText((mCurrentIndex + 1) + "/" + mFileInfoList.size());
    }

    public interface OnTrackCurrentPhotoFromDialog {
        void onTrackCurrentPhotoFromDialog(int index, FileInfo fileInfo);
    }

    private class PhotoDownloader extends AsyncImageDownloader {

        private final String mDriveId;

        private final long mLocalId;

        public PhotoDownloader(FileInfo fileInfo) {
            super(getContext().getApplicationContext(), fileInfo, true);
            this.mDriveId = fileInfo.driveId;
            this.mLocalId = fileInfo.localId;
        }

        @Override
        protected ThrowableResult<Bitmap> doInBackground(FileInfo... params) {
            Bitmap bitmap;
            if (mLocalId > 0) {
                Log.d(TAG, "Loading image driveId " + mDriveId + " from gallery");
                try {
                    GalleryDAO galleryDAO = new GalleryDAO(mContext);
                    if (galleryDAO.exists(mFileInfo)) {
                        Pair<Integer, Integer> screenSize = Utils.getScreenSize(mContext);
                        bitmap = getSampleSizedBitmap(mFileInfo.localPath, screenSize.first, screenSize.second);
                        publishProgress(0L, 0L, FROM_GALLERY);
                    } else
                        throw new IOException(mFileInfo.name + " not found in gallery");
                } catch (IOException e) {
                    e.printStackTrace();
                    bitmap = super.doInBackground(params).result;
                }
            } else {
                bitmap = super.doInBackground(params).result;
            }
            return result(bitmap);
        }

        @Override
        protected void onPreExecute() {
            if (mCurrentBitmap != null)
                mCurrentBitmap.recycle();
            setVisibility(GONE);
        }

        @Override
        protected void onCancelled(ThrowableResult<Bitmap> result) {
            setVisibility(VISIBLE);
            if (result.error != null) {
                UIUtils.showMultiLineMessage(mBinding.txtLoadingSource, result.error.getMessage());
            }
        }

        @Override
        protected void onPostExecute(ThrowableResult<Bitmap> result) {
            mCurrentBitmap = result.result;
            setVisibility(VISIBLE);
            mBinding.imgPhoto.setImageBitmap(mCurrentBitmap);
            mBinding.txtLoadingSource.setText(mLoadingSources[(int) mLoadingSource]);
            mBinding.txtPositionTrack.setText((mCurrentIndex + 1) + "/" + mFileInfoList.size());

        }

        @Override
        public HttpResponse createHttpResponse() throws IOException {
            Drive driveService = CloudService.getInstance().getGoogleDriveAPI();
            return driveService.files().get(mDriveId).executeMedia();
        }

        private void setVisibility(int visibility) {
            mBinding.imgPhoto.setVisibility(visibility);
            mBinding.progressLoadPhoto.setVisibility(GONE - visibility);
            if (mBinding.btnSync.getVisibility() == VISIBLE)
                mBinding.btnSync.setEnabled(visibility == VISIBLE);
        }
    }

    private class LocalSynchronized extends SynchronizingPhotoDownloader {

        public LocalSynchronized(Context context, FileInfo fileInfo, Bitmap source) {
            super(context, fileInfo, source);
        }

        @Override
        protected void onPreExecute() {
            mBinding.btnSync.setEnabled(false);
        }

        @Override
        protected void onCancelled() {
            mBinding.btnSync.setEnabled(true);
        }

        @Override
        protected void onPostExecute(ThrowableResult<Bitmap> result) {
            super.onPostExecute(result);
            mBinding.btnSync.setVisibility(GONE);
            if (mOnPhotoSynced != null)
                mOnPhotoSynced.onPhotoSynced(mCurrentIndex, mFileInfo);
        }
    }
}
