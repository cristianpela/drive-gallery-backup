package home.crsk.drivegallerybackup.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.WindowManager;

import com.google.api.client.util.DateTime;
import com.google.api.services.drive.model.File;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.model.FileInfoBuilder;

/**
 * Created by criskey on 15/12/2016.
 */

public final class Utils {

    private static final String DEFAULT_FORMAT = "yyyy-MM-dd hh:mm:ss";

    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DEFAULT_FORMAT);

    private Utils() {
    }

    public static long now() {
        return new DateTime(new Date()).getValue();
    }

    public static String[] getAccountsName(AccountManager manager, String accountType) {
        Account[] accounts = (accountType == null)
                ? manager.getAccounts()
                : manager.getAccountsByType(accountType);
        final String[] accountNames = new String[accounts.length];
        for (int i = 0, l = accounts.length; i < l; i++) {
            accountNames[i] = accounts[i].name;
        }
        return accountNames;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

    public static String[] getAccountsName(AccountManager manager) {
        return getAccountsName(manager, null);
    }

    @Nullable
    public static Account getAccountByName(Account[] accounts, String name) {
        for (int i = 0, l = accounts.length; i < l; i++) {
            if (accounts[i].name.equals(name)) {
                return accounts[i];
            }
        }
        return null;
    }


    public static Pair<Integer, Integer> getScreenSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        int height = displaymetrics.heightPixels;
        return new Pair(width, height);
    }

//    public static Drive getDriveService(Context context) {
//        return getDriveService(context, null);
//    }

    public static String formatStringDate(long value) {
        return formatStringDate(value, null);
    }

    public static String formatStringDate(long value, String format) {
        SimpleDateFormat sdf = DATE_FORMAT;
        if (format != null)
            sdf = new SimpleDateFormat(format);
        return sdf.format(value);
    }

    public static String getDeviceName() {
        return Build.MANUFACTURER + " - " + Build.MODEL;
    }

    public static FileInfo createFromDriveFile(File file) {
        Map<String, String> properties = file.getProperties();
        long localId = 0L;
        String localPath = null;
        if (properties != null) {
            String localIdStr = properties.get(FileInfo.LOCAL_ID_FIELD);
            localId = Long.parseLong(localIdStr);
            localPath = properties.get(FileInfo.LOCAL_PATH);
        }
        return new FileInfoBuilder()
                .setDriveId(file.getId())
                .setLocalId(localId)
                .setName(file.getName())
                .setCreatedTime(file.getCreatedTime().getValue())
                .setSize(file.getSize())
                .setMimeType(file.getMimeType())
                .setLocalPath(localPath)
                .setThumbPath(file.getThumbnailLink())
                .setSynced(true)
                .createFileInfo();
    }

    public static Uri getFileInfoUri(FileInfo fileInfo) {
        return getFileInfoUri(Long.toString(fileInfo.localId));
    }

    public static Uri getFileInfoUri(String localId) {
        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI.buildUpon()
                .appendPath(localId)
                .build();
    }

    public static String getGalleryPath() {
        return Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_DCIM + "/";
    }

    public static java.io.File getGalleryFolder() {
        return new java.io.File(Environment.getExternalStorageDirectory(), Environment.DIRECTORY_DCIM);
    }

}
