package home.crsk.drivegallerybackup.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 5/1/2017.
 */
public class FluentMap {

    private Map<String, Object> mMap;

    public FluentMap(Map<String, Object> mMap) {
        this.mMap = mMap;
    }

    public FluentMap() {
        this.mMap = new HashMap<>();
    }

    public static final Map<String, Object> single(String key, Object value) {
        return new FluentMap().put(key, value).get();
    }

    public FluentMap put(String key, Object value) {
        mMap.put(key, value);
        return this;
    }

    public Map<String, Object> get() {
        return mMap;
    }

}
