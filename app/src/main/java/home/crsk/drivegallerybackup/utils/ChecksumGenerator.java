package home.crsk.drivegallerybackup.utils;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import home.crsk.drivegallerybackup.services.StreamHandler;

/**
 * Created by criskey on 21/1/2017.
 */

public final class ChecksumGenerator {

    private StreamHandler mStreamHandler;

    private File mFile;

    private MessageDigest mMessageDigest;

    public ChecksumGenerator(String algorithm, int bufferSize) {
        mStreamHandler = new StreamHandler(bufferSize);
        try {
            mMessageDigest = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public ChecksumGenerator(String algorithm) {
        this(algorithm, 8192);
    }

    public ChecksumGenerator() {
        this("MD5", 8192);
    }

    public String generate(File file) {
        String checksum = null;
        try {
            byte[] bytes = mStreamHandler.read(file, null);
            byte[] digested = mMessageDigest.digest(bytes);
            StringBuilder checksumBuilder = new StringBuilder();
            for (int i = 0; i < digested.length; i++) {
                checksumBuilder.append(Integer.toString((digested[i] & 0xff) + 0x100, 16).substring(1));
            }
            checksum = checksumBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return checksum;
    }
}
