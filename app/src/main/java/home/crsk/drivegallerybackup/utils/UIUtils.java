package home.crsk.drivegallerybackup.utils;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by criskey on 6/1/2017.
 */

public class UIUtils {

    public static void showMultiLineMessage(int lines, View view, String text) {
        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        TextView textView = (TextView) snackbar
                .getView()
                .findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);
        snackbar.show();
    }

    public static void showMultiLineMessage(View view, String text) {
        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        TextView textView = (TextView) snackbar
                .getView()
                .findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);
        snackbar.show();
    }

    public static void showSimpleMessage(View view, String text) {
        Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
    }

    @BindingAdapter(value = {"time", "format"}, requireAll = false)
    public static void bindDateTimeFormat(TextView textView, Long time, String format) {
        if (time == null) {
            throw new IllegalArgumentException("\"time\" attribute is mandatory");
        }
        textView.setText(Utils.formatStringDate(time, null));
    }
}
