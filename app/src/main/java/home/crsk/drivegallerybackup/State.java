package home.crsk.drivegallerybackup;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * Created by criskey on 14/12/2016.
 */

public class State extends BaseObservable {

    private String folderName = "";

    private String folderId;

    private String accountName = "";

    private boolean configured;

    private boolean authenticated;

    private boolean processing;

    private boolean dbCreated;

    private boolean syncing;

    public State() {
    }

    private State(String folderName, String folderId,
                  String accountName, boolean configured,
                  boolean authenticated, boolean processing,
                  boolean dbCreated, boolean syncing) {
        this.folderName = folderName;
        this.folderId = folderId;
        this.accountName = accountName;
        this.configured = configured;
        this.authenticated = authenticated;
        this.processing = processing;
        this.dbCreated = dbCreated;
        this.syncing = syncing;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    @Bindable
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
        notifyPropertyChanged(BR.accountName);
    }

    public boolean isConfigured() {
        return configured;
    }

    public void setConfigured(boolean configured) {
        this.configured = configured;
        setProcessing(false);
        notifyPropertyChanged(BR.status);
    }

    @Bindable
    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
        setProcessing(false);
        notifyPropertyChanged(BR.authenticated);
        notifyPropertyChanged(BR.status);
    }

    @Bindable
    public String getStatus() {
        if (authenticated && configured)
            return "Reset";
        else if (authenticated)
            return "Finish";
        else
            return "Authenticate";
    }

    @Bindable
    public boolean isProcessing() {
        return processing;
    }

    public void setProcessing(boolean processing) {
        this.processing = processing;
        notifyPropertyChanged(BR.processing);
    }

    public boolean isDbCreated() {
        return dbCreated;
    }

    public void setDbCreated(boolean dbCreated) {
        this.dbCreated = dbCreated;
    }

    public static class StateBuilder {

        private String folderName;
        private String folderId;
        private String accountName;
        private boolean configured;
        private boolean authenticated;
        private boolean processing;
        private boolean dbCreated;
        private boolean syncing;

        public StateBuilder setFolderName(String folderName) {
            this.folderName = folderName;
            return this;
        }

        public StateBuilder setFolderId(String folderId) {
            this.folderId = folderId;
            return this;
        }

        public StateBuilder setAccountName(String accountName) {
            this.accountName = accountName;
            return this;
        }

        public StateBuilder setConfigured(boolean configured) {
            this.configured = configured;
            return this;
        }

        public StateBuilder setAuthenticated(boolean authenticated) {
            this.authenticated = authenticated;
            return this;
        }

        public StateBuilder setProcessing(boolean processing) {
            this.processing = processing;
            return this;
        }

        public StateBuilder setDbCreated(boolean dbCreated) {
            this.dbCreated = dbCreated;
            return this;
        }

        public StateBuilder setSyncing(boolean syncing) {
            this.syncing = syncing;
            return this;
        }

        public State createState() {
            return new State(folderName, folderId, accountName, configured, authenticated, processing, dbCreated, syncing);
        }
    }
}
