package home.crsk.drivegallerybackup.async;

import android.os.AsyncTask;

/**
 * Created by criskey on 6/1/2017.
 */

public abstract class ThrowableAsyncTask<Params, Progress, Result>
        extends AsyncTask<Params, Progress, ThrowableAsyncTask.ThrowableResult<Result>> {


    protected ThrowableResult<Result> result(Result result) {
        return new ThrowableResult<>(result);
    }

    protected ThrowableResult<Result> error(Throwable error) {
        return new ThrowableResult<>(error);
    }

    protected ThrowableResult<Result> cancelOnThrow(Throwable error) {
        cancel(false);
        return error(error);
    }

    public void cancel(){
        cancel(true);
    }

    public static class ThrowableResult<R> {
        public final R result;
        public final Throwable error;

        public ThrowableResult(R result, Throwable error) {
            this.result = result;
            this.error = error;
        }

        public ThrowableResult(Throwable error) {
            this(null, error);
        }

        public ThrowableResult(R result) {
            this(result, null);
        }
    }

}
