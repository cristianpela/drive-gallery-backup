package home.crsk.drivegallerybackup.async;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.util.Pair;

import com.google.api.client.http.HttpResponse;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.services.CacheService;
import home.crsk.drivegallerybackup.services.ICacheService;
import home.crsk.drivegallerybackup.services.StreamHandler;
import home.crsk.drivegallerybackup.utils.Utils;

/**
 * Created by criskey on 26/12/2016.
 */

public abstract class AsyncImageDownloader extends ThrowableAsyncTask<FileInfo, Long, Bitmap> {

    public static final long UNKNOWN = 0L;
    public static final long FROM_CLOUD = 1L;
    public static final long FROM_CACHE = 2L;
    public static final long FROM_GALLERY = 3L;

    private static final String TAG = AsyncImageDownloader.class.getSimpleName();
    private final boolean mAllowCache;

    protected long mLoadingSource = UNKNOWN;
    protected Context mContext;
    protected FileInfo mFileInfo;

    protected ICacheService mCacheService;
    protected StreamHandler mStreamHandler;

    public AsyncImageDownloader(Context context, FileInfo fileInfo, boolean allowCache) {
        mContext = context.getApplicationContext();
        mAllowCache = allowCache;
        mFileInfo = fileInfo;
        mStreamHandler = new StreamHandler();
        mCacheService = new CacheService(context, mStreamHandler);
    }


    @Override
    protected ThrowableResult<Bitmap> doInBackground(FileInfo... params) {
        Pair<Integer, Integer> screenSize = Utils.getScreenSize(mContext);
        int width = screenSize.first;
        int height = screenSize.second;
        //try loading from cache
        ICacheService cacheService = new CacheService(mContext);
        if (mAllowCache && cacheService.exists(mFileInfo)) {
            Log.d(TAG, " Loading image driveId " + mFileInfo.name + " from cache");
            try {
                byte[] loadFromCache = cacheService.loadFromCache(mFileInfo);
                publishProgress(0L, 0L, FROM_CACHE);
                return result(getSampleSizedBitmap(loadFromCache, width, height));
            } catch (IOException e) {
                return error(e);
            }
        }
        //try loading from drive and write to cache
        Log.d(TAG, " Loading image driveId " + mFileInfo.driveId + " from cloud");
        try {
            HttpResponse httpResponse = createHttpResponse();
            final long length = httpResponse.getContentLoggingLimit();
            InputStream in = httpResponse.getContent();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            mStreamHandler.write(out, in, new StreamHandler.Tracker() {
                @Override
                public void onTrack(long totalBytesRead) {
                    publishProgress(length, totalBytesRead, FROM_CLOUD);
                }
            });
            byte[] bytes = out.toByteArray();
            Bitmap bitmap = getSampleSizedBitmap(bytes, width, height);
            if (mAllowCache) {
                Log.d(TAG, " Saving image to cache " + mFileInfo.driveId);
                mCacheService.save(ICacheService.NORMAL, mFileInfo, bytes);
            }
            return result(bitmap);
        } catch (IOException e) {
            return error(e);
        }
    }

    private Bitmap getSampleSizedBitmap(byte[] bytes, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
    }

    protected Bitmap getSampleSizedBitmap(String path, int reqWidth, int reqHeight) throws IOException {
        InputStream input = new FileInputStream(path);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(input, null, options);
        input.close();
        //reopen the stream and show scaled photo
        input = new FileInputStream(path);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inMutable = true;
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, options);
        input.close();
        return bitmap;
    }

    //from: https://developer.android.com/training/displaying-bitmaps/load-bitmap.html
    protected int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    @Override
    protected void onProgressUpdate(Long... values) {
        if (mLoadingSource != values[2])
            mLoadingSource = values[2]; // third argument should be the loading source
    }

    public AsyncImageDownloader executeTask() {
        return (AsyncImageDownloader) this.execute(mFileInfo);
    }

    @Override
    public void cancel() {
        mStreamHandler.cancel();
        super.cancel();
    }


    /**
     * This method will be called in background
     *
     * @return
     */
    public abstract HttpResponse createHttpResponse() throws IOException;


}
