package home.crsk.drivegallerybackup.async;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Pair;

import com.google.api.client.http.HttpResponse;
import com.google.api.services.drive.Drive;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.services.CloudService;
import home.crsk.drivegallerybackup.services.GalleryBackupIntentService;
import home.crsk.drivegallerybackup.services.ICacheService;
import home.crsk.drivegallerybackup.sqlite.FileInfoDAO;
import home.crsk.drivegallerybackup.sqlite.GalleryDAO;
import home.crsk.drivegallerybackup.sqlite.SQLiteFileInfoConnection;
import home.crsk.drivegallerybackup.utils.FluentMap;
import home.crsk.drivegallerybackup.utils.Utils;

import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.LOCAL_PATH;

/**
 * Created by criskey on 27/12/2016.
 */

public abstract class SynchronizingPhotoDownloader extends AsyncImageDownloader {

    private Bitmap mSource;

    private File mGalleryFile;

    private AtomicLong mNewLocalId = new AtomicLong();

    private AtomicReference<String> newLocalPath = new AtomicReference<>();

    public SynchronizingPhotoDownloader(Context context, FileInfo fileInfo) {
        super(context, fileInfo, false);
    }

    public SynchronizingPhotoDownloader(Context context, FileInfo fileInfo, Bitmap source) {
        super(context, fileInfo, false);
        this.mSource = source;
        mGalleryFile = new File(Utils.getGalleryFolder(), fileInfo.name);
    }

    @Override
    protected ThrowableResult<Bitmap> doInBackground(FileInfo... params) {
        if (mCacheService.exists(mFileInfo)) { // if is cached, move it from cache to galleryFile
            try {
                mCacheService.move(mFileInfo, mGalleryFile);
            } catch (IOException e) {
                return cancelOnThrow(e);
            }
        } else { // download from cloud or just use the provided bitmap source (if there's one)
            Bitmap bitmap = (mSource == null) ? super.doInBackground(params).result : mSource;
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(mGalleryFile);
                bitmap.compress(getCompressFormat(), 70, out);
            } catch (IOException e) {
                return cancelOnThrow(e);
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        return cancelOnThrow(e);
                    }
                }
            }
        }

        try {
            saveMetadata();
            return null;
        } catch (IOException e) {
            return cancelOnThrow(e);
        }
    }

    @Override
    protected ThrowableResult<Bitmap> result(Bitmap bitmap) {
        //making sure that we change fileInfo in the main thread
        mFileInfo.localPath = mGalleryFile.getAbsolutePath();
        mFileInfo.localId = mNewLocalId.get();
        return super.result(bitmap);
    }

    private Bitmap.CompressFormat getCompressFormat() {
        String mimeType = mFileInfo.mimeType.split("/")[1].toUpperCase();
        Bitmap.CompressFormat format = Bitmap.CompressFormat.valueOf(mimeType);
        return (format != null) ? format : Bitmap.CompressFormat.JPEG;
    }

    //called on background thread
    private void saveMetadata() throws IOException {
        FileInfo newFileInfo = mFileInfo.mutate(new Pair<String, Object>(LOCAL_PATH, mGalleryFile.getAbsolutePath()));
        FileInfoDAO galleryDao = new GalleryDAO(mContext);
        long newLocalId = galleryDao.create(newFileInfo);
        mNewLocalId.compareAndSet(0L, newLocalId);
    }

    @Override
    protected void onPostExecute(ThrowableResult<Bitmap> result) {
        mFileInfo.synced = true;
        mFileInfo.localId = mNewLocalId.get();
        Intent intentService = new Intent(mContext, GalleryBackupIntentService.class)
                .setAction(GalleryBackupIntentService.ACTION_UPDATE)
                .setData(Utils.getFileInfoUri(mFileInfo));
        mContext.startService(intentService);
    }

    @Override
    public HttpResponse createHttpResponse() throws IOException {
        Drive driveService = CloudService.getInstance().getGoogleDriveAPI();
        return driveService.files().get(mFileInfo.driveId).executeMedia();
    }

}
