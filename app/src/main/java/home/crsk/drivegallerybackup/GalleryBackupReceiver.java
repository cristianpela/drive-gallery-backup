package home.crsk.drivegallerybackup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import home.crsk.drivegallerybackup.services.GalleryBackupIntentService;

public class GalleryBackupReceiver extends BroadcastReceiver {
    public GalleryBackupReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(GalleryBackupReceiver.class.getSimpleName(), "Prepare to upload to cloud " + intent.getData());
        Intent intentService = new Intent(context, GalleryBackupIntentService.class)
                .setAction(GalleryBackupIntentService.ACTION_UPLOAD)
                .setData(intent.getData());
        context.startService(intentService);
    }
}
