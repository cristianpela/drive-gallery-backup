package home.crsk.drivegallerybackup.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by criskey on 21/12/2016.
 */

public class GalleryFileRecycleAdapter extends RecyclerView.Adapter<GalleryViewHolder> {

    private ObservableFileInfoEntries mFileInfEntries;

    private OnRecyclerItemPressedListener mItemPressedListener;

    private boolean mCheckVisibility;

    private boolean mCheckAll;

    @ListingType
    private int mListingType;

    public GalleryFileRecycleAdapter(ObservableFileInfoEntries fileInfoEntries,
                                     OnRecyclerItemPressedListener itemPressedListener) {
        this.mFileInfEntries = fileInfoEntries;
        this.mItemPressedListener = itemPressedListener;
        fileInfoEntries.addOnListChangedCallback(new RecyclerOnListChangedCallback(this));
    }

    @Override
    public int getItemViewType(int position) {
        return mListingType;
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, @ListingType int viewType) {
        return GalleryViewHolderFactory.create(parent, viewType);
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {
        holder.bind(mCheckAll, mCheckVisibility, mFileInfEntries.get(position), mItemPressedListener);
    }

    @Override
    public void onViewRecycled(GalleryViewHolder holder) {
        holder.recycle();
    }

    @Override
    public int getItemCount() {
        return mFileInfEntries.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setCheckVisibility(boolean checkVisibility) {
        mCheckVisibility = checkVisibility;
        notifyItemRangeChanged(0, mFileInfEntries.size());
    }

    public void setCheckAll(boolean checkAll) {
        mCheckAll = checkAll;
        notifyItemRangeChanged(0, mFileInfEntries.size());
    }

    public void setListingType(@ListingType int listingType) {
        mListingType = listingType;
    }

    @ListingType
    public int getListingType(){
        return mListingType;
    }

}
