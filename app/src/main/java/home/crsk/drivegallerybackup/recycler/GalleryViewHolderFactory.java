package home.crsk.drivegallerybackup.recycler;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import home.crsk.drivegallerybackup.R;

/**
 * Created by criskey on 16/1/2017.
 */

public class GalleryViewHolderFactory {

    public static GalleryViewHolder create(ViewGroup parent, @ListingType int listingType) {
        GalleryViewHolder galleryViewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (listingType) {
            case ListingType.GRID: {
                galleryViewHolder = new GalleryThumbViewHolder(inflater
                        .inflate(R.layout.item_list_thumb_gallery_backup, parent, false));
                break;
            }
            default: {
                galleryViewHolder = new GalleryListViewHolder(inflater
                        .inflate(R.layout.item_list_gallery_backup, parent, false));
                break;
            }
        }
        return galleryViewHolder;
    }

}
