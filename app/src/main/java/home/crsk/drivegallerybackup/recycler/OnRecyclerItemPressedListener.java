package home.crsk.drivegallerybackup.recycler;

import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * Created by criskey on 21/12/2016.
 */

public interface OnRecyclerItemPressedListener {

    void onItemPressed(FileInfo fileInfo, boolean isLongPressed);

}
