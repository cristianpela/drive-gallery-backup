package home.crsk.drivegallerybackup.recycler;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import home.crsk.drivegallerybackup.R;
import home.crsk.drivegallerybackup.model.FileInfo;

import static home.crsk.drivegallerybackup.recycler.ListingType.GRID;
import static home.crsk.drivegallerybackup.recycler.ListingType.LIST;

/**
 * Created by criskey on 21/12/2016.
 */

public final class RecyclerBindingAdapters {

    private RecyclerBindingAdapters() {
    }


    @BindingAdapter(value = {"items", "pressedListener"})
    public static void bindRecyclerViewAdapter(RecyclerView recyclerView,
                                               ObservableFileInfoEntries observableFileInfoList,
                                               OnRecyclerItemPressedListener itemPressedListener) {
        GalleryFileRecycleAdapter adapter = (GalleryFileRecycleAdapter) recyclerView.getAdapter();
        if (adapter == null) {
            recyclerView.setAdapter(new GalleryFileRecycleAdapter(observableFileInfoList, itemPressedListener));
            Resources res = recyclerView.getContext().getResources();
            bindRecyclerListingType(recyclerView, res.getInteger(R.integer.gallery_listing_type));
        }
    }

    @BindingAdapter(value = {"listingType"})
    public static void bindRecyclerListingType(RecyclerView recyclerView, @ListingType int listingType) {
        GalleryFileRecycleAdapter adapter = (GalleryFileRecycleAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            Context context = recyclerView.getContext();
            if (adapter.getListingType() != listingType) {
                recyclerView.setLayoutManager(listingType == LIST
                        ? new LinearLayoutManager(context)
                        : new GridLayoutManager(context, 3));
                adapter.setListingType(listingType);
            }
        }
    }

    @BindingAdapter(value = {"checkVisibility"})
    public static void bindRecyclerViewCheckVisibility(RecyclerView recyclerView,
                                                       boolean checkVisibility) {
        GalleryFileRecycleAdapter adapter = (GalleryFileRecycleAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.setCheckVisibility(checkVisibility);
        }
    }

    @BindingAdapter(value = {"checkAll"})
    public static void bindRecyclerViewCheckAll(RecyclerView recyclerView,
                                                boolean checkAll) {
        GalleryFileRecycleAdapter adapter = (GalleryFileRecycleAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.setCheckAll(checkAll);
        }
    }

    @BindingAdapter(value = {"onLongClick", "fileInfo"})
    public static void bindLongPressedListener(View view,
                                               final OnRecyclerItemPressedListener itemPressedListener,
                                               final FileInfo fileInfo) {
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                itemPressedListener.onItemPressed(fileInfo, true);
                return true;
            }
        });
    }
}
