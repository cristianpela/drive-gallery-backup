package home.crsk.drivegallerybackup.recycler;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by criskey on 16/1/2017.
 */

@Retention(SOURCE)
@IntDef({ListingType.LIST, ListingType.GRID})
public @interface ListingType {
    int LIST  = 0;
    int GRID = 1;
}
