package home.crsk.drivegallerybackup.recycler;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import home.crsk.drivegallerybackup.databinding.ItemListGalleryBackupBinding;

/**
 * Created by criskey on 16/1/2017.
 */
public class GalleryListViewHolder extends GalleryViewHolder {

    private ItemListGalleryBackupBinding binding;

    public GalleryListViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }

    @Override
    void bind(boolean checkAll, boolean checkVisibility, FileInfoEntry entry,
              OnRecyclerItemPressedListener itemPressedListener) {
        binding.setChecked(checkAll);
        binding.setCheckVisible(checkVisibility);
        binding.setEntry(entry);
        binding.setItemPressedListener(itemPressedListener);
        binding.executePendingBindings();
    }
}
