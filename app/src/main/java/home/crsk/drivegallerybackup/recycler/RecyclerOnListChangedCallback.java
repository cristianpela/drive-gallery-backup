package home.crsk.drivegallerybackup.recycler;

import android.databinding.ObservableList;

/**
 * Created by criskey on 16/1/2017.
 */
class RecyclerOnListChangedCallback extends ObservableList.OnListChangedCallback<ObservableFileInfoEntries> {

    final GalleryFileRecycleAdapter mAdapter;

    public RecyclerOnListChangedCallback(GalleryFileRecycleAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }

    @Override
    public void onChanged(ObservableFileInfoEntries list) {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemRangeChanged(ObservableFileInfoEntries list, int positionStart, int itemCount) {
        mAdapter.notifyItemRangeChanged(positionStart, itemCount);
    }

    @Override
    public void onItemRangeInserted(ObservableFileInfoEntries list, int positionStart, int itemCount) {
        mAdapter.notifyItemRangeInserted(positionStart, itemCount);
    }

    @Override
    public void onItemRangeMoved(ObservableFileInfoEntries list, int fromPosition, int toPosition, int itemCount) {
        mAdapter.notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemRangeRemoved(ObservableFileInfoEntries list, int positionStart, int itemCount) {
        mAdapter.notifyItemRangeRemoved(positionStart, itemCount);
    }

}
