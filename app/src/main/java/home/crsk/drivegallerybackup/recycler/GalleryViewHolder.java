package home.crsk.drivegallerybackup.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by criskey on 16/1/2017.
 */

public abstract class GalleryViewHolder extends RecyclerView.ViewHolder {


    public GalleryViewHolder(View itemView) {
        super(itemView);
    }

    abstract void bind(boolean checkAll,
                       boolean checkVisibility,
                       FileInfoEntry entry,
                       OnRecyclerItemPressedListener itemPressedListener);

    public void recycle(){}
}
