package home.crsk.drivegallerybackup.recycler;

import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * Created by criskey on 16/1/2017.
 */
public class FileInfoEntry {

    public FileInfo fileInfo;
    public boolean checked;

    public FileInfoEntry(FileInfo fileInfo, boolean checked) {
        this.fileInfo = fileInfo;
        this.checked = checked;
    }

    public FileInfoEntry(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
        this.checked = false;
    }


    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileInfoEntry that = (FileInfoEntry) o;

        return fileInfo != null ? fileInfo.equals(that.fileInfo) : that.fileInfo == null;

    }

    @Override
    public int hashCode() {
        return fileInfo != null ? fileInfo.hashCode() : 0;
    }
}
