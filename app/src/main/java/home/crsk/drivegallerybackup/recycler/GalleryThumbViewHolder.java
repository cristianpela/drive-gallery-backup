package home.crsk.drivegallerybackup.recycler;

import android.databinding.DataBindingUtil;
import android.support.annotation.ColorRes;
import android.view.View;

import com.bumptech.glide.Glide;

import java.io.File;

import home.crsk.drivegallerybackup.R;
import home.crsk.drivegallerybackup.databinding.ItemListThumbGalleryBackupBinding;
import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * Created by criskey on 16/1/2017.
 */

public class GalleryThumbViewHolder extends GalleryViewHolder {

    private ItemListThumbGalleryBackupBinding binding;

    public GalleryThumbViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }

    @Override
    void bind(boolean checkAll, boolean checkVisibility,
              FileInfoEntry entry, OnRecyclerItemPressedListener itemPressedListener) {
        binding.setChecked(checkAll);
        binding.setCheckVisible(checkVisibility);
        binding.setEntry(entry);
        binding.setItemPressedListener(itemPressedListener);
        binding.setStatusColor(getStatusColor(entry.fileInfo));
        binding.executePendingBindings();
        Glide.with(binding.getRoot().getContext())
                .load(entry.fileInfo.localPath)
                .placeholder(R.drawable.ic_google_drive_icon_128)
                .skipMemoryCache(true)
                .override(96, 96)
                .centerCrop()
                .into(binding.imageThumb);
    }

    @ColorRes
    private int getStatusColor(FileInfo fileInfo) {
        int color;
        if (fileInfo.pendingUp || fileInfo.pendingUpdate) {
            color = R.color.colorAmber200;
        } else if (fileInfo.synced) {
            color = R.color.colorGreen200;
        } else
            color = R.color.colorRed200;
        return color;
    }

    @Override
    public void recycle() {
        Glide.clear(binding.imageThumb);
    }
}
