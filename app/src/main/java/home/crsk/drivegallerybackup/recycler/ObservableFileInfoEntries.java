package home.crsk.drivegallerybackup.recycler;

import android.databinding.ObservableArrayList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * A naive "set" list
 * Created by criskey on 21/12/2016.
 */
public class ObservableFileInfoEntries extends ObservableArrayList<FileInfoEntry> {


    public ObservableFileInfoEntries() {
    }

    public ArrayList<FileInfo> getData() {
        ArrayList<FileInfo> data = new ArrayList<>(size());
        for (int i = 0, s = size(); i < s; i++) {
            data.add(get(i).fileInfo);
        }
        return data;
    }

    public ArrayList<FileInfo> getCheckedData(){
        ArrayList<FileInfo> data = new ArrayList<>(size());
        for (int i = 0, s = size(); i < s; i++) {
            final FileInfoEntry e = get(i);
            if(e.checked)
                data.add(e.fileInfo);
        }
        return data;
    }

    public void remove(FileInfo fileInfo) {
        FileInfoEntry entry = new FileInfoEntry(fileInfo);
        int position = indexOf(entry);
        remove(position);
    }

    public void remove(Collection<FileInfo> collection) {
        for(FileInfo f: collection){
            remove(f);
        }
    }

    public boolean addAllFileInfo(Collection<? extends FileInfo> collection) {
        filter(collection);
        return super.addAll(createEntriesFromCollection(collection));
    }

    public boolean addAllFileInfo(int index, Collection<? extends FileInfo> collection) {
        filter(collection);
        return super.addAll(index, createEntriesFromCollection(collection));
    }

    public void checkAll(boolean checked) {
        for (int i = 0, s = size(); i < s; i++) {
            FileInfoEntry entry = get(i);
            entry.checked = checked;
            set(i, entry);
        }
    }

    private List<FileInfoEntry> createEntriesFromCollection(Collection<? extends FileInfo> collection) {
        List<FileInfoEntry> entries = new LinkedList<>();
        for (FileInfo f : collection) {
            entries.add(new FileInfoEntry(f));
        }
        return entries;
    }

    private void filter(Collection<? extends FileInfo> collection) {
        //filter  duplicates
        Iterator<? extends FileInfo> it = collection.iterator();
        while (it.hasNext()) {
            FileInfo fileInfo = it.next();
            if (contains(fileInfo)) it.remove();
        }
    }

    public void setFileInfo(int index, FileInfo fileInfo) {
        set(index, new FileInfoEntry(fileInfo));
    }

    public int index(FileInfo mCurrentFileInfo) {
        return indexOf(new FileInfoEntry(mCurrentFileInfo));
    }
}
