package home.crsk.drivegallerybackup.services;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.google.android.gms.auth.GoogleAuthException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import home.crsk.drivegallerybackup.sqlite.CompositePage;
import home.crsk.drivegallerybackup.sqlite.Pageable;
import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.State;
import home.crsk.drivegallerybackup.sqlite.FileInfoDAO;
import home.crsk.drivegallerybackup.sqlite.FileInfoDBConnection;
import home.crsk.drivegallerybackup.sqlite.GalleryDAO;
import home.crsk.drivegallerybackup.sqlite.Page;
import home.crsk.drivegallerybackup.sqlite.SQLiteFileInfoConnection;
import home.crsk.drivegallerybackup.sqlite.Transaction;
import home.crsk.drivegallerybackup.utils.FluentMap;
import home.crsk.drivegallerybackup.utils.Utils;

import static android.os.Build.ID;
import static android.provider.BaseColumns._ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.CREATED_TIME;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.DRIVE_ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.LOCAL_ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.LOCAL_PATH;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.PENDING_DEL;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.PENDING_UP;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.PENDING_UPDATE;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.THUMB_PATH;

/**
 * Created by criskey on 5/1/2017.
 */

public class GalleryBackupService {

    private static final String TAG = GalleryBackupService.class.getSimpleName();

    private ICloudService mCloudService;

    private FileInfoDBConnection mConnection;

    private IStateLoader mStateLoader;

    private FileInfoDAO mDao;

    private FileInfoDAO mGalleryDao;

    private ICacheService mCache;

    public GalleryBackupService(ICloudService mCloudService,
                                FileInfoDBConnection mConnection,
                                IStateLoader IStateLoader,
                                ICacheService cache,
                                FileInfoDAO galleryDao) {
        this.mCloudService = mCloudService;
        this.mConnection = mConnection;
        this.mConnection.open();
        this.mStateLoader = IStateLoader;
        this.mDao = mConnection.getDAO();
        this.mCache = cache;
        this.mGalleryDao = galleryDao;
    }

    public GalleryBackupService(Context context) {
        this(CloudService.getInstance(),
                SQLiteFileInfoConnection.getInstance(),
                StateLoader.getInstance(context),
                new CacheService(context),
                new GalleryDAO(context));
    }

    public void auth(String accountName) throws IOException {
        mCloudService.auth(accountName);
        State state = mStateLoader
                .loadBuilder()
                .setAccountName(accountName)
                .setAuthenticated(true)
                .createState();
        mStateLoader.save(state);
    }

    public void setup(String folderName) throws IOException {
        String folderId = mCloudService.setup(folderName);
        State state = mStateLoader
                .loadBuilder()
                .setFolderId(folderId)
                .setFolderName(folderName)
                .setConfigured(true)
                .createState();
        mStateLoader.save(state);
    }

    void reset() throws IOException, GoogleAuthException {
        mCloudService.reset(mStateLoader.load().getAccountName());
        mCache.clear();
        mStateLoader.reset();
        mDao.deleteAll();
    }


    public int sync() throws IOException {
        return sync(false);
    }

    public int sync(boolean forced) throws IOException {
        State state = mStateLoader.load();
        boolean createDb = !state.isDbCreated() || forced;
        int resolvedPending = 0;
        if (createDb) {
            final List<FileInfo> cloudFiles = mCloudService.fetch(state.getFolderId());
            mConnection.runTransaction(new Transaction.Query<Void>() {
                @Override
                public Void execute(FileInfoDAO dao) {
                    dao.deleteAll();
                    for (int i = 0, s = cloudFiles.size(); i < s; i++) {
                        dao.create(cloudFiles.get(i));
                    }
                    return null;
                }
            });
            state.setDbCreated(true);
            mStateLoader.save(state);
        } else { // just do the  periodic db maintenance
            List<FileInfo> pendingList = mDao.read("WHERE " + PENDING_UP + "=1 OR " +
                    PENDING_DEL + "=1 OR " + PENDING_UPDATE + "=1");
            //filter list by checking the files exist on the device
            for (int i = 0, size = pendingList.size(); i < size; i++) {
                FileInfo fileInfo = pendingList.get(i);
                if (!mGalleryDao.exists(fileInfo) && !mCache.exists(fileInfo)) {
                    fileInfo.pendingDel = true; // doesn't exist on device, mark for deletion
                }
            }
            //order is important. First weed out pending deletes from the list,
            //then do the pending uploads and only then the pending updates
            deletePending(pendingList);
            uploadPending(pendingList);
            updatePending(pendingList);

            resolvedPending = pendingList.size();
            Log.i(TAG, "Sync report: resolved " + pendingList.size() + " pending records with cloud");
        }
        return resolvedPending;
    }

    public Pair<Pageable, List<FileInfo>> fetchFiles(Pageable page) {
        if (CompositePage.class.isInstance(page)) {
            CompositePage compositePage = (CompositePage) page;
            Page galleryPage = compositePage.galleryPage;
            Page fileInfoPage = compositePage.fileInfoPage;

            Set<FileInfo> compositeFetch = new HashSet<>();
            List<FileInfo> galleryFetch = new ArrayList<>();
            List<FileInfo> fileInfoFetch = new ArrayList<>();
            if (galleryPage.hasMore()) {
                Pair<Pageable, List<FileInfo>> galleryFetchResult = mGalleryDao.read(galleryPage);
                galleryPage = (Page) galleryFetchResult.first;
                galleryFetch.addAll(galleryFetchResult.second);
            }
            if (fileInfoPage.hasMore()) {
                Pair<Pageable, List<FileInfo>> fileInfoFetchResult = mDao.read(fileInfoPage);
                fileInfoPage = (Page) fileInfoFetchResult.first;
                fileInfoFetch.addAll(fileInfoFetchResult.second);
            }

            compositeFetch.addAll(fileInfoFetch);
            compositeFetch.addAll(galleryFetch);

            String nextToken = compositePage.encodeToken(galleryPage.getNextToken(), fileInfoPage.getNextToken());
            CompositePage nextCompositePage = (CompositePage) compositePage.next(nextToken);
            return new Pair(nextCompositePage, new ArrayList<>(compositeFetch));
        } else {
            //for now I'm using this workaround until I properlly use the composite page and cursor joiner
            final Pair<Pageable, List<FileInfo>> read = mDao.read(page);
            List<FileInfo> fetched = read.second;
            for (FileInfo f : fetched) {
                //reason: if this file is pending for update or upload user should not be able
                // to press the sync button on UI
                f.synced = (f.pendingUpdate || f.pendingUp) || mGalleryDao.exists(f);
            }
            return read;
        }
    }

    public void release() {
        mConnection.close();
    }

    //**************************UPDATE SECTION**************************

    private void update(FileInfo fileInfo, String folderId) throws IOException, NotFoundOnCloudException {
        FileInfo record = fileInfo;
        record.createdTime = Utils.now();
        try {
            if (mCloudService.exists(fileInfo)) { // update but only if exits on cloud
                mCloudService.update(fileInfo, folderId);
                Log.i(TAG, "Updated to cloud -> " + fileInfo.name);
            } else {
                Log.e(TAG, "Could not update" + record.name + ". reason: doesn't exist on cloud");
                throw new NotFoundOnCloudException(record.name + " doesn't exist on cloud");
            }
        } catch (IOException e) {
            Log.e(TAG, record.name + " could not be updated. Will retry later on next sync cycle");
            record.pendingUpdate = true;
            throw e;
        }
    }

    public void update(final FileInfo fileInfo) throws IOException {
        String folderId = mStateLoader.load().getFolderId();
        try {
            update(fileInfo, folderId);
            mDao.update(fileInfo);
        } catch (IOException e) {
            Log.e(TAG, "Photo updated on database wih pending update: " + fileInfo.toString());
            mDao.update(fileInfo);
            throw e;
        } catch (NotFoundOnCloudException e) {
            e.printStackTrace();
            mDao.delete(fileInfo);
        }
    }


    public void multipleUpdate(List<FileInfo> fileInfoList, Tracker tracker) {
        String folderId = mStateLoader.load().getFolderId();
        final List<FileInfo> localRecords = new ArrayList<>();
        for (int i = 0, s = fileInfoList.size(); i < s; i++) {
            FileInfo fileInfo = fileInfoList.get(i);
            try {
                update(fileInfo, folderId);
            } catch (IOException e) {
                e.printStackTrace();
                fileInfo.pendingUpdate = true;
                if (tracker != null)
                    tracker.onError(i, e);
            } catch (NotFoundOnCloudException e) {
                //is not on cloud the delete it from local db
                fileInfo.pendingDel = true;
                if (tracker != null)
                    tracker.onError(i, e);
            }
            localRecords.add(fileInfo);
        }
        //write to db
        mConnection.runTransaction(new Transaction.Query<Void>() {
            @Override
            public Void execute(FileInfoDAO dao) {
                for (int i = 0, s = localRecords.size(); i < s; i++) {
                    FileInfo fileInfo = localRecords.get(i);
                    if (fileInfo.pendingDel)
                        dao.delete(fileInfo);
                    else
                        dao.update(fileInfo);
                }
                return null;
            }
        });
    }

    private int updatePending(List<FileInfo> pendingUpdate) {
        int resolved = 0;

        final List<FileInfo> updatedToCloud = new ArrayList<>();

        String folderId = mStateLoader.load().getFolderId();
        for (int i = pendingUpdate.size() - 1; i >= 0; i--) {
            FileInfo fileInfo = pendingUpdate.get(i);
            try {
                if (fileInfo.pendingUpdate) {
                    update(fileInfo, folderId);
                    fileInfo.pendingUpdate = false;
                    updatedToCloud.add(fileInfo);
                    resolved++;
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (NotFoundOnCloudException e) {
                // was deleted from cloud in the meantime, so mark for delete from local db
                fileInfo.pendingDel = true;
                e.printStackTrace();
            }
        }

        //update database
        mConnection.runTransaction(new Transaction.Query<Void>() {
            @Override
            public Void execute(FileInfoDAO dao) {
                for (int i = 0, s = updatedToCloud.size(); i < s; i++) {
                    FileInfo fileInfo = updatedToCloud.get(i);
                    if (fileInfo.pendingDel) {
                        dao.delete(fileInfo);
                    } else {
                        dao.update(fileInfo);
                    }
                }
                return null;
            }
        });

        return resolved;
    }

    //**************************UPLOAD SECTION**************************
    private FileInfo upload(FileInfo fileInfo, String folderId) throws IOException, FileExistsOnCloudException {
        FileInfo record = fileInfo;
        try {
            if (!mCloudService.exists(fileInfo)) {
                record = mCloudService.upload(fileInfo, folderId);
                Log.i(TAG, "Uploaded to cloud -> " + record.name);
                return record;
            } else {
                Log.e(TAG, record.name + " already exists on cloud");
                throw new FileExistsOnCloudException(record.name + " already exists on cloud or is prepared to upload");
            }
        } catch (IOException e) {
            Log.e(TAG, record.name + " could not be created. Will retry later on next sync cycle");
            fileInfo.createdTime = Utils.now();
            throw e;
        }
    }

    public FileInfo upload(final FileInfo fileInfo) throws IOException {
        FileInfo uploaded;
        try {
            checkLocalExistence(fileInfo);
            String folderId = mStateLoader.load().getFolderId();
            uploaded = upload(fileInfo, folderId);
            uploaded.id = mDao.create(uploaded);
            Log.i(TAG, "Photo created on cloud and synced with local database: " + uploaded.toString());
            return uploaded;
        } catch (IOException e) {
            Log.e(TAG, "Photo saved on database with pending upload and pending cache: " + fileInfo.toString());
            fileInfo.pendingUp = true; // mark for sync session
            fileInfo.id = mDao.create(fileInfo);
            mCache.save(ICacheService.PENDING, fileInfo);
            throw e;
        } catch (FileExistsOnCloudException e) {
            String message = e.getMessage();
            if (fileInfo.pendingDel) {
                //this scenario applies when a photo was deleted from local db while offline.
                //the same photo was uploaded again before a sync cycle(automatic or manual)
                //so basically now there's no reason to upload it back to cloud (since wasn't deleted from cloud)
                // just remove the pending delete status in the local db
                Log.i(TAG, "Photo " + fileInfo.toString() + " reverted from pending delete.");
                message = "Photo " + fileInfo.name + " reverted from pending delete.";
                fileInfo.pendingUp = true;
                fileInfo.pendingDel = false;
                mDao.update(fileInfo);
            }
            e.printStackTrace();
            throw new IOException(message);
        }
    }

    public int multipleUpload(List<FileInfo> fileInfoList, Tracker tracker) {
        String folderId = mStateLoader.load().getFolderId();
        final List<FileInfo> localRecords = new ArrayList<>();
        for (int i = 0, s = fileInfoList.size(); i < s; i++) {
            FileInfo fileInfo = fileInfoList.get(i);
            try {
                checkLocalExistence(fileInfo);
                fileInfo = upload(fileInfo, folderId);
                if (tracker != null)
                    tracker.onTrack(i, fileInfo);
            } catch (IOException e) {
                e.printStackTrace();
                fileInfo.pendingUp = true;
                if (tracker != null)
                    tracker.onError(i, e);
            } catch (FileExistsOnCloudException e) {
                e.printStackTrace();
                if (tracker != null)
                    tracker.onError(i, e);
            }
            localRecords.add(fileInfo);
        }

        //write to db
        mConnection.runTransaction(new Transaction.Query<Void>() {
            @Override
            public Void execute(FileInfoDAO dao) {
                for (int i = 0, s = localRecords.size(); i < s; i++) {
                    FileInfo fileInfo = localRecords.get(i);
                    if (!fileInfo.pendingDel) {
                        fileInfo.id = dao.create(fileInfo);
                    } else {
                        Log.i(TAG, "Photo " + fileInfo.toString() + " reverted from pending delete.");
                        fileInfo.pendingUp = true;
                        fileInfo.pendingDel = false;
                        dao.update(fileInfo);
                    }
                }
                return null;
            }
        });

        //cache pending
        for (int i = 0, s = localRecords.size(); i < s; i++) {
            try {
                mCache.save(ICacheService.PENDING, localRecords.get(i));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return localRecords.size();
    }

    private void checkLocalExistence(FileInfo fileInfo) throws FileExistsOnCloudException {
        final FileInfo fileInfoLocal = mDao.readOne(LOCAL_ID + "=?",
                Long.toString(fileInfo.localId));
        boolean existsLocally = fileInfoLocal != null;
        //check to see if the file is pending for delete
        if (fileInfoLocal != null) {
            fileInfo.pendingDel = fileInfoLocal.pendingDel;
            if (fileInfo.pendingDel && fileInfo.id == 0) {
                fileInfo.change(fileInfoLocal, _ID, DRIVE_ID, THUMB_PATH, CREATED_TIME, LOCAL_PATH);
            }
        }
        if (existsLocally) {
            throw new FileExistsOnCloudException(fileInfo.name + " already exists on cloud or is prepared to upload");
        }
    }

    private int uploadPending(final List<FileInfo> pendingUploads) {

        int resolved = 0;

        final List<FileInfo> uploadedToCloud = new ArrayList<>();

        String folderId = mStateLoader.load().getFolderId();

        for (int i = pendingUploads.size() - 1; i >= 0; i--) {
            FileInfo fileInfo = pendingUploads.get(i);
            try {
                if (fileInfo.pendingUp) {
                    resolved++;
                    boolean missingFromGallery = !mGalleryDao.exists(fileInfo);
                    if (missingFromGallery) {
                        if (!mCache.exists(fileInfo)) {
                            //mark for delete if the file is not on device anymore
                            fileInfo.pendingDel = true;
                        } else {
                            fileInfo.localPath = mCache.getPath(fileInfo);
                        }
                    }
                    if (!fileInfo.pendingDel) {
                        FileInfo uploaded = upload(fileInfo, folderId);
                        fileInfo.driveId = uploaded.driveId;
                        fileInfo.thumbPath = uploaded.thumbPath;
                        fileInfo.pendingUp = false;
                        if (missingFromGallery) {
                            //move from pending to normal cache directory if there is not gallery anymore
                            fileInfo.localPath = mCache
                                    .move(fileInfo, mCache.getDirectory(ICacheService.NORMAL))
                                    .getAbsolutePath();
                        } else {
                            //we have the file gallery, was succesfull uploaded to cloud, now is safe to delete
                            //from pending cache
                            mCache.delete(fileInfo);
                        }
                    }
                    uploadedToCloud.add(fileInfo);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (FileExistsOnCloudException e) {
                e.printStackTrace();
            }
        }
        //update database
        mConnection.runTransaction(new Transaction.Query<Void>() {
            @Override
            public Void execute(FileInfoDAO dao) {
                for (int i = 0, s = uploadedToCloud.size(); i < s; i++) {
                    FileInfo fileInfo = uploadedToCloud.get(i);
                    if (fileInfo.pendingDel)
                        dao.delete(fileInfo);
                    else
                        dao.update(fileInfo);
                }
                return null;
            }
        });
        return resolved;
    }

    //**************************DELETE SECTION**************************

    private int deletePending(List<FileInfo> pendingDeletes) {
        int resolved = 0;

        final List<FileInfo> deletedFromCloud = new ArrayList<>();

        for (int i = pendingDeletes.size() - 1; i >= 0; i--) {
            FileInfo fileInfo = pendingDeletes.get(i);
            try {
                if (fileInfo.pendingDel) {
                    resolved++;
                    if (mCloudService.exists(fileInfo))
                        mCloudService.delete(fileInfo);
                    deletedFromCloud.add(fileInfo);
                    pendingDeletes.remove(i);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        //update database
        if (!deletedFromCloud.isEmpty())
            mConnection.runTransaction(new Transaction.Query<Void>() {
                @Override
                public Void execute(FileInfoDAO dao) {
                    for (int i = 0, s = deletedFromCloud.size(); i < s; i++) {
                        dao.delete(deletedFromCloud.get(i));
                    }
                    return null;
                }
            });

        return resolved;
    }

    public boolean delete(FileInfo fileInfo) throws IOException {
        try {
            if (mCloudService.exists(fileInfo)) {
                mCloudService.delete(fileInfo);
                mCache.delete(fileInfo);
            }
            mDao.delete(fileInfo);
            return true;
        } catch (IOException e) {
            fileInfo.pendingDel = true;
            mDao.update(fileInfo);
            throw e;
        }
    }

    public void multipleDelete(List<FileInfo> fileInfoList, Tracker tracker) {

        final List<FileInfo> deletedOnCloud = new ArrayList<>();
        final List<FileInfo> deletedFailed = new ArrayList<>();

        //delete on cloud
        for (int i = 0, s = fileInfoList.size(); i < s; i++) {
            FileInfo fileInfo = fileInfoList.get(i);
            boolean deleted = false;
            try {
                if (mCloudService.exists(fileInfo)) {
                    mCloudService.delete(fileInfo);
                }
                if (tracker != null)
                    tracker.onTrack(i, fileInfo);
                deleted = true;
            } catch (IOException e) {
                e.printStackTrace();
                if (tracker != null)
                    tracker.onError(i, e);
            }
            mCache.delete(fileInfo);
            if (deleted)
                deletedOnCloud.add(fileInfo);
            else
                deletedFailed.add(fileInfo);
        }

        //delete from local db
        mConnection.runTransaction(new Transaction.Query<Integer>() {
            @Override
            public Integer execute(FileInfoDAO dao) {
                for (int i = 0, s = deletedOnCloud.size(); i < s; i++) {
                    FileInfo fileInfo = deletedOnCloud.get(i);
                    dao.delete(fileInfo);
                }
                for (int i = 0, s = deletedFailed.size(); i < s; i++) {
                    FileInfo fileInfo = deletedFailed.get(i);
                    fileInfo.pendingDel = true;
                    dao.update(fileInfo);
                }
                return null;
            }
        });
    }

    //******************************************************************

    public interface Tracker {
        void onTrack(int index, FileInfo fileInfo);

        void onError(int index, Exception e);
    }

}
