package home.crsk.drivegallerybackup.services;

/**
 * Created by criskey on 17/1/2017.
 */
public class FileExistsOnCloudException extends Exception {

    public FileExistsOnCloudException() {
    }

    public FileExistsOnCloudException(String detailMessage) {
        super(detailMessage);
    }

    public FileExistsOnCloudException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public FileExistsOnCloudException(Throwable throwable) {
        super(throwable);
    }
}
