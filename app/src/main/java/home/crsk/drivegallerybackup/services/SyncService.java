package home.crsk.drivegallerybackup.services;

import android.content.Context;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import home.crsk.drivegallerybackup.utils.Utils;

/**
 * Created by criskey on 16/1/2017.
 */

public class SyncService extends JobService {

    public static final String JOB_TAG = "google_drive_gallery_backup_sync_job";

    private static final String TAG = SyncService.class.getSimpleName();

    private static int TRIGGER_START = (int) TimeUnit.MINUTES.toSeconds(55);

    private static int TRIGGER_END = (int) TimeUnit.HOURS.toSeconds(1);

    public static final void schedule(Context context) {
        Log.i(TAG, "Scheduling Job: " + JOB_TAG);
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
        Job job = dispatcher.newJobBuilder()
                .setService(SyncService.class)
                .setTag(JOB_TAG)
                .setRecurring(true)
                .setTrigger(Trigger.executionWindow(TRIGGER_START, TRIGGER_END))
                .setLifetime(Lifetime.FOREVER)
                //.setReplaceCurrent(true)
                .build();
        dispatcher.mustSchedule(job);
    }

    public static void cancel(Context context) {
        new FirebaseJobDispatcher(new GooglePlayDriver(context)).cancel(JOB_TAG);
    }

    public static void reschedule(Context context){
        cancel(context);
        schedule(context);
    }

    @Override
    public boolean onStartJob(final JobParameters job) {
        final NotificationHelper notification = new NotificationHelper(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Running scheduled job " + JOB_TAG + " (" + new Date() + ")");
                GalleryBackupService galleryBackupService = new GalleryBackupService(SyncService.this);
                try {
                    if (!Utils.isNetworkAvailable(SyncService.this))
                        throw new ConnectivityException("Device has no internet connection");
                    int resolved = galleryBackupService.sync();
                    notification.infoNotification("Scheduled sync complete for " + resolved + " pending records");
                } catch (IOException e) {
                    e.printStackTrace();
                    notification.errorNotification("Scheduled sync failed: " + e.getMessage());
                } catch (ConnectivityException e) {
                    e.printStackTrace();
                    notification.errorNotification("Scheduled sync failed: " + e.getMessage());
                }
                galleryBackupService.release();
                jobFinished(job, true);
            }
        }).start();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

}
