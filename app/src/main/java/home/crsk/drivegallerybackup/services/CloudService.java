package home.crsk.drivegallerybackup.services;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.accounts.GoogleAccountManager;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.R;
import home.crsk.drivegallerybackup.State;
import home.crsk.drivegallerybackup.utils.Utils;

/**
 * Created by criskey on 5/1/2017.
 */

public final class CloudService implements ICloudService {

    private static final String DRIVE_FILE_FIELDS_METADATA =
            "id, parents, name, createdTime, properties, size, mimeType, thumbnailLink, md5Checksum";

    private static CloudService INSTANCE;

    private Drive mDriveService;

    private Context mContext;

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    public CloudService(Context context) {
        mContext = context.getApplicationContext();
        initDrive();
    }

    public synchronized static void init(Context context) {
        if (INSTANCE == null)
            INSTANCE = new CloudService(context);
        else throw new IllegalStateException("CloudService already initialized");
    }

    public synchronized static CloudService getInstance() {
        if (INSTANCE == null)
            throw new IllegalStateException("CloudService must be initialized first");
        return INSTANCE;
    }

    private void initDrive() {
        State state = StateLoader.getInstance(mContext).load();
        String accountName = state.getAccountName();
        GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(
                mContext, DriveScopes.all())
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(accountName);
        mDriveService = new Drive.Builder(AndroidHttp.newCompatibleTransport(), JacksonFactory.getDefaultInstance(), credential)
                .setApplicationName(mContext.getString(R.string.app_name)).build();
    }

    public Drive getGoogleDriveAPI() {
        return mDriveService;
    }

    @Override
    public void auth(String accountName) throws IOException {
        checkConnection();
        mDriveService.about().get().setFields("user").execute();
    }

    @Override
    public String setup(String folderName) throws IOException {
        checkConnection();
        File fileMetadata = new File();
        fileMetadata.setName(folderName);
        fileMetadata.setMimeType("application/vnd.google-apps.folder");
        File createdFolder = mDriveService.files().create(fileMetadata).setFields("id").execute();
        return createdFolder.getId();
    }

    @Override
    public void reset(String accountName) throws IOException, GoogleAuthException {
        checkConnection();
        GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(
                mContext, DriveScopes.all())
                .setBackOff(null)
                .setSelectedAccountName(accountName);
        GoogleAccountManager manager = credential.getGoogleAccountManager();
        String token = credential.getToken();
        manager.invalidateAuthToken(token);
    }

    @Override
    public FileInfo upload(FileInfo fileInfo, String folderId) throws IOException {
        checkConnection();
        java.io.File filePath = new java.io.File(fileInfo.localPath);
        // custom drive file properties
        Map<String, String> properties = new HashMap<>(2);
        properties.put(FileInfo.LOCAL_ID_FIELD, "" + fileInfo.localId);
        properties.put(FileInfo.LOCAL_PATH, fileInfo.localPath);


        File fileMetadata = new File()
                .setProperties(properties)
                .setName(fileInfo.name)
                //TODO add checksum support.. so in that case properties will be obsolete, we don't need?
                //.setMd5Checksum(null)
                .setParents(Collections.singletonList(folderId));
        FileContent mediaContent = new FileContent(fileInfo.mimeType, filePath);
        File uploaded = mDriveService.files().create(fileMetadata, mediaContent)
                .setFields(DRIVE_FILE_FIELDS_METADATA)
                .execute();
        return Utils.createFromDriveFile(uploaded);
    }

    @Override
    public void update(FileInfo fileInfo, String folderId) throws IOException {
        checkConnection();
        java.io.File filePath = new java.io.File(fileInfo.localPath);
        // custom drive file properties
        Map<String, String> properties = new HashMap<>(2);
        properties.put(FileInfo.LOCAL_ID_FIELD, "" + fileInfo.localId);
        properties.put(FileInfo.LOCAL_PATH, fileInfo.localPath);

        File fileMetadata = new File();
        fileMetadata.setProperties(properties);
        mDriveService.files().update(fileInfo.driveId, fileMetadata)
                .execute();
    }

    @Override
    public void delete(FileInfo fileInfo) throws IOException {
        checkConnection();
        mDriveService.files().delete(fileInfo.driveId).execute();
    }

    @Override
    public boolean exists(FileInfo fileInfo) throws IOException {
        checkConnection();
        String driveId = fileInfo.driveId;
        if (driveId == null)
            return false;
        try {
            mDriveService.files().get(driveId)
                    .execute();
            return true;
        } catch (IOException e) {
            if (e.getMessage().contains("404 Not Found")) { // Not found status
                return false;
            } else
                throw e;
        }
    }

    @Override
    public List<FileInfo> fetch(String folderId) throws IOException {
        checkConnection();
        String pageToken = null;
        int itemsPerPage = 100; //max
        List<FileInfo> filesInfo = new ArrayList<>();
        do {
            FileList fileList = mDriveService.files()
                    .list()
                    .setQ("'" + folderId + "' in parents")
                    .setOrderBy("createdTime desc")
                    .setFields("nextPageToken, files(" + DRIVE_FILE_FIELDS_METADATA + ")")
                    .setPageSize(itemsPerPage)
                    .setPageToken(pageToken)
                    .execute();
            List<File> files = fileList.getFiles();
            for (File f : files) {
                FileInfo fromDriveFile = Utils.createFromDriveFile(f);
                filesInfo.add(fromDriveFile);
            }
            pageToken = fileList.getNextPageToken();
        } while (pageToken != null);
        return filesInfo;
    }

    @Override
    public FileInfo fetchOne(String driveId) throws IOException {
        checkConnection();
        File file = mDriveService.files().get(driveId)
                .setFields(DRIVE_FILE_FIELDS_METADATA)
                .execute();
        return Utils.createFromDriveFile(file);
    }


    private void checkConnection() throws IOException {
        if (!Utils.isNetworkAvailable(mContext))
            throw new IOException("No internet connection!");
    }
}
