package home.crsk.drivegallerybackup.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by criskey on 9/1/2017.
 */

public class StreamHandler {

    private static final int DEFAULT_BUFFER_SIZE = 1024;

    private int mBufferSize;

    private AtomicBoolean mCanceled;

    public StreamHandler(int bufferSize) {
        this.mBufferSize = bufferSize;
        mCanceled = new AtomicBoolean();
    }

    public StreamHandler() {
        this(DEFAULT_BUFFER_SIZE);
    }

    //TODO maybe add tracker
    public void write(File file, byte[] bytes) throws IOException {
        OutputStream output = new FileOutputStream(file);
        output.write(bytes);
        output.close();
    }

    public void write(File file, InputStream input, Tracker tracker) throws IOException {
        read(new FileOutputStream(file), input, tracker);
    }

    public void write(OutputStream output, InputStream input, Tracker tracker) throws IOException {
        read(output, input, tracker);
    }

    public byte[] read(File file, Tracker tracker) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        InputStream input = new FileInputStream(file);
        read(output, input, tracker);
        return output.toByteArray();
    }

    public byte[] read(InputStream input, Tracker tracker) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        read(output, input, tracker);
        return output.toByteArray();
    }

    public void cancel() {
        mCanceled.compareAndSet(false, true);
    }

    public boolean isCanceled() {
        return mCanceled.get();
    }

    private void read(OutputStream output, InputStream input, Tracker tracker) throws IOException {
        try {
            byte[] buffer = new byte[mBufferSize];
            int bytesRead;
            long totalBytesRead = 0L;
            while ((bytesRead = input.read(buffer)) != -1) {
                if (!isCanceled()) {
                    output.write(buffer, 0, bytesRead);
                    totalBytesRead += bytesRead;
                    if (tracker != null)
                        tracker.onTrack(totalBytesRead);
                } else {
                    throw new IOException("Canceled by request");
                }
            }
        } finally {
            output.close();
            input.close();
            reset();
        }
    }

    private void reset() {
        mCanceled.set(false);
    }

    public interface Tracker {
        void onTrack(long totalBytesRead);
    }

    public static final class TrackInfo{

        long size;

        long totalBytesRead;

        int data;

        byte[] buffer;

        private TrackInfo(){}
    }
}
