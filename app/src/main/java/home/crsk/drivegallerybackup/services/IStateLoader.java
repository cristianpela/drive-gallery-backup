package home.crsk.drivegallerybackup.services;

import org.jetbrains.annotations.NotNull;

import home.crsk.drivegallerybackup.State;

/**
 * Created by criskey on 8/1/2017.
 */
public interface IStateLoader {
    State load();

    State.StateBuilder loadBuilder();

    void save(@NotNull State state);

    void reset();
}
