package home.crsk.drivegallerybackup.services;

/**
 * Created by criskey on 18/1/2017.
 */
public class ConnectivityException extends Exception {
    public ConnectivityException() {
    }

    public ConnectivityException(String detailMessage) {
        super(detailMessage);
    }

    public ConnectivityException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ConnectivityException(Throwable throwable) {
        super(throwable);
    }
}
