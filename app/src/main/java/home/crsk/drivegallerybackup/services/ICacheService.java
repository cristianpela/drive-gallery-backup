package home.crsk.drivegallerybackup.services;

import android.support.annotation.StringDef;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Retention;

import home.crsk.drivegallerybackup.model.FileInfo;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by criskey on 7/1/2017.
 */
public interface ICacheService {

    long MAX_STORAGE = 1024 * 10 * 5; // 50MB for now

    String NORMAL = "NORMAL";

    String PENDING = "PENDING";

    NamingStrategy DEFAULT_NAMING_STRATEGY = new NamingStrategy() {
        @Override
        public String name(FileInfo fileInfo) {
            return Long.toString(fileInfo.id);
        }
    };

    boolean delete(FileInfo fileInfo);

    void clear();

    void setNamingStrategy(NamingStrategy namingStrategy);

    void save(@Destination String destination, FileInfo fileInfo, byte[] bytes) throws IOException;

    void save(@Destination String destination, FileInfo fileInfo, InputStream inputStream) throws IOException;

    void save(@Destination String destination, FileInfo fileInfo) throws IOException;

    byte[] loadFromCache(FileInfo fileInfo) throws IOException;

    File move(FileInfo fileInfo, File to) throws IOException;

    boolean exists(FileInfo fileInfo);

    String getPath(@Destination String destination, FileInfo fileInfo);

    String getPath(FileInfo fileInfo);

    File getDirectory(@Destination String destination);

    @Retention(SOURCE)
    @StringDef({NORMAL, PENDING})
    public @interface Destination {
    }

    public interface NamingStrategy {
        public String name(FileInfo fileInfo);
    }
}
