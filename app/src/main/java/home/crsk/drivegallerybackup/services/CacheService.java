package home.crsk.drivegallerybackup.services;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;

import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * Created by criskey on 5/1/2017.
 */

public final class CacheService implements ICacheService {


    private File mCacheDir;

    private File mNormalCacheDir;

    private File mPendingCacheDir;

    private StreamHandler mStreamHandler;

    private NamingStrategy mNamingStrategy;

    public CacheService(Context context) {
        this(context, new StreamHandler());
    }

    public CacheService(Context context, StreamHandler streamHandler) {
        mCacheDir = new File(context.getCacheDir(), "cache_service");
        mStreamHandler = streamHandler;
        initCaches();
    }

    /**
     * In production, make sure that cache dir is not {@link Context#getCacheDir()} but a child of it
     *
     * @param cacheDir
     * @param streamHandler
     */
    public CacheService(File cacheDir, StreamHandler streamHandler) {
        this.mCacheDir = cacheDir;
        this.mStreamHandler = streamHandler;
        initCaches();
    }

    private void initCaches() {
        mNamingStrategy = DEFAULT_NAMING_STRATEGY;
        if (!mCacheDir.exists())
            mCacheDir.mkdir();
        else if (mCacheDir.isFile())
            throw new IllegalStateException("Root cache " + mCacheDir.getName() + " must be a directory");
        mNormalCacheDir = new File(mCacheDir, NORMAL);
        if (!mNormalCacheDir.exists())
            mNormalCacheDir.mkdir();
        mPendingCacheDir = new File(mCacheDir, PENDING);
        if (!mPendingCacheDir.exists())
            mPendingCacheDir.mkdir();
    }

    @Override
    public boolean delete(FileInfo fileInfo) {
        File file = fromCache(fileInfo);
        if (file != null) {
            return file.delete();
        }
        return false;
    }

    @Override
    public void clear() {
        File[] children = mCacheDir.listFiles();
        for (int i = 0; i < children.length; i++) {
            File dir = children[i];
            clear(dir);
            dir.delete();
        }
    }

    @Override
    public void setNamingStrategy(NamingStrategy namingStrategy) {
        mNamingStrategy = namingStrategy == null
                ? DEFAULT_NAMING_STRATEGY
                : namingStrategy;
    }

    //TODO maybe use recursion in the future?
    private void clear(File dir) {
        File[] children = dir.listFiles();
        for (int i = 0; i < children.length; i++) {
            children[i].delete();
        }
    }

    @Override
    public void save(@Destination String destination, FileInfo fileInfo, byte[] bytes) throws IOException {
        if (fileInfo.id == -1)
            throw new IOException("Invalid cache file id: -1");
        ensureSpace();
        File cache = new File(getDirectory(destination), getFileName(fileInfo));
        mStreamHandler.write(cache, bytes);
        assert (cache.exists());
    }

    @Override
    public void save(@Destination String destination, FileInfo fileInfo, InputStream input) throws IOException {
        if (fileInfo.id == -1)
            throw new IOException("Invalid cache file id: -1");
        ensureSpace();
        File cache = new File(getDirectory(destination), getFileName(fileInfo));
        mStreamHandler.write(cache, input, null);
        assert (cache.exists());
    }

    @Override
    public void save(@Destination String destination, FileInfo fileInfo) throws IOException {
        if (fileInfo.id == -1)
            throw new IOException("Invalid cache file id: -1");
        ensureSpace();
        File cache = new File(getDirectory(destination), getFileName(fileInfo));
        InputStream input = new FileInputStream(fileInfo.localPath);
        mStreamHandler.write(cache, input, null);
        assert (cache.exists());
    }

    private void ensureSpace() {
        if (mCacheDir.length() > MAX_STORAGE) {
            clear(mNormalCacheDir);
        }
    }

    @Override
    public byte[] loadFromCache(FileInfo fileInfo) throws IOException {
        File cachedFile = fromCache(fileInfo);
        return mStreamHandler.read(cachedFile, null);
    }

    @Override
    public File move(FileInfo fileInfo, File location) throws IOException {
        assert (fileInfo.id > 0 && fileInfo.driveId != null);
        File to = (location.isDirectory())
                //TODO: is using "name" the best idea? better use a naming strategy interface?
                ? new File(location, getFileName(fileInfo))
                : location;
        File from = fromCache(fileInfo);
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(to).getChannel();
            inputChannel = new FileInputStream(from).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
            from.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }
        return to;
    }

    @Override
    public boolean exists(FileInfo fileInfo) {
        return fromCache(fileInfo) != null;
    }

    @Override
    public String getPath(@Destination String destination, FileInfo fileInfo) {
        File f = new File(getDirectory(destination), getFileName(fileInfo));
        if (f.exists())
            return f.getAbsolutePath();
        return null;
    }

    @Override
    public String getPath(FileInfo fileInfo) {
        String path;
        if ((path = getPath(PENDING, fileInfo)) != null) {
            return path;
        }
        if ((path = getPath(NORMAL, fileInfo)) != null) {
            return path;
        }
        return null;
    }

    @Override
    public File getDirectory(@Destination String destination) {
        if (destination.equals(NORMAL))
            return mNormalCacheDir;
        else
            return mPendingCacheDir;
    }

    private File fromCache(FileInfo fileInfo) {
        String fileName = getFileName(fileInfo);
        File file;
        file = new File(mNormalCacheDir, fileName);
        if (file.exists())
            return file;
        file = new File(mPendingCacheDir, fileName);
        if (file.exists())
            return file;
        return null;
    }

    private String getFileName(FileInfo fileInfo) {
        return mNamingStrategy.name(fileInfo);
    }

}
