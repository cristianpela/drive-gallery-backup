package home.crsk.drivegallerybackup.services;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Pair;

import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import home.crsk.drivegallerybackup.BaseActivity;
import home.crsk.drivegallerybackup.sqlite.Pageable;
import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.GalleryBackupActivity;
import home.crsk.drivegallerybackup.MainActivity;
import home.crsk.drivegallerybackup.sqlite.GalleryDAO;
import home.crsk.drivegallerybackup.utils.Utils;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 */
public class GalleryBackupIntentService extends IntentService {

    //namespace
    private static final String NS = "home.crsk.drivegallerybackup.service";

    public static final String ACTION_UPLOAD = NS + ".action.UPLOAD";
    public static final String ACTION_UPDATE = NS + ".action.UPDATE";
    public static final String ACTION_MULTIPLE_UPLOAD = NS + ".action.MULTIPLE_UPLOAD";
    public static final String ACTION_AUTH = NS + ".action.AUTH";
    public static final String ACTION_SETUP = NS + ".action.SETUP";
    public static final String ACTION_RESET = NS + ".action.RESET";
    public static final String ACTION_SYNC = NS + ".action.SYNC";
    public static final String ACTION_FILES_REQUEST = NS + ".action.FILES_REQUEST";
    public static final String ACTION_FILE_DELETE_REQUEST = NS + ".action.FILE_DELETE_REQUEST";
    public static final String ACTION_MULTIPLE_FILE_DELETE_REQUEST = NS + ".action.MULTIPLE_FILE_DELETE_REQUEST";

    public static final String EXTRA_SYNC_FORCED = NS + ".extra.SYNC_FORCED";
    public static final String EXTRA_SETUP_DIR = NS + ".extra.SETUP_DIR";
    public static final String EXTRA_AUTH_ACCOUNT = NS + ".extra.SETUP_ACCOUNT";
    public static final String EXTRA_NEXT_PAGE = NS + ".extra.NEXT_PAGE";
    public static final String EXTRA_FILE = NS + ".extra.FILE_ID";
    public static final String EXTRA_MULTIPLE_FILES = NS + ".extra.MULTIPLE_FILES";

    private static final String TAG = GalleryBackupIntentService.class.getSimpleName();

    private GalleryBackupService mService;

    private NotificationHelper mNotification;

    public GalleryBackupIntentService() {
        super("GalleryBackupIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            //  init();
            final String action = intent.getAction();
            if (action.equals(ACTION_AUTH)) {
                handleAuth(intent.getStringExtra(EXTRA_AUTH_ACCOUNT));
            } else if (action.equals(ACTION_SETUP)) {
                handleSetup(intent.getStringExtra(EXTRA_SETUP_DIR));
            } else if (action.equals(ACTION_UPLOAD)) {
                handleUpload(intent.getData());
            } else if (action.equals(ACTION_UPDATE)) {
                handleUpdate(intent.getData());
            } else if (action.equals(ACTION_MULTIPLE_UPLOAD)) {
                handleMultipleUpload(intent.getParcelableArrayListExtra(EXTRA_MULTIPLE_FILES));
            } else if (action.equals(ACTION_RESET)) {
                handleReset();
            } else if (action.equals(ACTION_FILES_REQUEST)) {
                handleFetchingFiles((Pageable) intent.getParcelableExtra(EXTRA_NEXT_PAGE));
            } else if (action.equals(ACTION_FILE_DELETE_REQUEST)) {
                handleDelete((FileInfo) intent.getParcelableExtra(EXTRA_FILE));
            } else if (action.equals(ACTION_MULTIPLE_FILE_DELETE_REQUEST)) {
                handleMultipleDelete(intent.<FileInfo>getParcelableArrayListExtra(EXTRA_MULTIPLE_FILES));
            } else if (action.equals(ACTION_SYNC)) {
                handleSync(intent.getBooleanExtra(EXTRA_SYNC_FORCED, false));
            }
            // mService.release();
        }
    }

    @Override
    public void onCreate() {
        init();
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        mService.release();
        super.onDestroy();
    }

    private void init() {
        mService = new GalleryBackupService(this);
        mNotification = new NotificationHelper(this);
    }

    private void handleSync(boolean forced) {
        try {
            if (!Utils.isNetworkAvailable(this))
                throw new ConnectivityException("Gallery Sync: Could not perform synchronization. Device has no internet connection");
            mService.sync(forced);
            mNotification.infoNotification("The app was fully synced with the cloud");
            sendBroadcast(new Intent(GalleryBackupActivity.ACTION_SYNC_RESPONSE));
        } catch (Exception e) {
            e.printStackTrace();
            handleException(e);
        }
    }

    private void handleSetup(String folderName) {
        try {
            mService.setup(folderName);
            mNotification.infoNotification("The app is configured and will upload any new photo to cloud");
            //start the synchronization service
            SyncService.schedule(this);
        } catch (Exception ex) {
            ex.printStackTrace();
            handleException(ex);
        }
    }

    private void handleAuth(String accountName) {
        try {
            mService.auth(accountName);
            sendBroadcast(new Intent(MainActivity.ACTION_AUTH_SUCCESS));
        } catch (Exception ex) {
            handleException(ex);
        }
    }

    private void handleMultipleUpload(ArrayList<Parcelable> uriList) {
        final NotificationCompat.Builder builder = mNotification.
                baseNotificationBuilder(android.R.drawable.ic_dialog_info, "Uploading photos");
        final int size = uriList.size();

        List<FileInfo> fromUri = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            fromUri.add(GalleryDAO.readFromUri(this, (Uri) uriList.get(i)));
        }
        int noOfUploads = mService.multipleUpload(fromUri, new GalleryBackupService.Tracker() {
            @Override
            public void onTrack(int index, FileInfo fileInfo) {
                builder.setProgress(size, index, false)
                        .setContentText((index + 1) + "/" + size + " uploading " + fileInfo.name);
                mNotification.notify(1222, builder);
            }

            @Override
            public void onError(int index, Exception e) {
                builder.setProgress(size, index, false)
                        .setContentText((index + 1) + "/" + size + " " + e.getMessage());
                mNotification.notify(1222, builder);
            }
        });
        sendBroadcast(new Intent(GalleryBackupActivity.ACTION_NEW_MULTIPLE_FILE_UP_RESPONSE)
                .putExtra(GalleryBackupActivity.EXTRA_NEW_MULTIPLE_FILE_UP, noOfUploads));

        builder.setContentText("Upload batch completed")
                .setProgress(0, 0, false);
        mNotification.notify(1222, builder);
    }

    private void handleUpload(Uri uri) {
        try {
            final FileInfo fileInfo = GalleryDAO.readFromUri(this, uri);
            mService.upload(fileInfo);
            mNotification.infoNotification("Photo " + fileInfo.name + " was uploaded to cloud!");
            sendBroadcast(new Intent(GalleryBackupActivity.ACTION_NEW_FILE_UP_RESPONSE));
        } catch (IOException e) {
            e.printStackTrace();
            handleException(e);
        }
    }

    private void handleUpdate(Uri uri) {
        try {
            final FileInfo fileInfo = GalleryDAO.readFromUri(this, uri);
            mService.update(fileInfo);
            Log.i(TAG, "Photo " + fileInfo.name + " was synced with cloud!");
            sendBroadcast(new Intent(GalleryBackupActivity.ACTION_FILE_UPDATE_RESPONSE));
        } catch (IOException e) {
            e.printStackTrace();
            handleException(e);
        }
    }

    private void handleDelete(FileInfo fileInfo) {
        try {
            mService.delete(fileInfo);
            sendBroadcast(new Intent(GalleryBackupActivity.ACTION_FILE_DELETE_RESPONSE));
            mNotification.infoNotification("Photo " + fileInfo.name + " was deleted from cloud!");
        } catch (Exception e) {
            e.printStackTrace();
            sendBroadcast(new Intent(GalleryBackupActivity.ACTION_FILE_DELETE_RESPONSE)
                    .putExtra(GalleryBackupActivity.EXTRA_FILE_DELETE_ERROR, true));
            handleException(e);
        }
    }

    private void handleMultipleDelete(final ArrayList<FileInfo> fileInfoList) {
        final NotificationCompat.Builder builder = mNotification
                .baseNotificationBuilder(android.R.drawable.ic_dialog_info, "Deleting photos");
        final int size = fileInfoList.size();

        mService.multipleDelete(fileInfoList, new GalleryBackupService.Tracker() {
            @Override
            public void onTrack(int index, FileInfo fileInfo) {
                builder.setProgress(size, index, false)
                        .setContentText((index + 1) + "/" + size + " deleting " + fileInfo.name);
                mNotification.notify(1223, builder);
//                sendBroadcast(new Intent(GalleryBackupActivity.ACTION_FILE_DELETE_RESPONSE)
//                        .putExtra(GalleryBackupActivity.EXTRA_FILE_DELETE, fileInfo.driveId));
            }

            @Override
            public void onError(int index, Exception e) {
                builder.setProgress(size, index, false)
                        .setContentText((index + 1) + "/" + size + " " + e.getMessage());
                mNotification.notify(1223, builder);
//                sendBroadcast(new Intent(GalleryBackupActivity.ACTION_FILE_DELETE_RESPONSE)
//                        .putExtra(GalleryBackupActivity.EXTRA_FILE_DELETE, fileInfoList.get(index).driveId)
//                        .putExtra(GalleryBackupActivity.EXTRA_FILE_DELETE_ERROR, true));
//                handleException(e);
            }
        });
        builder.setContentText("Deleting batch completed")
                .setProgress(0, 0, false);
        mNotification.notify(1223, builder);
    }

    private void handleReset() {
        Log.i(TAG, "Resetting account");
        try {
            mService.reset();
            SyncService.cancel(this);
            sendBroadcast(new Intent(MainActivity.ACTION_AUTH_RESET));
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void handleFetchingFiles(Pageable page) {
        Log.i(TAG, "Fetching files from pageToken " + page.getNextToken() + " . " + page.getSize() + " per page");
        Pair<Pageable, List<FileInfo>> result = mService.fetchFiles(page);
        Intent intent = new Intent(GalleryBackupActivity.ACTION_FILES_RESPONSE)
                .putExtra(GalleryBackupActivity.EXTRA_NEXT_PAGE, result.first)
                .putExtra(GalleryBackupActivity.EXTRA_FILES, new ArrayList<>(result.second));
        sendBroadcast(intent);
    }

    private void handleException(Exception e) {
        e.printStackTrace();
        Log.e(TAG, e.getMessage());
        if (e instanceof UserRecoverableAuthIOException) {
            sendBroadcast(new Intent(MainActivity.ACTION_AUTH_ERROR).putExtra(MainActivity.EXTRA_ERROR, e));
        } else {
            mNotification.showNotification(1235, android.R.drawable.ic_dialog_alert, e.getMessage());
            sendBroadcast(new Intent(BaseActivity.ACTION_ERROR).putExtra(BaseActivity.EXTRA_ERROR, e.getMessage()));
        }
    }

}
