package home.crsk.drivegallerybackup.services;

import android.app.NotificationManager;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;

/**
 * Created by criskey on 16/1/2017.
 */

public class NotificationHelper {

    private NotificationManager mManager;

    private Context mContext;

    public NotificationHelper(Context context) {
        mManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mContext = context;

    }

    public NotificationCompat.Builder baseNotificationBuilder(@DrawableRes int icon, String content) {
        return new NotificationCompat.Builder(mContext)
                .setSmallIcon(icon)
                .setContentTitle("Gallery Backup")
                .setContentText(content)
                .setAutoCancel(true);
    }

    public void showNotification(int id, @DrawableRes int icon, String content) {
        NotificationCompat.Builder builder = baseNotificationBuilder(icon, content);
        mManager.notify(id, builder.build());
    }

    public void infoNotification(String message) {
        showNotification(1221, android.R.drawable.ic_dialog_info, message);
    }

    public void errorNotification(String error) {
        showNotification(1221, android.R.drawable.ic_dialog_alert, error);
    }

    public void notify(int id, NotificationCompat.Builder builder) {
        mManager.notify(id, builder.build());
    }

}
