package home.crsk.drivegallerybackup.services;

import com.google.android.gms.auth.GoogleAuthException;

import java.io.IOException;
import java.util.List;

import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * Created by criskey on 5/1/2017.
 */

public interface ICloudService {

    void auth(String accountName) throws IOException;

    /**
     * @param folderName provided name for creation
     * @return the cloud ID of the created folder
     * @throws IOException
     */
    String setup(String folderName) throws IOException;

    void reset(String accountName) throws IOException, GoogleAuthException;

    FileInfo upload(FileInfo fileInfo, String folderId) throws IOException;

    void delete(FileInfo fileInfo) throws IOException;

    void update(FileInfo fileInfo, String folderId) throws IOException;

    boolean exists(FileInfo fileInfo) throws IOException;

    List<FileInfo> fetch(String folderId) throws IOException;

    FileInfo fetchOne(String driveId) throws IOException;

}
