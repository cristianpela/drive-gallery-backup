package home.crsk.drivegallerybackup.services;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.IOException;

/**
 * Created by criskey on 8/1/2017.
 */
public class NotFoundOnDeviceException extends Exception {

    public NotFoundOnDeviceException() {
        super();
    }

    public NotFoundOnDeviceException(String detailMessage) {
        super(detailMessage);
    }

    public NotFoundOnDeviceException(String detailMessage, Throwable cause) {
        super(detailMessage, cause);
    }

    public NotFoundOnDeviceException(Throwable cause) {
        super(cause);
    }

}
