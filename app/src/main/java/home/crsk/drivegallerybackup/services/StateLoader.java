package home.crsk.drivegallerybackup.services;

import android.content.Context;
import android.content.SharedPreferences;

import org.jetbrains.annotations.NotNull;

import home.crsk.drivegallerybackup.State;
import home.crsk.drivegallerybackup.utils.Utils;

/**
 * Created by criskey on 14/12/2016.
 */
public class StateLoader implements IStateLoader {

    private static final String KEY_ACCOUNT_NAME = "KEY_ACCOUNT_NAME";
    private static final String KEY_FOLDER_NAME = "KEY_FOLDER_NAME";
    private static final String KEY_FOLDER_ID = "KEY_FOLDER_ID";
    private static final String KEY_AUTHENTICATED = "KEY_AUTHENTICATED";
    private static final String KEY_CONFIGURED = "KEY_CONFIGURED";
    private static final String KEY_DB_CREATED = "KEY_CREATED";

    private static StateLoader ourInstance = null;

    private SharedPreferences prefs;

    //for testing
    public StateLoader() {
    }

    public synchronized static IStateLoader getInstance(Context context) {
        if (ourInstance == null)
            ourInstance = new StateLoader();
        ourInstance.prefs = context.getSharedPreferences("gallery_backup_state", Context.MODE_PRIVATE);
        return ourInstance;
    }

    @Override
    public State load() {
        return loadBuilder().createState();
    }

    @Override
    public State.StateBuilder loadBuilder(){
        return new State.StateBuilder()
                .setAccountName(prefs.getString(KEY_ACCOUNT_NAME, ""))
                .setFolderName(prefs.getString(KEY_FOLDER_NAME, Utils.getDeviceName() + " Backup"))
                .setFolderId(prefs.getString(KEY_FOLDER_ID, null))
                .setAuthenticated(prefs.getBoolean(KEY_AUTHENTICATED, false))
                .setConfigured(prefs.getBoolean(KEY_CONFIGURED, false))
                .setDbCreated(prefs.getBoolean(KEY_DB_CREATED, false));
    }

    @Override
    public void save(@NotNull State state) {
        SharedPreferences.Editor editor = prefs.edit()
                .putString(KEY_ACCOUNT_NAME, state.getAccountName())
                .putString(KEY_FOLDER_NAME, state.getFolderName())
                .putString(KEY_FOLDER_ID, state.getFolderId())
                .putBoolean(KEY_AUTHENTICATED, state.isAuthenticated())
                .putBoolean(KEY_CONFIGURED, state.isConfigured())
                .putBoolean(KEY_DB_CREATED, state.isDbCreated());
        editor.apply();
    }

    @Override
    public void reset(){
        prefs.edit().clear().apply();
    }
}
