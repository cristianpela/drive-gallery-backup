package home.crsk.drivegallerybackup.services;

/**
 * Created by criskey on 7/1/2017.
 */

public class NotFoundOnCloudException extends Exception {

    public NotFoundOnCloudException() {
        super();
    }

    public NotFoundOnCloudException(String detailMessage) {
        super(detailMessage);
    }

    public NotFoundOnCloudException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public NotFoundOnCloudException(Throwable throwable) {
        super(throwable);
    }
}
