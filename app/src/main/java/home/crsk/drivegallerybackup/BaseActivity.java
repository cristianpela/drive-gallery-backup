package home.crsk.drivegallerybackup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import home.crsk.drivegallerybackup.services.IStateLoader;
import home.crsk.drivegallerybackup.services.StateLoader;
import home.crsk.drivegallerybackup.sqlite.FileInfoDBConnection;
import home.crsk.drivegallerybackup.sqlite.SQLiteFileInfoConnection;

/**
 * Created by criskey on 19/12/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected static final String NS = "home.crsk.drivegallerybackup.activity.";

    public static final String ACTION_ERROR = NS + "action.ERROR";

    public static final String EXTRA_ERROR = NS + "extra.ERROR";

    protected State mState;

    protected IStateLoader mIStateLoader;

    private ActivityReceiver mReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIStateLoader = StateLoader.getInstance(this);
        mState = mIStateLoader.load();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mReceiver = new ActivityReceiver();
        registerReceiver(mReceiver, registerFilterIntent());
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIStateLoader.save(mState);
        unregisterReceiver(mReceiver);
    }

    protected abstract void onReceive(Context context, Intent intent);

    @NotNull
    protected abstract IntentFilter registerFilterIntent();

    public class ActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            BaseActivity.this.onReceive(context, intent);
        }

    }

}
