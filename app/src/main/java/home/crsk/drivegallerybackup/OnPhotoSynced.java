package home.crsk.drivegallerybackup;

import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * Created by criskey on 27/12/2016.
 */
public interface OnPhotoSynced {

    void onPhotoSynced(int index, FileInfo fileInfo);

}
