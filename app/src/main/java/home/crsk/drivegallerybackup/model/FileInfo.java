package home.crsk.drivegallerybackup.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import home.crsk.drivegallerybackup.sqlite.Contract;

import static android.provider.BaseColumns._ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.CREATED_TIME;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.DRIVE_ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.LOCAL_ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.MIME_TYPE;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.NAME;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.PENDING_DEL;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.PENDING_UP;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.PENDING_UPDATE;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.SIZE;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.SYNC;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.THUMB_PATH;

/**
 * Created by criskey on 19/12/2016.
 */

public class FileInfo implements Parcelable, Comparable<FileInfo> {

    public static final String LOCAL_ID_FIELD = "localId";

    public static final String LOCAL_PATH = "localPath";

    public static final Creator<FileInfo> CREATOR = new Creator<FileInfo>() {
        @Override
        public FileInfo createFromParcel(Parcel in) {
            return new FileInfo(in);
        }

        @Override
        public FileInfo[] newArray(int size) {
            return new FileInfo[size];
        }
    };

    @Contract.Column(name = _ID)
    public long id;
    @Contract.Column(name = DRIVE_ID)
    public String driveId;
    @Contract.Column(name = LOCAL_ID)
    public long localId;
    @Contract.Column(name = NAME)
    public String name;
    @Contract.Column(name = CREATED_TIME)
    public long createdTime;
    @Contract.Column(name = MIME_TYPE)
    public String mimeType;
    @Contract.Column(name = SIZE)
    public long size;
    @Contract.Column(name = LOCAL_PATH)
    public String localPath;
    @Contract.Column(name = THUMB_PATH)
    public String thumbPath;
    @Contract.Column(name = PENDING_UP)
    public boolean pendingUp;
    @Contract.Column(name = SYNC)
    public boolean synced;
    @Contract.Column(name = PENDING_DEL)
    public boolean pendingDel;
    @Contract.Column(name = PENDING_UPDATE)
    public boolean pendingUpdate;

    public FileInfo() {
    }

    public FileInfo(long id, String driveId, long localId, String name, long createdTime,
                    String mimeType, long size, String localPath, String thumbPath,
                    boolean synced, boolean pendingUp, boolean pendingDel, boolean pendingUpdate) {
        this.id = id;
        this.driveId = driveId;
        this.localId = localId;
        this.name = name;
        this.createdTime = createdTime;
        this.mimeType = mimeType;
        this.size = size;
        this.localPath = localPath;
        this.thumbPath = thumbPath;
        this.synced = synced;
        this.pendingUp = pendingUp;
        this.pendingDel = pendingDel;
        this.pendingUpdate = pendingUpdate;
    }

    protected FileInfo(Parcel in) {
        id = in.readLong();
        driveId = in.readString();
        localId = in.readLong();
        name = in.readString();
        createdTime = in.readLong();
        mimeType = in.readString();
        size = in.readLong();
        localPath = in.readString();
        thumbPath = in.readString();
        synced = in.readByte() != 0;
        pendingUp = in.readByte() != 0;
        pendingDel = in.readByte() != 0;
        pendingUpdate = in.readByte() != 0;
    }

    public FileInfo mutate(Pair<String, Object>... properties) {
        Map<String, Object> propMap = new HashMap<>();
        for (int i = 0, s = properties.length; i < s; i++) {
            Pair<String, ?> property = properties[i];
            propMap.put(property.first, property.second);
        }
        return mutate(propMap);
    }

    public FileInfo mutate(Map<String, ?> propMap) {
        FileInfo mutated = new FileInfo();
        changeProperties(this, mutated, propMap);
        return mutated;
    }

    public FileInfo change(Pair<String, Object>... properties) {
        Map<String, Object> propMap = new HashMap<>();
        for (int i = 0, s = properties.length; i < s; i++) {
            Pair<String, ?> property = properties[i];
            propMap.put(property.first, property.second);
        }
        return change(propMap);
    }

    public FileInfo change(Map<String, ?> propMap) {
        changeProperties(this, this, propMap);
        return this;
    }

    //overkill method for a just a prop value transfer?
    public FileInfo change(FileInfo from, String... properties) {
        Map propMap = new HashMap();
        for(String p : properties){
            propMap.put(p, null);
        }
        for(Field field : FileInfo.class.getDeclaredFields()){
            Annotation[] annotations = field.getDeclaredAnnotations();
            for(Annotation a: annotations){
                if(a.annotationType().equals(Contract.Column.class)){
                    Contract.Column columnAnn = (Contract.Column) a;
                    if(propMap.containsKey(columnAnn.name())){
                        try {
                            Object fromValue = field.get(from);
                            field.set(this, fromValue);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return this;
    }

    private void changeProperties(FileInfo source, FileInfo to, Map<String, ?> propMap) {
        long id = propMap.containsKey(_ID) ? (Long) propMap.get(_ID) : source.id;
        String driveId = propMap.containsKey(DRIVE_ID) ? (String) propMap.get(DRIVE_ID) : source.driveId;
        long localId = propMap.containsKey(LOCAL_ID) ? (Long) propMap.get(LOCAL_ID) : source.localId;
        long createdTime = propMap.containsKey(CREATED_TIME) ? (Long) propMap.get(CREATED_TIME) : source.createdTime;
        String name = propMap.containsKey(NAME) ? (String) propMap.get(NAME) : source.name;
        String localPath = propMap.containsKey(LOCAL_PATH) ? (String) propMap.get(LOCAL_PATH) : source.localPath;
        String mimeType = propMap.containsKey(MIME_TYPE) ? (String) propMap.get(MIME_TYPE) : source.mimeType;
        long size = propMap.containsKey(SIZE) ? (Long) propMap.get(SIZE) : source.localId;
        String thumbPath = propMap.containsKey(THUMB_PATH) ? (String) propMap.get(THUMB_PATH) : source.thumbPath;
        boolean synced = propMap.containsKey(SYNC) ? (Boolean) propMap.get(SYNC) : source.synced;
        boolean pendingUp = propMap.containsKey(PENDING_UP) ? (Boolean) propMap.get(PENDING_UP) : source.pendingUp;
        boolean pendingDel = propMap.containsKey(PENDING_DEL) ? (Boolean) propMap.get(PENDING_DEL) : source.pendingDel;
        boolean pendingUpdate = propMap.containsKey(PENDING_UPDATE) ? (Boolean) propMap.get(PENDING_UPDATE) : source.pendingUpdate;
        to.id = id;
        to.driveId = driveId;
        to.localId = localId;
        to.createdTime = createdTime;
        to.name = name;
        to.localPath = localPath;
        to.mimeType = mimeType;
        to.size = size;
        to.thumbPath = thumbPath;
        to.synced = synced;
        to.pendingUp = pendingUp;
        to.pendingDel = pendingDel;
        to.pendingUpdate = pendingUpdate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(driveId);
        dest.writeLong(localId);
        dest.writeString(name);
        dest.writeLong(createdTime);
        dest.writeString(mimeType);
        dest.writeLong(size);
        dest.writeString(localPath);
        dest.writeString(thumbPath);
        dest.writeByte((byte) (synced ? 1 : 0));
        dest.writeByte((byte) (pendingUp ? 1 : 0));
        dest.writeByte((byte) (pendingDel ? 1 : 0));
        dest.writeByte((byte) (pendingUpdate ? 1 : 0));
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "id=" + id +
                ", driveId='" + driveId + '\'' +
                ", localId=" + localId +
                ", name='" + name + '\'' +
                ", size='" + size + '\'' +
                ", createdTime=" + createdTime +
                ", mimeType='" + mimeType + '\'' +
                ", localPath='" + localPath + '\'' +
                ", thumbPath='" + thumbPath + '\'' +
                ", synced=" + synced +
                ", pendingUp=" + pendingUp +
                ", pendingDel=" + pendingDel +
                ", pendingUpdate=" + pendingUpdate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileInfo fileInfo = (FileInfo) o;

        boolean eq;
        eq = (id > 0 && fileInfo.id > 0) && (id == fileInfo.id);
        eq |= (localId > 0 && fileInfo.localId > 0) && (localId == fileInfo.localId);
        eq |= driveId != null ? driveId.equals(fileInfo.driveId) : false;

        return eq;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public int compareTo(FileInfo another) {
        return createdTime < another.createdTime ? -1 :
                createdTime > another.createdTime ? 1 : 0;
    }
}
