package home.crsk.drivegallerybackup.model;

public class FileInfoBuilder {

    private long id;
    private String driveId;
    private long localId;
    private String name;
    private long createdTime;
    private long size;
    private String mimeType;
    private String localPath;
    private String thumbPath;
    private boolean synced;
    private boolean pendingUp;
    private boolean pendingDel;
    private boolean pendingUpdate;

    public FileInfoBuilder() {
    }

    public FileInfoBuilder setId(long id) {
        this.id = id;
        return this;
    }

    public FileInfoBuilder setDriveId(String driveId) {
        this.driveId = driveId;
        return this;
    }

    public FileInfoBuilder setLocalId(long localId) {
        this.localId = localId;
        return this;
    }

    public FileInfoBuilder setSize(long size) {
        this.size = size;
        return this;
    }

    public FileInfoBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public FileInfoBuilder setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    public FileInfoBuilder setMimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public FileInfoBuilder setLocalPath(String localPath) {
        this.localPath = localPath;
        return this;
    }

    public FileInfoBuilder setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
        return this;
    }

    public FileInfoBuilder setSynced(boolean synced) {
        this.synced = synced;
        return this;
    }

    public FileInfoBuilder setPendingUp(boolean pendingUp) {
        this.pendingUp = pendingUp;
        return this;
    }

    public FileInfoBuilder setPendingDel(boolean pendingDel) {
        this.pendingDel = pendingDel;
        return this;
    }

    public FileInfoBuilder setPendingUpdate(boolean pendingUpdate) {
        this.pendingUpdate = pendingUpdate;
        return this;
    }

    public FileInfo createFileInfo() {
        return new FileInfo(id, driveId, localId, name, createdTime, mimeType, size, localPath, thumbPath,
                synced, pendingUp, pendingDel, pendingUpdate);
    }
}