package home.crsk.drivegallerybackup.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Pair;

import com.google.api.client.util.DateTime;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.model.FileInfoBuilder;

import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.*;

/**
 * Created by criskey on 2/1/2017.
 */

public class SQLiteFileInfoDAO implements FileInfoDAO {

    private SQLiteDatabase mDb;

    public SQLiteFileInfoDAO(SQLiteDatabase db) {
        this.mDb = db;
        if(!db.isOpen())
            throw new IllegalStateException("Database is closed");
    }

    @Override
    public long create(FileInfo fileInfo) {
        ContentValues cv = getContentValues(fileInfo);
        return mDb.insertOrThrow(TABLE_NAME, null, cv);
    }


    @Override
    public List<FileInfo> read(String where, String... selectionArgs) {
        if (!where.toUpperCase().startsWith("WHERE"))
            where = "WHERE " + where;
        List<FileInfo> result = new LinkedList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " " + where;
        Cursor cursor = mDb.rawQuery(query, selectionArgs);
        if (cursor.moveToFirst()) {
            do {
                result.add(getFileInfoFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    @Override
    public FileInfo readOne(String where, String... selectionArgs) {
        List<FileInfo> one = read(where, selectionArgs);
        return one.isEmpty() ? null : one.get(0);
    }

    /**
     * paging is done by sorting CREATED_TIME
     * nextPageToken basically is CREATED_TIME from last row of the previous page. 0 if nextPageToken is null
     *
     * @param page
     * @return
     */
    @Override
    public Pair<Pageable, List<FileInfo>> read(Pageable page) {
        List<FileInfo> entries = new LinkedList<>();
        long createdTimeFromLastPage = (page.getNextToken().equals(Page.NULL_TOKEN))
                ? Long.MAX_VALUE
                : Long.parseLong(page.getNextToken());
        int size = page.getSize();
        int probingSize = size + 1; // peek to see if there is a next page
        String query = "SELECT * FROM " + TABLE_NAME
                + " WHERE " + CREATED_TIME + "<" + createdTimeFromLastPage
                + " AND " + PENDING_DEL + "=0 "
                + " ORDER BY " + CREATED_TIME
                + " DESC LIMIT " + probingSize;
        Cursor cursor = mDb.rawQuery(query, null);
        //if cursor count is equal to probingSize, this means that
        //there are at least 1 record left over the requested page size, basically at least one more page
        boolean hasMorePages = cursor.getCount() == probingSize;
        if (cursor.moveToFirst()) {
            int rowCount = 0; //real row count ignoring the probing record
            do {
                entries.add(getFileInfoFromCursor(cursor));
            } while (++rowCount < size && cursor.moveToNext());
        }
        cursor.close();
        boolean noMorePages = entries.size() <= size && !hasMorePages;
        Pageable nextPage = (noMorePages) ? page.end() :
                page.next(Long.toString(entries.get(entries.size() - 1).createdTime));
        return new Pair(nextPage, entries);
    }

    @Override
    public int update(FileInfo fileInfo) {
        if (fileInfo.id < 1) { //if invalid, try to get the id from db first
            if (fileInfo.driveId != null) {
                FileInfo readById = readOne(DRIVE_ID + "=?", new String[]{fileInfo.driveId});
                fileInfo.id = readById.id;
            } else {
                throw new IllegalStateException("When updating a fileInfo, " +
                        "it should have valid id with an optionally valid driveId. " +
                        "Current: ID=" + fileInfo.id + " and DriveID=" + fileInfo.driveId);
            }
        }
        fileInfo.createdTime = new DateTime(new Date()).getValue();
        ContentValues cv = getContentValues(fileInfo);
        return mDb.update(TABLE_NAME, cv, _ID + "=?", new String[]{Long.toString(fileInfo.id)});
    }

    @Override
    public int deleteAll() {
        return mDb.delete(TABLE_NAME, null, null);
    }

    @Override
    public int delete(FileInfo fileInfo) {
        long id = fileInfo.id;
        if (id <= 0)
            throw new IllegalStateException("File Info databse id " + id + " + is invalid");
        return mDb.delete(TABLE_NAME, _ID + "=?", new String[]{Long.toString(id)});
    }

    @Override
    public int count() {
        Cursor cursor = mDb.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);
        int count = 0;
        if (cursor.moveToFirst())
            count = cursor.getInt(0);
        cursor.close();
        return count;
    }

    @Override
    public boolean exists(FileInfo fileInfo) {
        String query;
        if (fileInfo.id > 0)
            query = "SELECT " + _ID + " FROM " + TABLE_NAME + " WHERE " + _ID + "=" + fileInfo.id
                    + " AND " + PENDING_DEL + "=0" ;
        else
            query = "SELECT " + _ID + " FROM " + TABLE_NAME + " WHERE " + LOCAL_ID + "=" + fileInfo.localId
                    + " AND " + PENDING_DEL + "=0" ;
        Cursor cursor = mDb.rawQuery(query, null);
        boolean exists = cursor.getCount() > 0;
        cursor.close();
        return exists;
    }

    //------Helper methods;--------
    @NonNull
    private ContentValues getContentValues(FileInfo fileInfo) {
        ContentValues cv = new ContentValues();
        cv.put(DRIVE_ID, fileInfo.driveId);
        cv.put(LOCAL_ID, fileInfo.localId);
        cv.put(NAME, fileInfo.name);
        cv.put(CREATED_TIME, fileInfo.createdTime);
        cv.put(MIME_TYPE, fileInfo.mimeType);
        cv.put(LOCAL_PATH, fileInfo.localPath);
        cv.put(THUMB_PATH, fileInfo.thumbPath);
        cv.put(SYNC, fileInfo.synced ? 1 : 0);
        cv.put(PENDING_UP, fileInfo.pendingUp ? 1 : 0);
        cv.put(PENDING_DEL, fileInfo.pendingDel ? 1 : 0);
        cv.put(PENDING_UPDATE, fileInfo.pendingUpdate ? 1 : 0);
        return cv;
    }

    private FileInfo getFileInfoFromCursor(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(_ID));
        long localId = cursor.getLong(cursor.getColumnIndex(LOCAL_ID));
        String driveId = cursor.getString(cursor.getColumnIndex(DRIVE_ID));
        String localPath = cursor.getString(cursor.getColumnIndex(LOCAL_PATH));
        String thumbPath = cursor.getString(cursor.getColumnIndex(THUMB_PATH));
        String name = cursor.getString(cursor.getColumnIndex(NAME));
        String mimeType = cursor.getString(cursor.getColumnIndex(MIME_TYPE));
        long createdTime = cursor.getLong(cursor.getColumnIndex(CREATED_TIME));
        boolean synced = cursor.getInt(cursor.getColumnIndex(SYNC)) == 1;
        boolean pendingUp = cursor.getInt(cursor.getColumnIndex(PENDING_UP)) == 1;
        boolean pendingDel = cursor.getInt(cursor.getColumnIndex(PENDING_DEL)) == 1;
        boolean pendingUpdate = cursor.getInt(cursor.getColumnIndex(PENDING_UPDATE)) == 1;
        return new FileInfoBuilder()
                .setId(id)
                .setDriveId(driveId)
                .setLocalId(localId)
                .setName(name)
                .setCreatedTime(createdTime)
                .setMimeType(mimeType)
                .setLocalPath(localPath)
                .setThumbPath(thumbPath)
                .setSynced(synced)
                .setPendingUp(pendingUp)
                .setPendingDel(pendingDel)
                .setPendingUpdate(pendingUpdate)
                .createFileInfo();
    }

}
