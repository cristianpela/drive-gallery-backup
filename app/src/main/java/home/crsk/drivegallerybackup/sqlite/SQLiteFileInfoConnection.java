package home.crsk.drivegallerybackup.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by criskey on 4/1/2017.
 */

public class SQLiteFileInfoConnection implements FileInfoDBConnection {

    private static final String TAG = SQLiteFileInfoConnection.class.getSimpleName();

    private static final Object lock = new Object();

    private static SQLiteFileInfoConnection INSTANCE;

    private SQLiteFileInfoDBHelper mDbHelper;
    private Transaction mTransaction;
    private FileInfoDAO mDao;
    private SQLiteDatabase mDb;

    private AtomicInteger mDbUseCount = new AtomicInteger();

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    public SQLiteFileInfoConnection(Context context, String database, int version) {
        mDbHelper = new SQLiteFileInfoDBHelper(context, database, version);
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    public SQLiteFileInfoConnection(Context context, String database) {
        mDbHelper = new SQLiteFileInfoDBHelper(context, database);
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    public SQLiteFileInfoConnection(Context context) {
        mDbHelper = new SQLiteFileInfoDBHelper(context);
    }

    public static SQLiteFileInfoConnection getInstance() {
        synchronized (lock) {
            if (INSTANCE == null)
                throw new IllegalStateException("Must call init first");
        }
        return INSTANCE;
    }

    public static void init(Context context) {
        synchronized (lock) {
            if (INSTANCE != null)
                throw new IllegalStateException("Connection already initialized");
            else
                INSTANCE = new SQLiteFileInfoConnection(context);
        }
    }

    @Override
    public void open() {
        if (mDbUseCount.getAndIncrement() == 0) {
            Log.w(TAG, "Opening database from thread: " + Thread.currentThread());
            mDb = mDbHelper.getWritableDatabase();
            mDao = new SQLiteFileInfoDAO(mDb);
            mTransaction = new SQLiteTransaction(mDao, mDb);
        } else {
            Log.w(TAG, "[" + Thread.currentThread() + "]. Database already opened. Usage: " + mDbUseCount.get());
        }
    }

    @Override
    public void close() {
        if (mDbUseCount.decrementAndGet() <= 0) {
            mDbUseCount.compareAndSet(mDbUseCount.get(), 0);// forced reset
            Log.w(TAG, "Warning closing database![" + Thread.currentThread() + "]");
            mDbHelper.close();
        } else
            Log.e(TAG, "[" + Thread.currentThread() + "] Can't close; database is still in use by other threads." +
                    " Usage:" + mDbUseCount.get());
    }

    @Override
    public FileInfoDAO getDAO() {
        checkDbOpen();
        return mDao;
    }

    private void checkDbOpen() {
        if (mDb == null || !mDb.isOpen())
            throw new IllegalStateException("Database is closed. Make sure you've called open() first");
    }

    @Override
    public <Result> Result runTransaction(Transaction.Query<Result> query) {
        checkDbOpen();
        if (mDb.inTransaction()) {
            throw new IllegalStateException("Database is already in transaction");
        }
        return mTransaction.execute(query);
    }


    private static class SQLiteTransaction extends Transaction {

        private SQLiteDatabase mDb;

        public SQLiteTransaction(FileInfoDAO dao, SQLiteDatabase db) {
            super(dao);
            this.mDb = db;
            if (!db.isOpen())
                throw new IllegalStateException("Database is closed");
        }

        @Override
        public void setTransactionSuccessful() {
            mDb.setTransactionSuccessful();
        }

        @Override
        public void beginTranscation() {
            mDb.beginTransaction();
        }

        @Override
        public void endTransaction() {
            mDb.endTransaction();
        }
    }

}
