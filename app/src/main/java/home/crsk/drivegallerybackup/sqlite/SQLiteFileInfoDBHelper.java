package home.crsk.drivegallerybackup.sqlite;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.*;

/**
 * Created by criskey on 2/1/2017.
 */

public class SQLiteFileInfoDBHelper extends SQLiteOpenHelper {

    //@formatter:off
    private static final String CREATION_SCRIPT = "CREATE TABLE " + TABLE_NAME + "("
            + _ID               + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DRIVE_ID          + " TEXT, "
            + LOCAL_ID          + " INTEGER, "
            + NAME              + " TEXT NOT NULL, "
            + CREATED_TIME      + " INTEGER NOT NULL, "
            + SIZE              + " INTEGER DEFAULT 0, "
            + MIME_TYPE         + " TEXT NOT NULL, "
            + LOCAL_PATH        + " TEXT, "
            + THUMB_PATH        + " TEXT, "
            + SYNC              + " INTEGER DEFAULT 1, "
            + PENDING_UP        + " INTEGER DEFAULT 0, "
            + PENDING_DEL       + " INTEGER DEFAULT 0, "
            + PENDING_UPDATE    + " INTEGER DEFAULT 0"
            +");";
    //@formatter:on


    public SQLiteFileInfoDBHelper(Context context) {
        this(context, Contract.DATABASE_NAME);
    }


    public SQLiteFileInfoDBHelper(Context context, String database, int version) {
        super(context, database, null, version);
    }

    public SQLiteFileInfoDBHelper(Context context, String database) {
        this(context, database, Contract.DATABASE_VERSION);
    }

    public SQLiteFileInfoDBHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, Contract.DATABASE_NAME, factory, Contract.DATABASE_VERSION);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SQLiteFileInfoDBHelper(Context context, SQLiteDatabase.CursorFactory factory,
                                  DatabaseErrorHandler errorHandler) {
        super(context, Contract.DATABASE_NAME, factory, Contract.DATABASE_VERSION, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATION_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onDatabaseVersionChanged(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onDatabaseVersionChanged(db);
    }

    private void onDatabaseVersionChanged(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
