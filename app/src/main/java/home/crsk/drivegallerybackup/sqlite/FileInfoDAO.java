package home.crsk.drivegallerybackup.sqlite;

import android.util.Pair;

import java.util.List;

import home.crsk.drivegallerybackup.model.FileInfo;

/**
 * Created by criskey on 2/1/2017.
 */

public interface FileInfoDAO {

    long create(FileInfo fileInfo);

    /**
     * Returns results (in a quantity give by suze)starting from the specified pageToken
     * @param page
     * @return a pair of next page and result collection.
     */
    Pair<Pageable, List<FileInfo>> read(Pageable page);

    List<FileInfo> read(String where, String... selectionArgs);

    FileInfo readOne(String where, String... selectionArgs);

    int update(FileInfo fileInfo);

    int deleteAll();

    int delete(FileInfo fileInfo);

    int count();

    boolean exists(FileInfo fileInfo);


}
