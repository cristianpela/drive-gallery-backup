package home.crsk.drivegallerybackup.sqlite;

import android.provider.BaseColumns;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by criskey on 2/1/2017.
 */

public interface Contract {

    String DATABASE_NAME = "file_info.db";

    int DATABASE_VERSION = 2;

    //@formatter:off
    interface FileInfoTable extends BaseColumns {
        String TABLE_NAME       = "file_info_table";
        String DRIVE_ID         = "driveId";
        String LOCAL_ID         = "localId";
        String NAME             = "name";
        String CREATED_TIME     = "createdTime";
        String MIME_TYPE        = "mimeType";
        String SIZE             = "size";
        String LOCAL_PATH       = "localPath";
        String THUMB_PATH       = "thumbnailLink";
        String SYNC             = "sync";
        String PENDING_UP       = "pendingUp";
        String PENDING_DEL      = "pendingDel";
        String PENDING_UPDATE   = "pendingUpdate";
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @interface Column{
        String name();
    }
}
