package home.crsk.drivegallerybackup.sqlite;

import android.os.Parcelable;

/**
 * Created by criskey on 12/1/2017.
 */
public interface Pageable extends Parcelable {

    Pageable next(String token);

    Pageable end();

    int getSize();

    void setSize(int size);

    boolean hasMore();

    String getNextToken();
}
