package home.crsk.drivegallerybackup.sqlite;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Pair;

import com.google.api.client.util.DateTime;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.model.FileInfoBuilder;
import home.crsk.drivegallerybackup.utils.Utils;

/**
 * Created by criskey on 11/1/2017.
 */

public class GalleryDAO implements FileInfoDAO {


    private static final Uri EXTERNAL_CONTENT_URI = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

    //hijacking description field for drive id
    //TODO let description field to accept checksum too. The format should be <checksum>|<drive-id>?
    private static final String MEDIA_STORE_IMAGES_MEDIA_DRIVE_ID = MediaStore.Images.Media.DESCRIPTION;

    private static final String TAG = GalleryDAO.class.getSimpleName();

    private String[] mProjection;

    private ContentResolver mContentResolver;

    public GalleryDAO(Context context) {
        mContentResolver = context.getContentResolver();
        mProjection = new String[]{
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.MIME_TYPE,
                MEDIA_STORE_IMAGES_MEDIA_DRIVE_ID,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.DATE_ADDED};
    }

    public static FileInfo readFromUri(Context context, Uri uri) {
        GalleryDAO dao = new GalleryDAO(context);
        return dao.readOne(MediaStore.Images.Media._ID + "=?", new String[]{"" + ContentUris.parseId(uri)});
    }

    @Override
    public long create(FileInfo fileInfo) {

        long createdTime = Utils.now();

        ContentValues cv = new ContentValues();
        cv.put(MediaStore.Images.Media.TITLE, fileInfo.name);
        cv.put(MediaStore.Images.Media.DISPLAY_NAME, fileInfo.name);
        cv.put(MediaStore.Images.Media.DATE_ADDED, createdTime);
        cv.put(MediaStore.Images.Media.DATE_TAKEN, createdTime);
        cv.put(MediaStore.Images.Media.DATE_MODIFIED, createdTime);
        cv.put(MediaStore.Images.Media.MIME_TYPE, fileInfo.mimeType);
        cv.put(MEDIA_STORE_IMAGES_MEDIA_DRIVE_ID, fileInfo.driveId);
        cv.put(MediaStore.Images.Media.ORIENTATION, 0);
        cv.put(MediaStore.Images.Media.DATA, fileInfo.localPath);
        cv.put(MediaStore.Images.Media.SIZE, fileInfo.size);

        Uri uri = mContentResolver.insert(EXTERNAL_CONTENT_URI, cv);
        return ContentUris.parseId(uri);
    }

    @Override
    public Pair<Pageable, List<FileInfo>> read(Pageable page) {
        List<FileInfo> entries = new LinkedList<>();
        long createdTimeFromLastPage = (page.getNextToken().equals(Page.NULL_TOKEN))
                ? Long.MAX_VALUE
                : Long.parseLong(page.getNextToken());
        int size = page.getSize();
        int probingSize = size + 1; // peek to see if there is a next page
        String where = MediaStore.Images.Media.DATE_ADDED + "<?";
        String order = MediaStore.Images.Media.DATE_ADDED + " DESC LIMIT " + probingSize;
        Cursor cursor = mContentResolver.query(EXTERNAL_CONTENT_URI,
                mProjection,
                where, new String[]{Long.toString(createdTimeFromLastPage)},
                order);
        //if cursor count is equal to probingSize, this means that
        //there are at least 1 record left over the requested page size, basically at least one more page
        boolean hasMorePages = cursor.getCount() == probingSize;
        if (cursor.moveToFirst()) {
            int rowCount = 0; //real row count ignoring the probing record
            do {
                entries.add(getFileInfoFromCursor(cursor));
            } while (++rowCount < size && cursor.moveToNext());
        }
        cursor.close();
        boolean noMorePages = (entries.size() <= size) && !hasMorePages;
        Pageable nextPage = (noMorePages) ? page.end() :
                page.next(Long.toString(entries.get(entries.size() - 1).createdTime));
        return new Pair(nextPage, entries);
    }

    @Override
    public List<FileInfo> read(String where, String... selectionArgs) {
        Cursor cursor = mContentResolver.query(EXTERNAL_CONTENT_URI, mProjection, where, selectionArgs, null);
        List<FileInfo> result = new LinkedList<>();
        if (cursor.moveToFirst()) {
            do {
                result.add(getFileInfoFromCursor(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    @Override
    public FileInfo readOne(String where, String... selectionArgs) {
        Cursor cursor = mContentResolver.query(EXTERNAL_CONTENT_URI, mProjection, where, selectionArgs, null);
        FileInfo fileInfo = null;
        if (cursor.moveToFirst()) {
            fileInfo = getFileInfoFromCursor(cursor);
        }
        cursor.close();
        return fileInfo;
    }

    @Override
    public int update(FileInfo fileInfo) {
        Log.w(TAG, "update not implemented");
        return 0;
    }

    @Override
    public int deleteAll() {
        Log.w(TAG, "deleteAll not implemented");
        return 0;
    }

    @Override
    public int delete(FileInfo fileInfo) {
        Log.w(TAG, "delete not implemented");
        return 0;
    }

    @Override
    public int count() {
        Cursor cursor = mContentResolver.query(EXTERNAL_CONTENT_URI,
                new String[]{"COUNT(*) AS count"}, null, null, null);
        int count = 0;
        if (cursor.moveToFirst())
            count = cursor.getInt(0);
        cursor.close();
        return count;
    }

    @Override
    public boolean exists(FileInfo fileInfo) {
        Uri uri = ContentUris
                .appendId(EXTERNAL_CONTENT_URI.buildUpon(), fileInfo.localId)
                .build();
        Cursor cursor = mContentResolver.query(uri, new String[]{MediaStore.MediaColumns._ID}, null, null, null);
        boolean exists = false;
        if (cursor != null)
            exists = cursor.moveToFirst();
        cursor.close();
        return exists;
    }

    private FileInfo getFileInfoFromCursor(Cursor cursor) {
        long localId = cursor.getLong(cursor.getColumnIndex(mProjection[0]));
        String localPath = cursor.getString(cursor.getColumnIndex(mProjection[1]));
        String name = cursor.getString(cursor.getColumnIndex(mProjection[2]));
        String mimeType = cursor.getString(cursor.getColumnIndex(mProjection[3]));
        String driveId = cursor.getString(cursor.getColumnIndex(mProjection[4]));
        long size = cursor.getLong(cursor.getColumnIndex(mProjection[5]));
        long createdTime = cursor.getLong(cursor.getColumnIndex(mProjection[6]));
        return new FileInfoBuilder()
                .setDriveId(driveId)
                .setLocalId(localId)
                .setName(name)
                .setCreatedTime(createdTime)
                .setMimeType(mimeType)
                .setLocalPath(localPath)
                .setSize(size)
                .createFileInfo();
    }
}
