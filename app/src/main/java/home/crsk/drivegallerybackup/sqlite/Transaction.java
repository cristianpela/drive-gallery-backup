package home.crsk.drivegallerybackup.sqlite;

/**
 * Created by criskey on 2/1/2017.
 */
public abstract class Transaction {

    private FileInfoDAO mDao;

    public Transaction(FileInfoDAO dao) {
        this.mDao = dao;
    }

    public <R> R execute(Query<R> query) {
        R result;
        try {
            beginTranscation();
            result = query.execute(mDao);
            setTransactionSuccessful();
        } finally {
            endTransaction();
        }
        return result;
    }

    public abstract void setTransactionSuccessful();

    public abstract void beginTranscation();

    public abstract void endTransaction();

    public interface Query<R> {
        R execute(FileInfoDAO dao);
    }

}
