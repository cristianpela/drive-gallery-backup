package home.crsk.drivegallerybackup.sqlite;

/**
 * Created by criskey on 4/1/2017.
 */

public interface FileInfoDBConnection {

    void open();

    void close();

    FileInfoDAO getDAO();

    <Result> Result runTransaction(Transaction.Query<Result> query);
}
