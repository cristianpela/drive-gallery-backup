package home.crsk.drivegallerybackup.sqlite;

import android.os.Parcel;

import java.util.UUID;

/**
 * Created by criskey on 5/1/2017.
 */
public class Page implements Pageable {

    public static final String NULL_TOKEN = UUID.randomUUID().toString();

    public static final Creator<Pageable> CREATOR = new Creator<Pageable>() {
        @Override
        public Pageable createFromParcel(Parcel in) {
            return new Page(in);
        }

        @Override
        public Pageable[] newArray(int size) {
            return new Pageable[size];
        }
    };

    //@formatter:off
    private final String nextToken;
    private final String lastToken;

    private int size;

    public Page(int size) {
        this(null, NULL_TOKEN, size);
    }

    private Page(String currentToken, String nextToken, int size) {
        this.lastToken = currentToken;
        this.nextToken = nextToken;
        this.size = size;
    }

    protected Page(Parcel in) {
        lastToken = in.readString();
        nextToken = in.readString();
        size = in.readInt();
    }

    @Override public Pageable next(String token) {
        return new Page(this.nextToken, token, size);
    }

    @Override public Pageable end() {
        return new Page(nextToken, nextToken, size);
    }

    @Override public int getSize() {
        return size;
    }

    @Override public void setSize(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("size <= 0");
        }
        this.size = size;
    }

    @Override public boolean hasMore() {
        if (nextToken != null)
            return !nextToken.equals(lastToken);
        else if (lastToken != null)
            return !lastToken.equals(nextToken);
        return true;
    }

    @Override
    public String getNextToken() {
        return nextToken;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(lastToken);
        dest.writeString(nextToken);
        dest.writeInt(size);
    }

    @Override
    public String toString() {
        return "Page{" +
                "lastToken='" + lastToken + '\'' +
                ", nextToken='" + nextToken + '\'' +
                ", size=" + size +
                '}';
    }
}
