package home.crsk.drivegallerybackup.sqlite;

import android.os.Parcel;
import android.support.annotation.NonNull;


/**
 * Created by criskey on 12/1/2017.
 */

public class CompositePage implements Pageable {

    public static final Creator<Pageable> CREATOR = new Creator<Pageable>() {
        @Override
        public Pageable createFromParcel(Parcel in) {
            return new CompositePage(in);
        }

        @Override
        public Pageable[] newArray(int size) {
            return new Pageable[size];
        }
    };

    public final Page galleryPage;
    public final Page fileInfoPage;
    private int size;

    public CompositePage(Page galleryPage, Page fileInfoPage, int size) {
        this.fileInfoPage = fileInfoPage;
        this.galleryPage = galleryPage;
        this.size = size;
    }

    public CompositePage(int size) {
        this.size = size;
        galleryPage = new Page(size);
        fileInfoPage = new Page(size);
    }

    protected CompositePage(Parcel in) {
        galleryPage = in.readParcelable(Page.class.getClassLoader());
        fileInfoPage = in.readParcelable(Page.class.getClassLoader());
        size = in.readInt();
    }

    @Override
    public Pageable next(String token) {
        String[] tokens = token.split("|");
        return new CompositePage(
                (Page) galleryPage.next(tokens[0]),
                (Page) fileInfoPage.next(tokens[1]),
                size);
    }

    @Override
    public Pageable end() {
        return new CompositePage((Page) galleryPage.end(), (Page) fileInfoPage.end(), size);
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        galleryPage.setSize(size);
        fileInfoPage.setSize(size);
        this.size = size;
    }

    @Override
    public boolean hasMore() {
        return galleryPage.hasMore() && fileInfoPage.hasMore();
    }

    @Override
    public String getNextToken() {
        return encodeToken(galleryPage.getNextToken(), fileInfoPage.getNextToken());
    }

    public String encodeToken(String firstNexToken, String secondNextToken) {
        return firstNexToken + "|" + secondNextToken;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(galleryPage, flags);
        dest.writeParcelable(fileInfoPage, flags);
        dest.writeInt(size);
    }

    @Override
    public String toString() {
        return "CompositePage{" +
                "fileInfoPage=" + fileInfoPage +
                ", galleryPage=" + galleryPage +
                '}';
    }
}
