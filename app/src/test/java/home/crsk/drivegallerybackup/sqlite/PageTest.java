package home.crsk.drivegallerybackup.sqlite;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Created by criskey on 11/1/2017.
 */
public class PageTest {


    @Test
    public void should_have_more_pages_for_the_first_time(){
        Pageable page = new Page(10);
        assertTrue(page.hasMore());
        page = page.end();
        assertFalse(page.hasMore());
        page = page.next(UUID.randomUUID().toString());
        assertTrue(page.hasMore());
        page = page.end();
        assertFalse(page.hasMore());
    }
}