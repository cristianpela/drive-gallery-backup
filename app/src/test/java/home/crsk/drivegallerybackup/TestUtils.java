package home.crsk.drivegallerybackup;

import java.util.Random;
import java.util.UUID;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.model.FileInfoBuilder;
import home.crsk.drivegallerybackup.services.CacheServiceTest;

/**
 * Created by criskey on 8/1/2017.
 */

public class TestUtils {

    private static final Random mRandom = new Random();

    public static FileInfo generateRandomFileInfo() {
        return generateRandomFileInfo(false, false, false);
    }

    public static FileInfo generateRandomFileInfo(boolean pendingUp, boolean pendingUpdate, boolean pendingDelete) {
        return new FileInfoBuilder()
                .setId(mRandom.nextInt(10000) + 1)
                .setDriveId(UUID.randomUUID().toString())
                .setLocalId(mRandom.nextInt(10000) + 1)
                .setName(UUID.randomUUID().toString())
                .setCreatedTime(System.currentTimeMillis())
                .setMimeType(UUID.randomUUID().toString())
                .setLocalPath(UUID.randomUUID().toString())
                .setThumbPath(UUID.randomUUID().toString())
                .setSynced(true)
                .setPendingDel(pendingDelete)
                .setPendingUp(pendingUp)
                .setPendingUpdate(pendingUpdate)
                .createFileInfo();
    }

    public static String getPath(Class clazz){
        return clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
    }

}
