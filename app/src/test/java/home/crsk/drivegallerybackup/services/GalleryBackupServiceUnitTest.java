package home.crsk.drivegallerybackup.services;

import android.util.Pair;

import com.google.android.gms.auth.GoogleAuthException;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import home.crsk.drivegallerybackup.sqlite.Pageable;
import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.State;
import home.crsk.drivegallerybackup.sqlite.FileInfoDAO;
import home.crsk.drivegallerybackup.sqlite.FileInfoDBConnection;
import home.crsk.drivegallerybackup.sqlite.Transaction;
import home.crsk.drivegallerybackup.utils.FluentMap;

import static android.provider.BaseColumns._ID;
import static home.crsk.drivegallerybackup.TestUtils.generateRandomFileInfo;
import static home.crsk.drivegallerybackup.services.ICacheService.NORMAL;
import static home.crsk.drivegallerybackup.services.ICacheService.PENDING;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.DRIVE_ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.LOCAL_ID;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 8/1/2017.
 */

public class GalleryBackupServiceUnitTest {

    FileInfoDBConnection mConnection;

    MockFileInfoDAO mDao;

    GalleryBackupService mService;

    List<FileInfo> mGallery = new ArrayList<>();

    MockCloudService mCloudService;

    FileInfoDAO mGalleryDao;

    Map<String, List<FileInfo>> mCacheFolder;

    @Before
    public void setup() {

        mCacheFolder = new HashMap<>();
        mCacheFolder.put(NORMAL, new ArrayList<FileInfo>());
        mCacheFolder.put(PENDING, new ArrayList<FileInfo>());
        ICacheService mCacheService = mock(ICacheService.class);
        whenCacheService(mCacheService);


        mConnection = new MockConnection();
        mDao = (MockFileInfoDAO) mConnection.getDAO();

        mCloudService = spy(new MockCloudService());

        mGalleryDao = mock(FileInfoDAO.class);
        whenGallery();

        mService = new GalleryBackupService(
                mCloudService,
                mConnection,
                new MockStateLoader(),
                mCacheService,
                mGalleryDao);
    }

    @Test
    public void test_gallery() {
        FileInfo fileInfo = mGalleryDao.readOne("0");
        assertTrue("file info must not be null", fileInfo != null);
        assertEquals("file gallery local id must be 0", 0L, fileInfo.localId);

        assertEquals("File info must exist", true, mGalleryDao.exists(fileInfo));
        assertEquals("File doesn't must exist", false, mGalleryDao.exists(generateRandomFileInfo()));
    }

    @Test
    public void test_basic_upload() {
        FileInfo fileInfo = mGalleryDao.readOne("0");

        FileInfo uploaded = null;
        try {
            uploaded = mService.upload(fileInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(uploaded != null);
        assertTrue(mDao.count() == 1);
        assertEquals("Cloud ID must be 1", "1", uploaded.driveId);
        FileInfo savedLocally = mDao.read(null, null).get(0);
        uploaded = uploaded.mutate(new FluentMap().put(_ID, savedLocally.id).get());
        assertEquals("Saved file must be equal wih uploaded", true, uploaded.equals(savedLocally));
        assertEquals("Saved file must have local id 0", 0L, uploaded.localId);
    }

    @Test
    public void test_offline_upload() {
        FileInfo fileInfo = mGalleryDao.readOne("0");

        goOffline();


        try {
            fileInfo = mService.upload(fileInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertTrue(mDao.count() == 1);
        FileInfo savedLocally = mDao.read(null, null).get(0);
        assertEquals("saved to db file must have same id with file from db", fileInfo.id, savedLocally.id);
        assertEquals("Saved file must be pending upload", true, savedLocally.pendingUp);


        //try again with the same file, but this time it wont be saved locally
        try {
            mService.upload(fileInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals("Must be only one record", 1, mDao.count());


        //adding a new file
        try {
            mService.upload(mGalleryDao.readOne("1"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertTrue(mDao.count() == 2);

    }

    @Test
    public void upload_offline_then_delete_one_then_sync_when_online() {

        goOffline();
        mService.multipleUpload(mGallery, null);
        assertEquals("Must be 4 records in local db", 4, mDao.count());
        mGallery.remove(0); //meanwhile user removes a file
        assertEquals("Must be 4 files in pending cache", 4, mCacheFolder.get(PENDING).size());

        // whe are  online
        goOnline();
        try {
            mService.sync(false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            List<FileInfo> cloudFiles = mCloudService.fetch("");
            assertEquals("There must 4 files on cloud", 4, cloudFiles.size());
            assertEquals("Must be 0 files in pending cache", 0, mCacheFolder.get(PENDING).size());
            assertEquals("Must be 1 files in normal cache because we remove first file from gallery", 1, mCacheFolder.get(NORMAL).size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test_update(){
        String newLocalPath = "new_local_path";
        long newLocalId = 5555L;
        FileInfo file = null;
        try {
            file = mService.upload(mGallery.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }

        file.localPath  = newLocalPath;
        file.localId = newLocalId;

        try {
            mService.update(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileInfo fromDb = get(file, mDao.mData, new Exist<FileInfo>() {
            @Override
            public boolean exists(FileInfo fromList, FileInfo toCheck) {
                return fromList.id == toCheck.id;
            }
        });
        assertEquals("Must have the local id updated", newLocalId, fromDb.localId);
        assertEquals("Must have the local path updated", newLocalPath, fromDb.localPath);

        FileInfo fromCloud = get(file, mCloudService.mData, new Exist<FileInfo>() {
            @Override
            public boolean exists(FileInfo fromList, FileInfo toCheck) {
                return fromList.driveId == toCheck.driveId;
            }
        });
        assertEquals("Must have the local id updated", newLocalId, fromCloud.localId);
        assertEquals("Must have the local path updated", newLocalPath, fromCloud.localPath);

    }

    @Test
    public void test_update_offline(){

        String newLocalPath = "new_local_path";
        FileInfo file = null;
        try {
            file = mService.upload(mGallery.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }


        goOffline();
        file.localPath  = newLocalPath;

        try {
            mService.update(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileInfo fromDb = mDao.readOne("0");
        assertEquals("Must have the local path updated", newLocalPath, fromDb.localPath);
        assertEquals("Must have pending update", true, fromDb.pendingUpdate);

        goOnline();

        try {
            mService.sync(false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        fromDb = mDao.readOne("0");
        assertEquals("Must not have pending update", false, fromDb.pendingUpdate);

        FileInfo fromCloud = get(file, mCloudService.mData, new Exist<FileInfo>() {
            @Override
            public boolean exists(FileInfo fromList, FileInfo toCheck) {
                return fromList.driveId == toCheck.driveId;
            }
        });
        assertEquals("Must have the local path updated", newLocalPath, fromCloud.localPath);
    }

    private void goOffline() {
        try {
            when(mCloudService.exists(isA(FileInfo.class))).thenThrow(new IOException("We are offline"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void goOnline() {
        reset(mCloudService);
    }

    private void whenCacheService(ICacheService mCacheService) {
        try {
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    FileInfo fileInfo = invocation.getArgument(1);
                    if (fileInfo != null) {
                        List<FileInfo> destFolder = mCacheFolder.get(invocation.getArgument(0));
                        destFolder.add(fileInfo.mutate());
                    }
                    return null;
                }
            }).when(mCacheService).save(or(eq(NORMAL), eq(PENDING)), isA(FileInfo.class));
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    FileInfo fileInfo = invocation.getArgument(0);
                    if (fileInfo != null) {
                        String dest = ((File) invocation.getArgument(1)).getName();
                        if (dest.equals(NORMAL)) {
                            List<FileInfo> destFolder = mCacheFolder.get(dest);
                            destFolder.add(fileInfo.mutate());
                        }
                    }
                    return null;
                }
            }).when(mCacheService).move(isA(FileInfo.class), isA(File.class));
            doAnswer(new Answer<Boolean>() {
                @Override
                public Boolean answer(InvocationOnMock invocation) throws Throwable {
                    FileInfo f = invocation.getArgument(0);
                    boolean isInNormal = (exists(f, mCacheFolder.get(NORMAL), new Exist<FileInfo>() {
                        @Override
                        public boolean exists(FileInfo fromList, FileInfo toCheck) {
                            return fromList.id == toCheck.id;
                        }
                    }));
                    boolean isInPending = exists(f, mCacheFolder.get(PENDING), new Exist<FileInfo>() {
                        @Override
                        public boolean exists(FileInfo fromList, FileInfo toCheck) {
                            return fromList.id == toCheck.id;
                        }
                    });
                    return f != null
                            && isInNormal
                            || isInPending;
                }
            }).when(mCacheService).exists(isA(FileInfo.class));
            doAnswer(new Answer<File>() {
                @Override
                public File answer(InvocationOnMock invocation) throws Throwable {
                    return new File((String) invocation.getArgument(0));
                }
            }).when(mCacheService).getDirectory(or(eq(NORMAL), eq(PENDING)));
            when(mCacheService.getPath(or(eq(NORMAL), eq(PENDING)), isA(FileInfo.class))).then(new Answer<String>() {
                @Override
                public String answer(InvocationOnMock invocation) throws Throwable {
                    String dest = invocation.getArgument(0);
                    return dest == null ? NORMAL : dest;
                }
            });
            doAnswer(new Answer<Boolean>() {
                @Override
                public Boolean answer(InvocationOnMock invocation) throws Throwable {
                    FileInfo f = invocation.getArgument(0);
                    return (f != null) &&
                            (remove(f, mCacheFolder.get(NORMAL)) || (remove(f, mCacheFolder.get(PENDING))));
                }
            }).when(mCacheService).delete(isA(FileInfo.class));
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    mCacheFolder.get(NORMAL).clear();
                    return null;
                }
            }).when(mCacheService).clear();
            doAnswer(new Answer<Void>() {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable {
                    FileInfo f = invocation.getArgument(0);
                    mCacheFolder.get(NORMAL).add(f);
                    remove(f, mCacheFolder.get(PENDING));
                    return null;
                }
            }).when(mCacheService).move(isA(FileInfo.class), any(File.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean remove(FileInfo fileInfo, List<FileInfo> list) {
        for (FileInfo f : list) {
            if (f.id == fileInfo.id)
                return list.remove(f);
        }
        return false;
    }

    private void whenGallery() {
        FileInfo fi = generateRandomFileInfo();
        fi = fi.mutate(new FluentMap().put(LOCAL_ID, 0L).get());
        mGallery.add(fi);
        fi = generateRandomFileInfo();
        fi = fi.mutate(new FluentMap().put(LOCAL_ID, 1L).get());
        mGallery.add(fi);
        fi = generateRandomFileInfo();
        fi = fi.mutate(new FluentMap().put(LOCAL_ID, 2L).get());
        mGallery.add(fi);
        fi = generateRandomFileInfo();
        fi = fi.mutate(new FluentMap().put(LOCAL_ID, 3L).get());
        mGallery.add(fi);
        when(mGalleryDao.readOne(anyString())).thenAnswer(new Answer<FileInfo>() {
            @Override
            public FileInfo answer(InvocationOnMock invocation) throws Throwable {
                int id = Integer.parseInt((String) invocation.getArgument(0));
                return mGallery.get(id);
            }
        });
        when(mGalleryDao.exists(isA(FileInfo.class))).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                final FileInfo argument = invocation.getArgument(0);
                return exists(argument, GalleryBackupServiceUnitTest.this.mGallery, new Exist<FileInfo>() {
                    @Override
                    public boolean exists(FileInfo fromList, FileInfo toCheck) {
                        return fromList.localId == toCheck.localId;
                    }
                });
            }
        });
    }

    private boolean exists(FileInfo argument, List<FileInfo> list, Exist exist) {
        for (FileInfo f : list) {
            if (exist.exists(f, argument))
                return true;
        }
        return false;
    }

    private FileInfo get(FileInfo argument, List<FileInfo> list, Exist exist) {
        for (FileInfo f : list) {
            if (exist.exists(f, argument))
                return f;
        }
        return null;
    }

    interface Exist<T> {
        boolean exists(T fromList, T toCheck);
    }

    private static class MockConnection implements FileInfoDBConnection {

        FileInfoDAO mDao = new MockFileInfoDAO();

        @Override
        public void open() {

        }

        @Override
        public void close() {

        }

        @Override
        public FileInfoDAO getDAO() {
            return mDao;
        }

        @Override
        public <Result> Result runTransaction(Transaction.Query<Result> query) {
            return new Transaction(mDao) {

                @Override
                public void setTransactionSuccessful() {

                }

                @Override
                public void beginTranscation() {

                }

                @Override
                public void endTransaction() {

                }
            }.execute(query);
        }


    }

    private static class MockFileInfoDAO implements FileInfoDAO {


        List<FileInfo> mData = new ArrayList<>();

        private long pk = 0;


        public List<FileInfo> pendings() {
            List<FileInfo> pendings = new ArrayList<>();
            for (FileInfo f : mData) {
                if (f.pendingDel || f.pendingUp || f.pendingUpdate)
                    pendings.add(f);
            }
            return pendings;
        }

        @Override
        public long create(FileInfo fileInfo) {
            fileInfo = fileInfo.mutate(new FluentMap().
                    put(_ID, ++pk).get());
            mData.add(fileInfo);
            return pk;
        }

        @Override
        public Pair<Pageable, List<FileInfo>> read(Pageable page) {
            return new Pair(page, mData);
        }

        @Override
        public List<FileInfo> read(String where, String... selectionArgs) {
            if (where != null)
                return pendings();
            return mData;
        }

        @Override
        public FileInfo readOne(String where, String... selectionArgs) {
            return mData.get(Integer.parseInt(where));
        }

        @Override
        public int update(FileInfo fileInfo) {
            int index = -1;
            for (int i = 0; i < mData.size(); i++) {
                FileInfo f = mData.get(i);
                if (f.id == fileInfo.id) {
                    index = i;
                    break;
                }
            }
            mData.set(index, fileInfo);
            return 0;
        }

        @Override
        public int deleteAll() {
            mData.clear();
            return 0;
        }

        @Override
        public int delete(FileInfo fileInfo) {
            mData.remove(fileInfo);
            return 0;
        }

        @Override
        public int count() {
            return mData.size();
        }

        @Override
        public boolean exists(FileInfo fileInfo) {
            for (int i = 0; i < mData.size(); i++) {
                FileInfo f = mData.get(i);
                if (f.localId == fileInfo.localId)
                    return true;
            }
            return false;
        }


    }

    private static class MockCloudService implements ICloudService {

        List<FileInfo> mData = new ArrayList<>();

        private int driveId = 0;

        @Override
        public void auth(String accountName) throws IOException {

        }

        @Override
        public String setup(String folderName) throws IOException {
            return null;
        }

        @Override
        public void reset(String accountName) throws IOException, GoogleAuthException {
        }

        @Override
        public FileInfo upload(FileInfo fileInfo, String folderId) throws IOException {
            if (fileInfo == null)
                return null;
            FileInfo cloudInfo = fileInfo.mutate(new FluentMap().put(DRIVE_ID, "" + (++driveId)).get());
            mData.add(cloudInfo);
            return cloudInfo;
        }

        @Override
        public void delete(FileInfo fileInfo) throws IOException {
            mData.remove(fileInfo);
        }

        @Override
        public void update(FileInfo fileInfo, String folderId) throws IOException {
            int index = find(fileInfo);
            mData.set(index, fileInfo);
        }

        @Override
        public boolean exists(FileInfo fileInfo) throws IOException {
            return mData.contains(fileInfo);
        }

        @Override
        public List<FileInfo> fetch(String folderId) throws IOException {
            return mData;
        }

        @Override
        public FileInfo fetchOne(String driveId) throws IOException {
            for (int i = 0; i < mData.size(); i++) {
                FileInfo f = mData.get(i);
                if (f.driveId.equals(driveId))
                    return f;
            }
            return null;
        }

        private int find(FileInfo fileInfo) {
            for (int i = 0; i < mData.size(); i++) {
                FileInfo f = mData.get(i);
                if (f.id == fileInfo.id)
                    return i;
            }
            return -1;
        }

    }

    private class MockStateLoader implements IStateLoader {

        State.StateBuilder builder = new State.StateBuilder().setFolderId("folderId")
                .setConfigured(true)
                .setAuthenticated(true)
                .setFolderName("folderName")
                .setDbCreated(true);

        @Override
        public State load() {
            return builder.createState();
        }

        @Override
        public State.StateBuilder loadBuilder() {
            return loadBuilder();
        }

        @Override
        public void save(@NotNull State state) {

        }

        @Override
        public void reset() {

        }
    }


}
