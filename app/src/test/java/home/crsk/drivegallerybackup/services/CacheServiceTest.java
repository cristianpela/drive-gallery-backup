package home.crsk.drivegallerybackup.services;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import home.crsk.drivegallerybackup.model.FileInfo;
import home.crsk.drivegallerybackup.TestUtils;

import static org.junit.Assert.*;

/**
 * Created by criskey on 9/1/2017.
 */
public class CacheServiceTest {

    ICacheService mCacheService;
    File mRootCache;
    File mGallery;
    String mPath;

    @Before
    public void before() {
        mPath = TestUtils.getPath(this.getClass());
        mRootCache = new File(mPath, "cache");
        mRootCache.mkdir();
        mCacheService = new CacheService(mRootCache, new StreamHandler());
        mGallery = new File(mPath, "gallery");
        mGallery.mkdir();
    }

    @After
    public void after() {
        mCacheService.clear();
        mRootCache.delete();
        for(File f : mGallery.listFiles()){
            f.delete();
        }
        mGallery.delete();
    }

    @Test
    public void test_cache_dir_creation() {
        assertEquals("Cache directory created", true, mRootCache.exists());
    }

    @Test
    public void test_file_caching() {
        String content = "Hello World Normal";
        FileInfo fileInfo = TestUtils.generateRandomFileInfo();
        try {
            mCacheService.save(ICacheService.PENDING, fileInfo, content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue("File must exist in cache", mCacheService.exists(fileInfo));
        try {
            assertEquals("Content must be 'Hello World Normal'", content, new String(mCacheService.loadFromCache(fileInfo), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //delete
        mCacheService.delete(fileInfo);
        assertTrue("File must not exist in cache", !mCacheService.exists(fileInfo));

        content = "Hello World Pending";
        fileInfo = TestUtils.generateRandomFileInfo();
        try {
            mCacheService.save(ICacheService.PENDING, fileInfo, content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue("File must exist in cache", mCacheService.exists(fileInfo));
        try {
            assertEquals("Content must be 'Hello World Pending'", content, new String(mCacheService.loadFromCache(fileInfo), "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //delete
        mCacheService.delete(fileInfo);
        assertTrue("File must not exist in cache", !mCacheService.exists(fileInfo));
    }

    @Test
    public void test_move() {
        String content = "Hello World Normal";

        FileInfo fileInfo = TestUtils.generateRandomFileInfo();
        File galleryFile = new File(mGallery, ""+fileInfo.id);
        try {
            mCacheService.save(ICacheService.NORMAL, fileInfo, content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            mCacheService.move(fileInfo, mGallery);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue("File must not exist in cache anymore", !mCacheService.exists(fileInfo));
        assertTrue("Gallery must have one file", mGallery.listFiles().length == 1);
        assertTrue("Gallery file must exist", galleryFile.exists());

    }


    @Test
    public void test_clear(){
        String content = "Hello World Normal";
        try {
            mCacheService.save(ICacheService.NORMAL, TestUtils.generateRandomFileInfo(), content.getBytes());
            mCacheService.save(ICacheService.NORMAL, TestUtils.generateRandomFileInfo(), content.getBytes());
            mCacheService.save(ICacheService.NORMAL, TestUtils.generateRandomFileInfo(), content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        File normalCache = new File(mRootCache, "NORMAL");
        assertTrue(normalCache.exists());
        assertTrue(normalCache.list().length == 3);

        mCacheService.clear();
        assertTrue(!normalCache.exists());
        assertTrue(mRootCache.list().length == 0);
    }

}

