package home.crsk.drivegallerybackup.model;

import org.junit.Test;

import home.crsk.drivegallerybackup.TestUtils;
import home.crsk.drivegallerybackup.utils.FluentMap;

import static android.provider.BaseColumns._ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.DRIVE_ID;
import static home.crsk.drivegallerybackup.sqlite.Contract.FileInfoTable.NAME;
import static org.junit.Assert.*;

/**
 * Created by criskey on 12/1/2017.
 */
public class FileInfoTest {

    @Test
    public void test_eq(){
        FileInfo f1 = new FileInfoBuilder().setId(1).createFileInfo();
        FileInfo f2 = new FileInfoBuilder().setId(1).createFileInfo();
        assertTrue(f1.equals(f2) && f2.equals(f1));
        f1 = new FileInfoBuilder().setId(0).setLocalId(123).createFileInfo();
        f2 = new FileInfoBuilder().setId(45).setLocalId(123).createFileInfo();
        assertTrue(f1.equals(f2) && f2.equals(f1));
        f1 = new FileInfoBuilder().setId(0).setLocalId(123).createFileInfo();
        f2 = new FileInfoBuilder().setId(45).setLocalId(123).setDriveId("3242").createFileInfo();
        assertTrue(f1.equals(f2) && f2.equals(f1));

        f1 = new FileInfoBuilder().setId(0).setLocalId(1234).createFileInfo();
        f2 = new FileInfoBuilder().setId(45).setLocalId(123).setDriveId("3242").createFileInfo();
        assertFalse(f1.equals(f2) && f2.equals(f1));
    }


    @Test
    public void testChange(){
        FileInfo fromFile = TestUtils.generateRandomFileInfo();
        FileInfo myFile = TestUtils.generateRandomFileInfo();
        myFile.change(fromFile, _ID, DRIVE_ID, NAME);
        assertEquals(fromFile.id, myFile.id);
        assertEquals(fromFile.driveId, myFile.driveId);
        assertEquals(fromFile.name, myFile.name);
    }

    @Test
    public void testMutate(){
        FileInfo fromFile = TestUtils.generateRandomFileInfo();
        FileInfo myFile = fromFile.mutate();
        assertEquals(fromFile.id, myFile.id);
        assertEquals(fromFile.driveId, myFile.driveId);
        assertEquals(fromFile.name, myFile.name);

        myFile = myFile.mutate(new FluentMap().put(DRIVE_ID, "foo").get());
        assertEquals(fromFile.id, myFile.id);
        assertEquals("foo", myFile.driveId);
        assertEquals(fromFile.name, myFile.name);
    }
}