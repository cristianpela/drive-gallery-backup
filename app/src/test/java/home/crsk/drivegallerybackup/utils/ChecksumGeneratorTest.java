package home.crsk.drivegallerybackup.utils;

import org.junit.Test;

import java.io.File;
import java.util.concurrent.TimeUnit;

import home.crsk.drivegallerybackup.TestUtils;

import static org.junit.Assert.*;

/**
 * Created by criskey on 21/1/2017.
 */
public class ChecksumGeneratorTest {

    @Test
    public void generate() throws Exception {
        File path = new File(TestUtils.getPath(this.getClass()));
        assertTrue(path.isDirectory());
        File photo = new File(path, "CAM00022.jpg");
        assertTrue(photo.exists() && photo.isFile());

        ChecksumGenerator checksumGenerator = new ChecksumGenerator();
        long start = Utils.now();
        String firstCheckSum = checksumGenerator.generate(photo);
        long delta = Utils.now() - start;
        System.out.println("Checksum duration: " + delta + " checksum: " + firstCheckSum);

        String secondCheckSum = checksumGenerator.generate(photo);
        assertEquals("Must be the same checksum ", true, secondCheckSum.equals(firstCheckSum));

    }

}